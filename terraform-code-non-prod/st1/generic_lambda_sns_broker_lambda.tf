#Logging buckets generic for all bucket of HN
resource "aws_s3_bucket" "hn-general-dv1-bucket-auditing" {
  bucket = "s3b-parcels-np-dv1-euwe01-hn-bucket-audits-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-dv1-euwe01-hn-bucket-audits-01"
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
}

resource "aws_s3_bucket" "hn-general-st1-bucket-auditing" {
  bucket = "s3b-parcels-np-st1-euwe01-hn-bucket-audits-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-st1-euwe01-hn-bucket-audits-01"
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
}

resource "aws_s3_bucket" "hn-general-ac1-bucket-auditing" {
  bucket = "s3b-parcels-np-ac1-euwe01-hn-bucket-audits-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-ac1-euwe01-hn-bucket-audits-01"
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
}

#Repositories bucket generic for all Lambda functions of HN
resource "aws_s3_bucket" "hn-general-dv1-lambda-repo" {
  bucket = "s3b-parcels-np-dv1-euwe01-hn-lambda-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-dv1-euwe01-hn-lambda-repo-01"   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }	
  }		
 logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = "s3b-parcels-np-dv1-euwe01-hn-lambda-repo-01"
  } 
}

resource "aws_s3_bucket" "hn-general-st1-lambda-repo" {
  bucket = "s3b-parcels-np-st1-euwe01-hn-lambda-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-st1euwe01-hn-lambda-repo-01"   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }	
  }		
 logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = "s3b-parcels-np-st1-euwe01-hn-lambda-repo-01"
  } 
}

resource "aws_s3_bucket" "hn-general-ac1-lambda-repo" {
  bucket = "s3b-parcels-np-ac1-euwe01-hn-lambda-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-ac1euwe01-hn-lambda-repo-01"   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }	
  }		
 logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = "s3b-parcels-np-ac1-euwe01-hn-lambda-repo-01"
  } 
}

resource "aws_lambda_function" "hn_lmb_filter_sns-msg-broker-commonevents_dv1" {
  s3_bucket = aws_s3_bucket.hn-general-dv1-lambda-repo.id
  s3_key = "lmb-parcels-np-dv1-euwe01-hn-sns-msg-broker-commonevents-01/app.zip"
  function_name = "lmb-parcels-np-dv1-euwe01-hn-sns-msg-broker-commonevents-01"
  role          = aws_iam_role.hn_lmb_filter_sns-msg-broker-commonevents-role.arn
  handler       = "CommonEventsSubscriptionLambda.handler"
  runtime = "nodejs12.x"
  timeout = 60

  environment {
    variables = {
	      BROKER_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-dv1-euwe01-brokerservice-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-dv1-euwe01-hn-sns-msg-broker-commonevents-01"
        }
  }

  resource "aws_lambda_permission" "allow_old_foundation_dv1" {
  statement_id  = "AllowExecutionFromSNSOldFoundation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_dv1.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-DV1-WE1-COMMON-EVENTS"
}

data "aws_iam_policy_document" "hn_lmb_filter_sns-msg-broker-commonevents-policydoc" {
    statement {
        actions = [
              "logs:CreateLogStream",
              "logs:PutLogEvents", 
              "logs:CreateLogGroup"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "kms:Decrypt",
              "kms:GenerateDataKey"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
                "sqs:SendMessage",
                "sqs:GetQueueAttributes"            
        ]
        effect = "Allow"
        resources=["*"]
    }
  }

data "aws_iam_policy_document" "hn_lmb_filter_sns-msg-broker-commonevents_assumerole-policydoc" {
   statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
   }
}

resource "aws_iam_policy" "hn_lmb_filter_sns-msg-broker-commonevents-policy" {
    name = "iap-parcels-np-euwe01-hn-lmb-sns-msg-broker-commonevents"  
    policy = data.aws_iam_policy_document.hn_lmb_filter_sns-msg-broker-commonevents-policydoc.json
}

resource "aws_iam_role_policy_attachment" "hn_lmb_filter_sns-msg-broker-commonevents-policy_custom" {
  role       = aws_iam_role.hn_lmb_filter_sns-msg-broker-commonevents-role.name
  policy_arn = aws_iam_policy.hn_lmb_filter_sns-msg-broker-commonevents-policy.arn
}

resource "aws_iam_role" "hn_lmb_filter_sns-msg-broker-commonevents-role" {
  name = "iar-parcels-np-euwe01-hn-lmb-sns-msg-broker-commonevents"
  path = "/"
  assume_role_policy = data.aws_iam_policy_document.hn_lmb_filter_sns-msg-broker-commonevents_assumerole-policydoc.json
}

resource "aws_sns_topic_subscription" "hn_lmb_filter_sns-msg-broker-commonevents_dv1-subscription" {
  topic_arn = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-DV1-WE1-COMMON-EVENTS"
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_dv1.arn
  filter_policy = "{ \"Origin\": [\"NeRO\",\"PPD\",\"LOS\"] }"
}

resource "aws_lambda_function" "hn_lmb_filter_sns-msg-broker-commonevents_st1" {
  s3_bucket = aws_s3_bucket.hn-general-st1-lambda-repo.id
  s3_key = "lmb-parcels-np-st1-euwe01-hn-sns-msg-broker-commonevents-01/app.zip"
  function_name = "lmb-parcels-np-st1-euwe01-hn-sns-msg-broker-commonevents-01"
  role          = aws_iam_role.hn_lmb_filter_sns-msg-broker-commonevents-role.arn
  handler       = "CommonEventsSubscriptionLambda.handler"
  runtime = "nodejs12.x"
  timeout = 60

  environment {
    variables = {
	      BROKER_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-st1-euwe01-brokerservice-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-st1-euwe01-hn-sns-msg-broker-commonevents-01"
        }
  }

  resource "aws_lambda_permission" "allow_old_foundation_st1" {
  statement_id  = "AllowExecutionFromSNSOldFoundation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_st1.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-ST1-WE1-COMMON-EVENTS"
}

resource "aws_sns_topic_subscription" "hn_lmb_filter_sns-msg-broker-commonevents_st1-subscription" {
  topic_arn = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-ST1-WE1-COMMON-EVENTS"
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_st1.arn
  filter_policy = "{ \"Origin\": [\"NeRO\",\"PPD\",\"LOS\"] }"
}

resource "aws_lambda_function" "hn_lmb_filter_sns-msg-broker-commonevents_ac1" {
  s3_bucket = aws_s3_bucket.hn-general-ac1-lambda-repo.id
  s3_key = "lmb-parcels-np-ac1-euwe01-hn-sns-msg-broker-commonevents-01/app.zip"
  function_name = "lmb-parcels-np-ac1-euwe01-hn-sns-msg-broker-commonevents-01"
  role          = aws_iam_role.hn_lmb_filter_sns-msg-broker-commonevents-role.arn
  handler       = "CommonEventsSubscriptionLambda.handler"
  runtime = "nodejs12.x"
  timeout = 60

  environment {
    variables = {
	      BROKER_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-ac1-euwe01-brokerservice-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-ac1-euwe01-hn-sns-msg-broker-commonevents-01"
        }
  }

  resource "aws_lambda_permission" "allow_new_foundation_topic_ac1" {
  statement_id  = "AllowExecutionFromSNSOldFoundation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_ac1.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = "arn:aws:sns:eu-west-1:086243371668:sns-hn-np-ac1-euwe01-common-events"
}

resource "aws_sns_topic_subscription" "hn_lmb_filter_sns-msg-broker-commonevents_ac1-subscription" {
  topic_arn = "arn:aws:sns:eu-west-1:086243371668:sns-hn-np-ac1-euwe01-common-events"
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_ac1.arn
  filter_policy = "{ \"Origin\": [\"NeRO\",\"PPD\",\"LOS\"] }"
}








