locals {
  # Common tags to be assigned to all resources
  common_tags = {
    "ApplicationAcronym"   = var.applicationAcronym
    "ApplicationName"      = var.applicationName
    "ApplicationOwner"     = var.applicationOwner
    "ApplicationSupport"   = var.applicationSupport
    "Backup"               = var.backups
    "CloudServiceProvider" = var.cloudServiceProvider
    "DRLevel"              = var.drLevel
    "DataProfile"          = var.dataProfile
    "Environment"          = var.envTaggingConvention
    "ManagedBy"            = var.managedBy

  }

  ############ RESOURCE NAMING CONVENTION ################
   #apg-parcels-np-st1-hn
#apg <resource type>-<app cluster>-<environment>-<application acronym>-<sequence number>
   apgName= lower(join("-", [var.resourceType["apigateway"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0],
   var.applicationAcronym]))
  
   apkName= lower(join("-", [var.resourceType["apikey"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym]))

   apuName= lower(join("-", [var.resourceType["apiusage"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym]))

  # "asm-parcels-np-st1-euwe01-hn-feedback-hn-intake-api-01"
   asmName= lower(join("-", [var.resourceType["secretmanager"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym]))
  
  #lambda  <resource type>-<app cluster>-<environment>-<region>-<application acronym>-<sequence number>
   lmbName = lower(join("-", [var.resourceType["lambda"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym]))
 
  #s3b Naming Convention <resource type>-<app cluster>-<environment>-<region>-<<application acronym>-<sequence number>
   s3bName = lower(join("-", [var.resourceType["s3bucket"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym,]))

  sqsName = lower(join("-", [var.resourceType["sqsqueue"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym,]))

   #ctd Naming Convention <resource type>-<app cluster>-<environment>-<region>-<<application acronym>-<sequence number>
   ctdName = lower(join("-", [var.resourceType["ecstaskdef"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym,]))

   #ecs Naming Convention <resource type>-<app cluster>-<environment>-<region>-<<application acronym>-<sequence number>
   ecsName = lower(join("-", [var.resourceType["ecssvc"], var.appcluster[0], var.envNamingConvention[0], var.categoryEnvironment[0], var.region,
   var.applicationAcronym,]))

 
 #########################RESOURCES REUSABLE BLOCK ##################
    vpcconfig = {
    security_group_ids = ["sg-0429b6a02a15d681a"]
    subnet_ids = [
      "subnet-05a33b81b6ea84ff4",
      "subnet-0b43ff380a425d9aa"
    ]
  }
    
}

########################### VARIABLES DEFINITION FOR LOCALS ######################
variable "resourceType" {
  type = map(string)
  default = {
    secretmanager : "asm"
    lambda : "lmb"
    s3bucket : "s3b"
    apigateway: "apg"
    apiusage: "apu"
    apikey: "apk"
    sqsqueue: "sqs"
    ecstaskdef: "ctd"
    ecssvc: "ecs"
    
  }
}
variable "envNamingConvention" {
  type = list(string)
  default =["np","pr"]
  }

variable "categoryEnvironment" {
  type = list(string)
  default = ["st1"]
  }


variable "appcluster" {
  type = list(string)
  default =["parcels","banking"]
  }

variable "region" {
  default = "euwe01"
}


variable "applicationAcronym" {
  #type = string
  #description="Used as Application acronym name in naming conventions" 
  default = "HN"
}

variable "applicationName" {
  #  type = string
  description = "Used as Application acronym name in naming conventions"
  default     = "Hybrid Network"

}

variable "applicationOwner" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "Pieter.BISSCHOPS@bpost.be"
}


variable "applicationSupport" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "SindhyaEsther.Selvin.ext@bpost.be"
}


variable "backups" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "False"
}


variable "cloudServiceProvider" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "AWS"
}


variable "drLevel" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "2"
}

variable "dataProfile" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "Confidential"
}

variable "envTaggingConvention" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "Non Prod"
}

variable "managedBy" {
  type        = string
  description = "Used as Application acronym name in naming conventions"
  default     = "TCS"
}


 