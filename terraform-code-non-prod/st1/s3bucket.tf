resource "aws_s3_bucket" "hn_fdbck_msg_bucket_dv1" {
    bucket = "s3b-parcels-np-dv1-euwe01-hn-notificationfeedbackmessages-01"
    acl    = "private"
    tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-dv1-euwe01-hn-notificationfeedbackmessages-01"
    }
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default {
                kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
                sse_algorithm     = "aws:kms"
            }
        }
    }

    logging {
        target_bucket = aws_s3_bucket.hn-general-dv1-bucket-auditing.id
        target_prefix = "s3b-parcels-np-dv1-euwe01-hn-notificationfeedbackmessages-01"
  } 
}

resource "aws_s3_bucket" "hn_fdbck_evnt_bucket_dv1" {
  bucket = "s3b-parcels-np-dv1-euwe01-hn-notificationfeedbackevent-01"
    acl    = "private"
    tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Prod"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-dv1-euwe01-hn-notificationfeedbackevent-01"
    }
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default {
                kms_master_key_id = var.kmsMasterKeyID
                sse_algorithm     = var.kms
            }
        }
    }

    logging {
        target_bucket = aws_s3_bucket.hn-general-dv1-bucket-auditing.id
        target_prefix = "s3b-parcels-np-dv1-euwe01-hn-notificationfeedbackevent-01"
  } 
}

############# REFACTORIZATION FROM Main.tf to ST1,AC1 #################
resource "aws_s3_bucket" "hn_fdbck_msg_bucket_st1" {
  bucket = join("-",[local.s3bName,"notificationfeedbackmessage-01"])
  acl    = "private"
  tags   =  merge(local.common_tags,map("Name",join("-",[local.s3bName,"notificationfeedbackmessage-01"])))
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.hn-general-st1-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"notificationfeedbackmessage-01"])
  }
}


resource "aws_s3_bucket" "hn_fdbck_evnt_bucket_st1" {
    bucket = join("-",[local.s3bName,"notificationfeedbackevent-01"])
    acl    = "private"
    tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"notificationfeedbackevent-01"])))
    
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default {
                kms_master_key_id = var.kmsMasterKeyID
                sse_algorithm     = var.kms
            }
        }
    }
    logging {
        target_bucket = aws_s3_bucket.hn-general-st1-bucket-auditing.id
        target_prefix = join("-",[local.s3bName,"notificationfeedbackevent-01"])
  } 
}
