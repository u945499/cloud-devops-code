resource "aws_cloudwatch_log_group" "hn_release_svc_st1" {
  name = "/ecs/ctd-parcels-st1-euwe01-hn-release-01"

  tags = merge(local.common_tags,map("Name","ctd-parcels-st1-euwe01-hn-release-01"))
}