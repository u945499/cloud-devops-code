data "template_file" "hn_api_gateway_openapi_spec" {
  
  template = file("./Feedback_Swagger_file.json")
  vars = {
    aws_region = data.aws_region.current.name
    aws_account_id = data.aws_caller_identity.current.account_id
    #uri_arn = join("/", ["arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions", aws_lambda_function.hn_lmb_notif_feedback_dv1.arn, "invocations"])
  }
}

resource "aws_api_gateway_rest_api" "hn_rest_api" {
  description    = "API Gateway for HN"
  name        = "apg-parcels-np-hn-01"
  body = data.template_file.hn_api_gateway_openapi_spec.rendered
  endpoint_configuration {
    types = ["REGIONAL"]
  }
 
 lifecycle {
    ignore_changes         = [body]
  } 
  
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "apg-parcels-np-hn-01"
        }
}





resource "aws_api_gateway_api_key" "hn_api_key" {
  name = "apk-parcels-np-hn-key-01"
}

resource "aws_api_gateway_usage_plan" "hn_api_UsagePlan" {
  name="apu-parcels-np-hn-key-01"
  description  = "Usage plan for apk-parcels-np-hn-key-01"
  api_stages {
    api_id = aws_api_gateway_rest_api.hn_rest_api.id
    stage  = aws_api_gateway_deployment.hn_api_deploy_dv1.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "hn_api_KeyUsagePlan" {
  depends_on = [aws_api_gateway_api_key.hn_api_key, aws_api_gateway_usage_plan.hn_api_UsagePlan]
  key_id        = aws_api_gateway_api_key.hn_api_key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.hn_api_UsagePlan.id
}

############# REFACTORIZATION FROM Main.tf to st1 #################

#Added deployment for HN notification Api for st1
resource "aws_api_gateway_deployment" "hn_api_deploy_st1" {
  rest_api_id = aws_api_gateway_rest_api.hn_rest_api.id
  stage_name  = join("",var.categoryEnvironment)

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "hn_api_stage_st1" {
  stage_name    = join("",var.categoryEnvironment)
  deployment_id = aws_api_gateway_deployment.hn_api_deploy_st1.id
  rest_api_id   = aws_api_gateway_rest_api.hn_rest_api.id
  variables     = {
    lambda_function_name = aws_lambda_function.hn_lmb_notif_feedback_st1.function_name
    host = "hn-api-st1.bposthn.net"
  }
}

resource "aws_api_gateway_api_key" "hn_api_key_st1" {
 # name = "apk-parcels-np-hn-key-st1-01"
  name= join("-",[local.apkName,"01"])
}

resource "aws_api_gateway_usage_plan" "hn_api_UsagePlan_st1" {
  depends_on = [aws_api_gateway_stage.hn_api_stage_st1]
  #name="apu-parcels-np-hn-key-st1-01"
  name= join("-",[local.apuName,"01"])
  description  = "Usage plan for apk-parcels-np-hn-key-01"
  api_stages {
    api_id = aws_api_gateway_rest_api.hn_rest_api.id
    stage  = aws_api_gateway_deployment.hn_api_deploy_st1.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "hn_api_KeyUsagePlan_st1" {
  depends_on = [aws_api_gateway_api_key.hn_api_key_st1, aws_api_gateway_usage_plan.hn_api_UsagePlan_st1]
  key_id        = aws_api_gateway_api_key.hn_api_key_st1.id
  key_type      = var.keyType
  usage_plan_id = aws_api_gateway_usage_plan.hn_api_UsagePlan_st1.id
}