resource "aws_secretsmanager_secret" "hn_nia_lrs_integration_secret_dv1" {
  description         = "Secret for NIA and LRS integration"
  name = "asm-parcels-np-dv1-euwe01-hn-events-tmm-user-detail-01"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-dv1-euwe01-hn-events-tmm-user-detail-01"
        }
}


resource "aws_secretsmanager_secret_version" "hn_nia_lrs_integration_secret_dv1" {
  secret_id     = aws_secretsmanager_secret.hn_nia_lrs_integration_secret_dv1.id
  secret_string = <<EOF
    {
    "endpoint" : "https://webservices.stbpost.be/dv2/trackedmail/hybridnetworkevents",
    "protocol" : "http",
    "USER" : "svc-00432",
    "PASSWORD" : "****"
  }
  EOF

  lifecycle {
    ignore_changes         = [secret_string]
  }
} 

resource "aws_secretsmanager_secret" "hn_nia_lrs_integration_secret_st1" {
  description         = "Secret for NIA and LRS integration"
  name = "asm-parcels-np-st1-euwe01-hn-events-tmm-user-detail-01"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-st1-euwe01-hn-events-tmm-user-detail-01"
        }
}


resource "aws_secretsmanager_secret_version" "hn_nia_lrs_integration_secret_st1" {
  secret_id     = aws_secretsmanager_secret.hn_nia_lrs_integration_secret_st1.id
  secret_string = <<EOF
    {
    "endpoint" : "https://webservices.stbpost.be/st2/trackedmail/hybridnetworkevents",
    "protocol" : "http",
    "USER" : "svc-00432",
    "PASSWORD" : "****"
  }
  EOF

  lifecycle {
    ignore_changes         = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_nia_lrs_integration_secret_ac1" {
  description         = "Secret for NIA and LRS integration"
  name = "asm-parcels-np-ac1-euwe01-hn-events-tmm-user-detail-01"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-ac1-euwe01-hn-events-tmm-user-detail-01"
        }
}


resource "aws_secretsmanager_secret_version" "hn_nia_lrs_integration_secret_ac1" {
  secret_id     = aws_secretsmanager_secret.hn_nia_lrs_integration_secret_ac1.id
  secret_string = <<EOF
    {
    "endpoint" : "https://webservices.acbpost.be/ac2/trackedmail/hybridnetworkevents",
    "protocol" : "http",
    "USER" : "svc-00432",
    "PASSWORD" : "****"
  }
  EOF

  lifecycle {
    ignore_changes         = [secret_string]
  }
} 