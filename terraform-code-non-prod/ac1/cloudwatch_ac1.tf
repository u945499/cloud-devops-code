resource "aws_cloudwatch_log_group" "hn_service_svc_loggroup_ac1" {
  name = "/ecs/ctd-parcels-ac1-euwe01-hn-release-01"

  tags = merge(local.common_tags,map("Name","ctd-parcels-ac1-euwe01-hn-release-01"))
}
