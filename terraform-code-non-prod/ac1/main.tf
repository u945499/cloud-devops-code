resource "aws_sns_topic" "brokersnsac1" {
  name = "sns-hn-np-ac1-euwe01-common-events"
   lifecycle {
    create_before_destroy = true
  }
    kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
	
  tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-hn-np-ac1-euwe01-common-events"
  }
}
resource "aws_sns_topic" "brokersnsst1" {
  name = "sns-hn-np-st1-euwe01-push-brk"
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
  tags = {
          "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-hn-np-st1-euwe01-push-brk"
  }
}

resource "aws_kinesis_stream" "test_stream" {
  name             = "kst-parcels-np-ac2-euwe01-solystics-01"
  shard_count      = 2
  retention_period = 24
  encryption_type = "KMS"
  kms_key_id = "f103ca3b-705d-4725-9c70-9c2d3261fe3a"
  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
"ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
    Environment = "AC1"
  }
}



data "aws_iam_policy_document" "sfm-lambda-solystics-policy" {
    statement {
        actions = [
              "logs:CreateLogStream",
              "logs:PutLogEvents", 
              "logs:CreateLogGroup"            
        ]
        effect = "Allow"
        resources=["arn:aws:logs:eu-west-1:086243371668:log-group:/aws/codebuild/cd*-parcels-*-euwe01-hn-*"]
    }
    statement {
        actions = [
              "kms:Decrypt",
              "kms:GenerateDataKey"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "s3:*"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "secretsmanager:*"            
        ]
        effect = "Allow"
        resources=["*"]
    }
}
resource "aws_iam_policy" "sfm-lambda-solystics-policy" {
    name = "iap-parcels-np-euwe01-sfm-lambda-solystics-execution-role"  
    policy = data.aws_iam_policy_document.sfm-lambda-solystics-policy.json
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_custom" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = aws_iam_policy.sfm-lambda-solystics-policy.arn
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_kinesisFullAccess" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFullAccess"
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_LambdeKinesisExecution" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaKinesisExecutionRole"
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_KinesisAnalyticsFullAccess" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisAnalyticsFullAccess"
}

data "aws_iam_policy_document" "sfm-lambda-solystics-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "sfm-lambda-solystics-execution-role" {
  name = "iar-parcels-np-euwe01-sfm-lambda-solystics-execution-role"
  path = "/"
  assume_role_policy = data.aws_iam_policy_document.sfm-lambda-solystics-assume-role.json
}



resource "aws_lambda_function" "prod_lambda" {
  s3_bucket = "s3-parcels-hn-np-dv1-euwe01-solysticsconsumerlambda"
  s3_key = "lmb-parcels-np-euwe01-solystics-consumer-01/app.zip"
  function_name = "lmb-parcels-np-euwe01-solystics-consumer-02"
  role          = aws_iam_role.sfm-lambda-solystics-execution-role.arn
  handler       = "hn-solystics-kinesis-consumer/src/index.handler"
  
  
   runtime = "nodejs12.x"

  environment {
    variables = {

	AWS_API_VERSION	= "2012-11-05"
AWS_REGION_DEFAULT = "eu-west-1"
IMAGES_REQUEST_DELAY_MS ="1100"
IMAGES_WSG_URL	= "https://webservices.bpost.be/ws/mmt/solystic/getMailImages?idTag="
INTAKE_API_SECRET_KEY	= "scr-parcels-np-st1-euwe01-intake-api-detail"
INTAKE_EVENT_URL ="https://hn-api-st1.bposthn.net/event"
INTAKE_WSG_URL = "https://hn-api-st1.bposthn.net/eventwithbinary"
PARALLEL_PROCESSING	= "1"
PRODUCTXML_BUCKET_NAME =	"s3-parcels-hn-np-dv1-euwe01-solysticsconsumerlambda"
PRODUCTXML_CACHE =	"3600000"
PRODUCTXML_KEY	= "ProductXML.xml"
SOLYSTICS_IMAGES_API_SECRET_KEY	 = "asm-parcels-np-euwe01-solystics-01"
	  
	  
    }
  }
  }
  
  resource "aws_s3_bucket" "solysticsconsumerlambda-repo" {
  bucket = "s3-parcels-np-euwe01-repo-hn-solysticsconsumerlambda-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-parcels-pr-euwe01-log-hn-solysticsconsumerlambda-01" 
   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	
  }
		
 logging {
    target_bucket = aws_s3_bucket.solysticsconsumerlambda-audit.id
    target_prefix = "log/"
  } 
}
  
  
resource "aws_s3_bucket" "solysticsconsumerlambda-audit" {
  bucket = "s3b-parcels-np-euwe01-hn-solysticsconsumerlambda-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non-Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3-parcels-np-euwe01-hn-solysticsconsumerlambda-01" 
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
	
  }  
  resource "aws_lambda_function" "Test_lambda" {
  s3_bucket = "s3-parcels-np-euwe01-repo-hn-solysticsconsumerlambda-01"
  s3_key = "lmb-parcels-np-euwe01-solystics-consumer-03/app.zip"
  function_name = "lmb-parcels-np-euwe01-solystics-consumer-03"
  role          = aws_iam_role.sfm-lambda-solystics-execution-role.arn
  handler       = "hn-solystics-kinesis-consumer/src/index.handler"
  tags = {
"ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
    Environment = "AC1"
  }
  
   runtime = "nodejs12.x"

  environment {
    variables = {

	AWS_API_VERSION	= "2012-11-05"
AWS_REGION_DEFAULT = "eu-west-1"
IMAGES_REQUEST_DELAY_MS ="1100"
IMAGES_WSG_URL	= "https://webservices.bpost.be/ws/mmt/solystic/getMailImages?idTag="
INTAKE_API_SECRET_KEY	= "scr-parcels-np-st1-euwe01-intake-api-detail"
INTAKE_EVENT_URL ="https://hn-api-st1.bposthn.net/event"
INTAKE_WSG_URL = "https://hn-api-st1.bposthn.net/eventwithbinary"
PARALLEL_PROCESSING	= "1"
PRODUCTXML_BUCKET_NAME =	"s3-parcels-hn-np-dv1-euwe01-solysticsconsumerlambda"
PRODUCTXML_CACHE =	"3600000"
PRODUCTXML_KEY	= "ProductXML.xml"
SOLYSTICS_IMAGES_API_SECRET_KEY	 = "asm-parcels-np-euwe01-solystics-01"
	  
	  
    }
  }
  }
  
  
  resource "aws_codebuild_project" "solysticsconsumerlambda_deploy" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for lambda solystics consumer deploy"
       name           = "cbd-parcels-np-euwe01-hn-Solystics-02"
       queued_timeout = 480
       service_role   = "arn:aws:iam::086243371668:role/service-role/iar-sfm-codebuild"
       source_version = "master"
       lifecycle {
        prevent_destroy = true
      }
       tags      = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "Na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non-Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "cbd-parcels-np-euwe01-hn-Solystics-02"
        }

       artifacts {
           encryption_disabled    = false
           location = "s3-parcels-np-euwe01-repo-hn-solysticsconsumerlambda-01"
           name = "cbd-parcels-np-euwe01-hn-Solystics-02"
           namespace_type = "NONE"
           override_artifact_name = true
           packaging = "ZIP"
           path = "Code"
           type = "S3"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
           image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = false
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "user_bitbucket_id"
               type  = "PLAINTEXT"
               value = "Ramkrupakaran"
            }
           environment_variable {
               name  = "bitbucket_password"
               type  = "PLAINTEXT"
               value = "Bpost123"
            }
       }
           
       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
               group_name = "Solystics"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/non-pr/build-spec-non-pr-deploy.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://sidharth.gandhi.ext@bitbucket.org/bpost_deliveryparcelsint/hn-solystics-kinesis-consumer.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }

variable "tmm-nero-user-detail_var" {
  default = {
    endpoint = "http://test"
    protocol = "http"
    TMM_WSG_USER = "test"
    TMM_WSG_PASSWORD = "test"
  }
  type = map
}

variable "region_var" {
  type = string
  default ="euwe01"
} 
variable "cluster_var" {
  type = string
  default = "parcels"
}
variable "envContext_var" {
  type = string
  default = "np"
}
variable "env_var" {
  type = string
  default = "dv1"
}
variable "envContextAndEnv_var" {
  type = string
  default = "np-dv1"
}

resource "aws_secretsmanager_secret" "hn-tmm-nero-user-detail_secret" {
  name = join("-", ["asm", var.cluster_var, var.envContextAndEnv_var, var.region_var,"tmm-nero-user-detail"])
  description = "Secret for HN to NeRo API"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-dv1-euwe01-tmm-nero-user-detail"
        }
}


resource "aws_sqs_queue" "notificationsqsdv1" {
  name                      = "sqs-parcels-np-dv1-euwe01-notification-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  redrive_policy = jsonencode({
    deadLetterTargetArn = "arn:aws:sqs:eu-west-1:086243371668:sqs-parcels-np-dv1-euwe01-notification-dlq-01"
    maxReceiveCount     = 4
  })
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
 tags = {
          "ApplicationAcronym"    = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                  = "sqs-parcels-np-dv1-euwe01-notification-01"
  }
  }



resource "aws_sqs_queue" "notificationsqsst1" {
  name                      = "sqs-parcels-np-st1-euwe01-notification-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  redrive_policy = jsonencode({
    deadLetterTargetArn = "arn:aws:sqs:eu-west-1:086243371668:sqs-parcels-np-st1-euwe01-notification-dlq-01"
    maxReceiveCount     = 4
  })
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
 tags = {
          "ApplicationAcronym"    = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                  = "sqs-parcels-np-st1-euwe01-notification-01"
  }
  }
  
  resource "aws_sqs_queue" "notificationsqsdlqst1" {
  name                      = "sqs-parcels-np-st1-euwe01-notification-dlq-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
 tags = {
          "ApplicationAcronym"    = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                  = "sqs-parcels-np-st1-euwe01-notification-dlq-01"
  }
  }
  resource "aws_sqs_queue" "notificationsqsdlqdv1" {
  name                      = "sqs-parcels-np-dv1-euwe01-notification-dlq-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
 tags = {
          "ApplicationAcronym"    = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                  = "sqs-parcels-np-dv1-euwe01-notification-dlq-01"
  }
  }
   resource "aws_sqs_queue" "notificationsqsdlqac11" {
  name                      = "sqs-parcels-np-ac1-euwe01-notification-dlq-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
 tags = {
          "ApplicationAcronym"    = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                  = "sqs-parcels-np-ac1-euwe01-notification-dlq-01"
  }
  }
  
  resource "aws_sqs_queue" "notificationsqsac1" {
  name                      = "sqs-parcels-np-ac1-euwe01-notification-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
    redrive_policy = jsonencode({
    deadLetterTargetArn = "arn:aws:sqs:eu-west-1:086243371668:sqs-parcels-np-ac1-euwe01-notification-dlq-01"
    maxReceiveCount     = 4
  })
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
 tags = {
          "ApplicationAcronym"    = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                  = "sqs-parcels-np-ac1-euwe01-notification-01"
  }
  }



 resource "aws_s3_bucket" "dchlambdarepodv1" {
  bucket = "s3b-hn-parcels-dv1-euwe01-notification-dch-connector-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "dv1-st1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-np-euwe01-notification-dch-connector-repo-01" 
   
  }
  
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	}
  
  }
  
 resource "aws_s3_bucket" "incomingnotificationdv1" {
  bucket = "s3b-hn-parcels-dv1-euwe01-log-incomingnotification-messages-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "dv1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-np-euwe01-log-incomingnotification-messages-01" 
   
  }
   versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	}
   }
 resource "aws_s3_bucket" "dchpayloadmessagesdv1" {
  bucket = "s3b-hn-parcels-dv1-euwe01-log-dch-payloadmessages-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "dv1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-np-euwe01-log-dch-payloadmessages-01" 
   
  }
    versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }  
 }
 }
	
resource "aws_s3_bucket" "incomingnotificationst1" {
  bucket = "s3b-hn-parcels-st1-euwe01-log-incomingnotification-messages-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "st1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-np-euwe01-log-incomingnotification-messages-01" 
   
  }
   versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	}
   }
   resource "aws_s3_bucket" "dchpayloadmessagesst1" {
  bucket = "s3b-hn-parcels-st1-euwe01-log-dch-payloadmessages-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "st1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-np-euwe01-log-dch-payloadmessages-01" 
   
  }
    versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	}
}
resource "aws_s3_bucket" "incomingnotificationac1" {
  bucket = "s3b-hn-parcels-ac1-euwe01-log-incomingnotification-messages-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "ac1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-ac1-euwe01-log-incomingnotification-messages-01" 
   
  }
   versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	}
   }
   
 resource "aws_s3_bucket" "dchpayloadmessagesac1" {
  bucket = "s3b-hn-parcels-ac1-euwe01-log-dch-payloadmessages-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
          "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "st1 non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-ac1-euwe01-log-dch-payloadmessages-01" 
   
  }
    versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
}
}	

variable "tmm-nero-user-detail_var_st1" {
  default = {
   endpoint = "https://webservices.stbpost.be/track/retail/event"
    protocol = "https"
    TMM_WSG_USER = "svc-00995"
    TMM_WSG_PASSWORD = ""
  }
  type = map
}

variable "region_var_st1" {
  type = string
  default ="euwe01"
} 
variable "cluster_var_st1" {
  type = string
  default = "parcels"
}
variable "envContext_var_st1" {
  type = string
  default = "np"
}
variable "env_var_st1" {
  type = string
  default = "st1"
}
variable "envContextAndEnv_var_st1" {
  type = string
  default = "np-st1"
}

resource "aws_secretsmanager_secret" "hn-tmm-nero-user-detail_secret_st1" {
  name = join("-", ["asm", var.cluster_var_st1, var.envContextAndEnv_var_st1, var.region_var_st1,"tmm-nero-user-detail"])
  description = "Secret for HN to NeRo API_st1"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-st1-euwe01-tmm-nero-user-detail"
        }
}

variable "tmm-nero-user-detail_var_ac1" {
  default = {
  endpoint = "https://webservices.acbpost.be/track/retail/event"
    protocol = "https"
    TMM_WSG_USER = "svc-00995"
    TMM_WSG_PASSWORD = ""
  }
  type = map
}

variable "region_var_ac1" {
  type = string
  default ="euwe01"
} 
variable "cluster_var_ac1" {
  type = string
  default = "parcels"
}
variable "envContext_var_ac1" {
  type = string
  default = "np"
}
variable "env_var_ac1" {
  type = string
  default = "ac1"
}
variable "envContextAndEnv_var_ac1" {
  type = string
  default = "np-ac1"
}

resource "aws_secretsmanager_secret" "hn-tmm-nero-user-detail_secret_ac1" {
  name = join("-", ["asm", var.cluster_var_ac1, var.envContextAndEnv_var_ac1, var.region_var_ac1,"tmm-nero-user-detail"])
  description = "Secret for HN to NeRo API_ac1"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-ac1-euwe01-tmm-nero-user-detail"
        }
}




  
  resource "aws_lambda_function" "dch_connector_lambda_st1" {
  s3_bucket = "s3b-hn-parcels-dv1-euwe01-notification-dch-connector-repo-01"
  s3_key = "st1-publisher-connector/app.zip"
  function_name = "lmd-parcels-hn-st1-euwe01-notification-publisher-connector-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "build/src/index.handler"
  
  
   runtime = "nodejs12.x"
timeout =123
  environment {
    variables = {
AWS_REGION_DEFAULT     = "eu-west-1"
	AWS_API_VERSION	= "2012-11-05"
	NOTIFICATION_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-st1-euwe01-notification-01"
	  
	  
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "ST1 Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmd-parcels-hn-st1-euwe01-notification-publisher-connector-01"
        }
  }
  
  resource "aws_lambda_function" "dch_connector_lambda" {
  s3_bucket = "s3b-hn-parcels-dv1-euwe01-notification-dch-connector-repo-01"
  s3_key = "dv1_publisher-connector/app.zip"
  function_name = "lmd-parcels-hn-dv1-euwe01-notification-publisher-connector-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "build/src/index.handler"
  
  
   runtime = "nodejs12.x"
timeout = 123
  environment {
    variables = {
AWS_REGION_DEFAULT     = "eu-west-1"
	AWS_API_VERSION	= "2012-11-05"
	NOTIFICATION_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-dv1-euwe01-notification-01"
	  
	  
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmd-parcels-hn-dv1-euwe01-notification-publisher-connector-01"
        }
  }
  
 
 variable "hn-bis-api-st1" {
  default = {
  
    username = "svc-00729"
    password = "****"
  }
  type = map
}

resource "aws_secretsmanager_secret" "hn-bis-api-st1_secret" {
  name = "asm-parcels-np-dv1-euwe01-bis-user-detail-01"
  description = "Secret for hn-bis-api-dv1"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production dv1"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-dv1-euwe01-bis-user-detail-01"
        }
}

variable "hn-bis-api-st11" {
  default = {
  
    username = "svc-00729"
    password = "****"
  }
  type = map
}

resource "aws_secretsmanager_secret" "hn-bis-api-st11_secret" {
  name = "asm-parcels-np-st1-euwe01-bis-user-detail-01"
  description = "Secret for hn-bis-api-st1"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production st1"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-st1-euwe01-bis-user-detail-01"
        }
}



resource "aws_lambda_function" "notification_dch_connector_lambda_st1" {
  s3_bucket = "s3b-hn-parcels-dv1-euwe01-notification-dch-connector-repo-01"
  s3_key = "DV1_dch-connector/app.zip"
  function_name = "lmb-parcels-hn-dv1-euwe01-notification-dch-connector-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "hn-notification-service/src/NotificationPushLambda.handler"
  runtime = "nodejs12.x"
  timeout = 120 

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }
        
  environment {
    variables = {
            DCH_EMAIL_ADDRESS                     = "DCH_Team@bpost.be"
            COGNITO_SERVICE_USER_AWS_SECRET	= "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE	= "ddb-hn-parcels-dv1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET	= "s3b-hn-parcels-dv1-euwe01-log-dch-payloadmessages-01"
            DCH_SERVICE_SERVICE_URL =   "https://communicationhub.stbpost.be/DEV/sendnotifications"
            DCH_TRIGGER_ENABLED	= "true"
            #INCOMING_NOTIFICATION_EVENT_TABLE	= "ddb-hn-parcels-dv1-euwe01-incoming_notification_events-01"
            #NOTIFICATION_EVENT_CODE_CONFIG_TABLE	= "ddb-hn-parcels-dv1-euwe01-notification_event_code_configuration-01"
            NOTIFICATION_INCOMING_MESSAGES_BUCKET	= "s3b-hn-parcels-dv1-euwe01-log-incomingnotification-messages-01"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
            DB_NAME = "dv1_notification"
            DB_REPLICA_SECRET = "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
            TRACK_N_TRACE_URL = "https://track-dv1.bpost.cloud/btr/web"
            REQUEST_TIME_TO_LIVE = "3600"
            REQUEST_RETRY_COUNT = "1"
    }
	
  }
  
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "ST1 Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-hn-dv1-euwe01-notification-dch-connector-01"
        }
  }

  variable "hn-ac1-BIS-cred" {
  default = {
     endpoint = "https://webservices.stbpost.be/ws/bis-hn-items_ac2/event"
   protocol = "http"
   BIS_WSG_USER = "svc-00729"
    TMM_WSG_PASSWORD = "*"
  }
  type = map
}

  resource "aws_secretsmanager_secret" "bis-ac1_secret" {
  name = "asm-parcels-np-ac1-euwe01-bis-user-detail-01"
  description = "Secret for BIS"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "AC1 non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-ac1-euwe01-bis-user-detail-01"
        }
}

resource "aws_secretsmanager_secret_version" "HN-AC1-BIS_values" {
  secret_id     = aws_secretsmanager_secret.bis-ac1_secret.id
  secret_string = jsonencode(var.hn-ac1-BIS-cred)
  
}

#Logging bucket generic for all bucket of HN
resource "aws_s3_bucket" "hn-general-bucket-auditing" {
  bucket = "s3b-parcels-np-euwe01-hn-bucket-audits-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-euwe01-hn-bucket-audits-01"
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
}

#Repository bucket generic for all Lambda functions of HN
resource "aws_s3_bucket" "hn-general-lambda-repo" {
  bucket = "s3b-parcels-np-euwe01-hn-lambda-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-np-euwe01-hn-lambda-repo-01"   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }	
  }		
 logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = "s3b-parcels-np-euwe01-hn-lambda-repo-01"
  } 
}

resource "aws_lambda_function" "hn_lambda_filter_common_events_msg_for_NeRO_st1" {
  s3_bucket = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key = "lmb-parcels-np-st1-euwe01-hn-sns-msg-broker-neroevents-01/app.zip"
  function_name = "lmb-parcels-np-st1-euwe01-hn-sns-msg-broker-neroevents-01"
  role          = aws_iam_role.hn_lambda_filter_common_events_msg_for_NeRO-role.arn
  handler       = "NeroEventsSubscriptionLambda.handler"
  runtime = "nodejs12.x"

  environment {
    variables = {
	BROKER_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-st1-euwe01-brokerservice-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-st1-euwe01-hn-sns-msg-broker-neroevents-01"
        }
  }

  resource "aws_lambda_permission" "allow_old_foundation" {
  statement_id  = "AllowExecutionFromSNSOldFoundation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lambda_filter_common_events_msg_for_NeRO_st1.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-ST1-WE1-COMMON-EVENTS"
}

data "aws_iam_policy_document" "hn_lambda_filter_common_events_msg_NeRO-policydoc" {
    statement {
        actions = [
              "logs:CreateLogStream",
              "logs:PutLogEvents", 
              "logs:CreateLogGroup"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "kms:Decrypt",
              "kms:GenerateDataKey"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
                "sqs:SendMessage",
                "sqs:GetQueueAttributes"            
        ]
        effect = "Allow"
        resources=["*"]
    }
  }

data "aws_iam_policy_document" "hn_lambda_filter_common_events_msg_NeRO-assumerole-policydoc" {
   statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
   }
}

resource "aws_iam_policy" "hn_lambda_filter_common_events_msg_for_NeRO-policy" {
    name = "iap-parcels-np-euwe01-hn-lambda-NeRO-filtering-execution-role"  
    policy = data.aws_iam_policy_document.hn_lambda_filter_common_events_msg_NeRO-policydoc.json
}

resource "aws_iam_role_policy_attachment" "hn_lambda_filter_common_events_msg_for_NeRO_st1-policy_custom" {
  role       = aws_iam_role.hn_lambda_filter_common_events_msg_for_NeRO-role.name
  policy_arn = aws_iam_policy.hn_lambda_filter_common_events_msg_for_NeRO-policy.arn
}

resource "aws_iam_role" "hn_lambda_filter_common_events_msg_for_NeRO-role" {
  name = "iar-parcels-np-euwe01-hn-lambda-NeRO-filtering-execution-role"
  path = "/"
  assume_role_policy = data.aws_iam_policy_document.hn_lambda_filter_common_events_msg_NeRO-assumerole-policydoc.json
}

resource "aws_sns_topic_subscription" "hn_lambda_filter_common_events_msg_for_NeRO-subscription" {
  topic_arn = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-ST1-WE1-COMMON-EVENTS"
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lambda_filter_common_events_msg_for_NeRO_st1.arn
  filter_policy = "{ \"Origin\": [\"NeRO_NA\"] }"
}

# NOT WORKING. RECEIVING ACCESS DENIED ON SUBSCRIPTION
/* resource "aws_sns_topic_subscription" "hn_queue_broker_service_common_events-subscription" {
  topic_arn = "arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-ST1-WE1-COMMON-EVENTS"
  protocol  = "sqs"
  endpoint  = "arn:aws:sqs:eu-west-1:086243371668:sqs-parcels-np-st1-euwe01-brokerservice-01"
  raw_message_delivery = true
  filter_policy = "{\n  \"Origin\": [\n    \"bpost\"\n  ],\n  \"ProductCategory\":[\"REGISTERED_LETTER\"],\n  \"TransportCategory\":[\"INBOUND\"]\n}"
} */

variable "cognito-api-details-cred-01_var" {
  default = {
    #### Old credentials. Changed in between ...
    /*
    COGNITO_SERVICE_USER = "HybridNetwork" 
    COGNITO_SERVICE_PASSWORD = "Sachin@47" 
    COGNITO_SERVICE_URL = "https://sxh8egqjwb.execute-api.eu-west-1.amazonaws.com/UAT/gettoken"
    */
    X_API_KEY = "eIRPag0trj9cRIj1OSpkF1gYQwbBKayHJ0485TT1"
    APPLICATION_NAME = "HybridNetwork"
    ENDPOINT = "https://communicationhub.acbpost.be/ac2/fetchtoken"
  }
  type = map
}

resource "aws_secretsmanager_secret" "hn-cognito-api-details-cred-01_secret" {
  name = join("-", ["asm", var.cluster_var, var.envContext_var, var.region_var,"cognito-api-details-cred-01"])
  description = "Secret for HN to cognito API"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-euwe01-cognito-api-details-cred-01"
        }
}

resource "aws_secretsmanager_secret_version" "hn-tmm-cognito-api-details-cred-01_values" {
  secret_id     = aws_secretsmanager_secret.hn-cognito-api-details-cred-01_secret.id
  secret_string = jsonencode(var.cognito-api-details-cred-01_var)
}

resource "aws_lambda_function" "hn_lambda_filter_common_events_msg_for_NeRO_ac1" {
  s3_bucket = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key = "lmb-parcels-np-ac1-euwe01-hn-sns-msg-broker-neroevents-01/app.zip"
  function_name = "lmb-parcels-np-ac1-euwe01-hn-sns-msg-broker-neroevents-01"
  role          = aws_iam_role.hn_lambda_filter_common_events_msg_for_NeRO-role.arn
  handler       = "NeroEventsSubscriptionLambda.handler"
  runtime = "nodejs12.x"

  environment {
    variables = {
	BROKER_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-ac1-euwe01-brokerservice-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-ac1-euwe01-hn-sns-msg-broker-neroevents-01"
        }
  }

  resource "aws_lambda_permission" "allow_old_foundation_ac1" {
  statement_id  = "AllowExecutionFromSNSOldFoundation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lambda_filter_common_events_msg_for_NeRO_ac1.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.brokersnsac1.arn
}

resource "aws_sns_topic_subscription" "hn_lambda_filter_common_events_msg_for_NeRO_ac1-subscription" {
  topic_arn = aws_sns_topic.brokersnsac1.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lambda_filter_common_events_msg_for_NeRO_ac1.arn
  filter_policy = "{ \"Origin\": [\"NeRO_NA\"] }"
}

resource "aws_sns_topic_subscription" "hn_queue_broker_service_common_events_ac1-subscription" {
  topic_arn = aws_sns_topic.brokersnsac1.arn
  protocol  = "sqs"
  endpoint  = "arn:aws:sqs:eu-west-1:086243371668:sqs-parcels-np-ac1-euwe01-brokerservice-01"
  raw_message_delivery = true
  filter_policy = "{\n  \"Origin\": [\n    \"bpost\"\n  ],\n  \"ProductCategory\":[\"REGISTERED_LETTER\"],\n  \"TransportCategory\":[\"INBOUND\"]\n}"
}

resource "aws_lambda_function" "connector_lambda_ac1" {
  s3_bucket = "s3b-hn-parcels-ac1-euwe01-notification-publisher-repo-01"
  s3_key = "ac-publisher/app.zip"
  function_name = "lmd-parcels-hn-ac1-euwe01-notification-publisher-connector-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "build/src/index.handler"
  
  
   runtime = "nodejs12.x"
timeout =123
  environment {
    variables = {
AWS_REGION_DEFAULT     = "eu-west-1"
	AWS_API_VERSION	= "2012-11-05"
	NOTIFICATION_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-ac1-euwe01-notification-01"
	  
	  
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "ac1 Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmd-parcels-hn-ac1-euwe01-notification-publisher-connector-01"
        }
  }

resource "aws_lambda_function" "notification_dch_connector_lambda_st11" {
  s3_bucket = "s3b-hn-parcels-dv1-euwe01-notification-dch-connector-repo-01"
  s3_key = "ST1_dch-connector/app.zip"
  function_name = "lmb-parcels-hn-st1-euwe01-notification-dch-connector-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "hn-notification-service/src/NotificationPushLambda.handler"
  
  runtime = "nodejs12.x"

  timeout                        = 120 

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }
        
  environment {
    variables = {
            DCH_EMAIL_ADDRESS                     = "DCH_Team@bpost.be"
            COGNITO_SERVICE_USER_AWS_SECRET	= "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE	= "ddb-hn-parcels-st1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET	= "s3b-hn-parcels-st1-euwe01-log-dch-payloadmessages-01"
            DCH_SERVICE_SERVICE_URL =   "https://communicationhub.stbpost.be/DEV/sendnotifications" 
            DCH_TRIGGER_ENABLED	= "true" 
            #INCOMING_NOTIFICATION_EVENT_TABLE	= "ddb-hn-parcels-st1-euwe01-incoming_notification_events-01"
            #NOTIFICATION_EVENT_CODE_CONFIG_TABLE	= "ddb-hn-parcels-st1-euwe01-notification_event_code_configuration-01"
            NOTIFICATION_INCOMING_MESSAGES_BUCKET	= "s3b-hn-parcels-st1-euwe01-log-incomingnotification-messages-01"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
            DB_NAME = "st1_notification"
            DB_REPLICA_SECRET = "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"  
            TRACK_N_TRACE_URL = "https://track-st1.bpost.cloud/btr/web"
            REQUEST_TIME_TO_LIVE = "3600"
            REQUEST_RETRY_COUNT = "1"        
	    }
	}

  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "ST1 Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-hn-st1-euwe01-notification-dch-connector-01"
        }
  }

  resource "aws_s3_bucket" "dch-connector-repo-01" {
  bucket = "s3b-hn-parcels-ac1-euwe01-notification-dch-connector-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-ac1-euwe01-notification-dch-connector-repo-01" 
   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	
  }
		
 logging {
    target_bucket = aws_s3_bucket.dch-connector-log-01.id
    target_prefix = "log/"
  } 
}

resource "aws_s3_bucket" "dch-connector-log-01" {
  bucket = "s3b-hn-parcels-ac1-euwe01-notification-dch-connector-log-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non-Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-ac1-euwe01-notification-dch-connector-log-01" 
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
	
  }

  resource "aws_s3_bucket" "publisher-connector-repo-01" {
  bucket = "s3b-hn-parcels-ac1-euwe01-notification-publisher-repo-01"
  acl    = "private"
  tags = {
            "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "AC1 Non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-ac1-euwe01-notification-publisher-repo-01" 
   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
		}
    }
	
  }
		
 logging {
    target_bucket = aws_s3_bucket.dch-connector-log-01.id
    target_prefix = "log/"
  } 
}

resource "aws_s3_bucket" "publisher-connector-log-01" {
  bucket = "s3b-hn-parcels-ac1-euwe01-notification-publisher-log-01"
  acl    = "log-delivery-write"
   tags = {
            "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "AC1 Non Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-hn-parcels-ac1-euwe01-notification-publisher-log-01" 
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
        sse_algorithm     = "aws:kms"
      }
    }
	}
	
  }

  variable "hn-tmm-api-nia-dv1" {
  default = {
    endpoint = "http://tmi-dv2.netpost/rest/internal/hybridnetworkevents"
    protocol = "http"
  }
  type = map
}

  resource "aws_secretsmanager_secret" "hn-tmm-api-nia-dv1_secret" {
  name = "asm-parcels-np-dv1-euwe01-tmm-nia-user-detail-01"
  description = "Secret for TMM NIA user details"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-dv1-euwe01-tmm-nia-user-detail-01"
        }
}

resource "aws_secretsmanager_secret_version" "hn-tmm-api-nia-dv1_values" {
  secret_id     = aws_secretsmanager_secret.hn-tmm-api-nia-dv1_secret.id
  secret_string = jsonencode(var.hn-tmm-api-nia-dv1)
  
}

variable "hn-tmm-api-nia-st1" {
  default = {
    endpoint = "http://tmi-st2.netpost/rest/internal/hybridnetworkevents"
    protocol = "http"
  }
  type = map
}

  resource "aws_secretsmanager_secret" "hn-tmm-api-nia-st1_secret" {
  name = "asm-parcels-np-st1-euwe01-tmm-nia-user-detail-01"
  description = "Secret for TMM NIA user details"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-st1-euwe01-tmm-nia-user-detail-01"
        }
}

resource "aws_secretsmanager_secret_version" "hn-tmm-api-nia-st1_values" {
  secret_id     = aws_secretsmanager_secret.hn-tmm-api-nia-st1_secret.id
  secret_string = jsonencode(var.hn-tmm-api-nia-st1)
  
}

variable "hn-tmm-api-nia-ac1" {
  default = {
    endpoint = "http://tmi-ac2.netpost/rest/internal/hybridnetworkevents"
    protocol = "http"
  }
  type = map
}

  resource "aws_secretsmanager_secret" "hn-tmm-api-nia-ac1_secret" {
  name = "asm-parcels-np-ac1-euwe01-tmm-nia-user-detail-01"
  description = "Secret for TMM NIA user details"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-ac1-euwe01-tmm-nia-user-detail-01"
        }
}

resource "aws_secretsmanager_secret_version" "hn-tmm-api-nia-ac1_values" {
  secret_id     = aws_secretsmanager_secret.hn-tmm-api-nia-ac1_secret.id
  secret_string = jsonencode(var.hn-tmm-api-nia-ac1)
  
}

resource "aws_lambda_event_source_mapping" "hn_lambda_trigger_sqs_notif_dch_connector_dv1" {
  event_source_arn  = aws_sqs_queue.notificationsqsdv1.arn
  function_name     = aws_lambda_function.notification_dch_connector_lambda_st1.arn
}

resource "aws_lambda_event_source_mapping" "hn_lambda_trigger_sqs_notif_dch_connector_st1" {
  event_source_arn = aws_sqs_queue.notificationsqsst1.arn
  function_name    = aws_lambda_function.notification_dch_connector_lambda_st11.arn
}


  
###############lmb-parcels-hn-ac1-euwe01-notification-dch-connector-01

resource "aws_lambda_function" "notification_dch_connector_lambda_AC1" {
  s3_bucket = "s3b-hn-parcels-ac1-euwe01-notification-dch-connector-repo-01"
  s3_key = "AC1_dch-connector/app.zip"
  function_name = "lmb-parcels-hn-ac1-euwe01-notification-dch-connector-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "hn-notification-service/src/NotificationPushLambda.handler"
  runtime = "nodejs12.x"
  timeout                        = 120 

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        } 
        
  environment {
    variables = {
            DCH_EMAIL_ADDRESS                     = "DCH_Team@bpost.be"
            COGNITO_SERVICE_USER_AWS_SECRET	= "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE	= "ddb-hn-parcels-ac1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET	= "s3b-hn-parcels-ac1-euwe01-log-dch-payloadmessages-01"
            DCH_SERVICE_SERVICE_URL =   "https://communicationhub.acbpost.be/UAT/sendnotifications"
            DCH_TRIGGER_ENABLED	= "true" 
            #INCOMING_NOTIFICATION_EVENT_TABLE	= "ddb-hn-parcels-ac1-euwe01-incoming_notification_events-01"
            #NOTIFICATION_EVENT_CODE_CONFIG_TABLE	= "ddb-hn-parcels-ac1-euwe01-notification_event_code_configuration-01"
            NOTIFICATION_INCOMING_MESSAGES_BUCKET	= "s3b-hn-parcels-ac1-euwe01-log-incomingnotification-messages-01"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
            DB_NAME = "ac1_notification"
            DB_REPLICA_SECRET = "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"     
            TRACK_N_TRACE_URL = "https://track-ac1.bpost.cloud/btr/web"
            REQUEST_TIME_TO_LIVE = "3600"
            REQUEST_RETRY_COUNT = "1"        
	    }
  }

  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "ST1 Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-hn-ac1-euwe01-notification-dch-connector-01"
        }
  }

  resource "aws_lambda_event_source_mapping" "hn_lambda_trigger_sqs_notif_dch_connector_ac1" {
  event_source_arn = aws_sqs_queue.notificationsqsac1.arn
  function_name    = aws_lambda_function.notification_dch_connector_lambda_AC1.arn
  enabled = true
}
