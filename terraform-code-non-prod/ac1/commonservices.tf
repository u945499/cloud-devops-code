resource "aws_secretsmanager_secret" "hn_notif_rds_adm_secret" {
  description         = "Secret for RDS HN notification - RO user"
  name = "scr-parcels-np-euwe01-hn-notification-rds-adm-user-details-01"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "scr-parcels-np-euwe01-hn-notification-rds-adm-user-details-01"
        }
}
 
resource "aws_secretsmanager_secret_version" "hn_notif_rds_adm_secret" {
  depends_on      = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_adm_secret.id
  secret_string = <<EOF
    {
    "user": "notiadmin",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "xxx_notification"
  }
  EOF
 
  lifecycle {
    ignore_changes         = [secret_string]
  }
}