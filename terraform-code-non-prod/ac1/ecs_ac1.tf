
resource "aws_ecs_task_definition" "hn_release_svc_ac1" {
    execution_role_arn = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRole"])
    task_role_arn  = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRoleSFM"])
    family = join("-",[local.ctdName,"release-01"])
    requires_compatibilities = ["FARGATE"]
    network_mode = "awsvpc"
    cpu = 256
    memory = 512
    ######## INITIAL TASK DEFINITION ==> TO BE REDEPLOYED BY DEV ########
    container_definitions = <<TASK_DEFINITION
    [
        {
            "logConfiguration": {
                "logDriver": "awslogs",
                "secretOptions": null,
                "options": {
                    "awslogs-group": "/ecs/ctd-parcels-ac1-euwe01-hn-release-01",
                    "awslogs-region": "eu-west-1",
                    "awslogs-stream-prefix": "event-release-dv1"
                }
            },
            "cpu": 0,
            "environment": [
                {
                    "name": "AWS_DEFAULT_REGION",
                    "value": "eu-west-1"
                },
               
                {
                    "name": "DB_CONNECTION_LIMIT",
                    "value": "100"
                },
                {
                    "name": "DB_NAME",
                    "value": "ac1_notification"
                },
                {
                    "name": "DB_REPLICA_SECRET",
                    "value": "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-02"
                },
                {
                    "name": "LOG_LEVEL",
                    "value": "verbose"
                },
                {
                    "name": "MAPPER_CACHE",
                    "value": "3600000"
                },
                {
                    "name": "MAX_INFLIGHT_MESSAGES",
                    "value": "20"
                },
                
               {
                "name": "DB_MASTER_SECRET",
                "value": "scr-parcels-np-euwe01-hn-notification-rds-connection-details-01"
               },
                           
            {
                "name": "LOG_LEVEL",
                "value": "verbose"
            }
           
        ],

            "image": "086243371668.dkr.ecr.eu-west-1.amazonaws.com/ecr-hn-np-euwe01-release-01:AC1-572",
            "name": "Release"
        }       
    ]
    TASK_DEFINITION
    lifecycle {
        ignore_changes = [container_definitions]
    }
}

/* resource "aws_ecs_service" "hn_release_svc" {
    name            = join("-",[local.ecsName,"release-01"])
    cluster         = var.HNECSC
    task_definition = aws_ecs_task_definition.hn_release_svc.arn
    desired_count   = 1
    launch_type = "FARGATE"
    network_configuration {
        subnets = [
            var.TrustedSubnetAZ1,
            var.TrustedSubnetAZ2
        ]
        security_groups = [
            var.ECSSecurityGroup
        ] 
    }

    lifecycle {
        ignore_changes = [desired_count]
    }
}

 */
     