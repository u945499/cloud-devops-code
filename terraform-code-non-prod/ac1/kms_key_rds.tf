data "aws_caller_identity" "current" {}
output "account_id" {
  value = "data.aws_caller_identity.current.account_id"
}
data "aws_region" "current" {}
output "name" {
  value = "data.aws_region.current.name"
}
resource "aws_kms_key" "hn_rds_kms_key_dv1" {
  description             = "KMS key RDS database for HN"
  customer_master_key_spec= "SYMMETRIC_DEFAULT"
  key_usage               = "ENCRYPT_DECRYPT"
  enable_key_rotation     = "true"
  is_enabled              = "true"
policy = <<EOF
{
    "Id": "key-consolepolicy-3",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
            },
            "Action": "kms:*",
            "Resource": "*"
        },
        {
            "Sid": "Allow access for Key Administrators",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-MigrationTeam"
            },
            "Action": [
                "kms:Create*",
                "kms:Describe*",
                "kms:Enable*",
                "kms:List*",
                "kms:Put*",
                "kms:Update*",
                "kms:Revoke*",
                "kms:Disable*",
                "kms:Get*",
                "kms:Delete*",
                "kms:TagResource",
                "kms:UntagResource",
                "kms:ScheduleKeyDeletion",
                "kms:CancelKeyDeletion"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-ParcelDeveloper",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-Reader"
                ]
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow attachment of persistent resources",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-ParcelDeveloper",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-Reader"
                ]
            },
            "Action": [
                "kms:CreateGrant",
                "kms:ListGrants",
                "kms:RevokeGrant"
            ],
            "Resource": "*",
            "Condition": {
                "Bool": {
                    "kms:GrantIsForAWSResource": "true"
                }
            }
        }
    ]
}
EOF
  deletion_window_in_days = 7
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "kms-parcels-np-dv1-euwe01-hn-rds-01"
        }
}


resource "aws_kms_alias" "hn_rds_kms_key_alias_dv1" {
  depends_on      = [aws_kms_key.hn_rds_kms_key_dv1]
  name = "alias/kms-parcels-np-dv1-euwe01-hn-rds-01"
  target_key_id = aws_kms_key.hn_rds_kms_key_dv1.key_id
}