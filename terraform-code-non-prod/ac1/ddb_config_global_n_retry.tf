resource "aws_lambda_function" "hn_lmb_dch_retry_dv1" {
  s3_bucket = aws_s3_bucket.hn-general-dv1-lambda-repo.id
  s3_key = "lmb-parcels-np-dv1-euwe01-hn-notification-dch-retry-01/app.zip"
  function_name = "lmb-parcels-np-dv1-euwe01-hn-notification-dch-retry-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "build/src/index.handler"
  runtime = "nodejs12.x"
  timeout = 300

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }
        
  environment {
    variables = {
	          AWS_REGION_DEFAULT = "eu-west-1"
            #NOTIFICATION_GLOBAL_CONFIG_TABLE = aws_dynamodb_table.hn-ddb-notification-global-config-dv1.id
            #NOTIFICATION_RETRY_CONFIG_TABLE = "aws_dynamodb_table.hn-ddb-notification-retry-config-dv1.id"
            COGNITO_SERVICE_USER_AWS_SECRET = "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE = "ddb-hn-parcels-dv1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET = aws_s3_bucket.dchpayloadmessagesdv1.id
            DCH_SERVICE_URL = "https://communicationhub.stbpost.be/DEV/sendnotifications"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
            DB_NAME = "dv1_notification"
            DB_REPLICA_SECRET = "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-dv1-euwe01-hn-notification-dch-retry-01"
        }
  }

  resource "aws_lambda_permission" "allow_cw_rule_trigger_dv1" {
  statement_id  = "AllowExecutionFromCWR"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_dch_retry_dv1.function_name
  principal     = "events.amazonaws.com"
  source_arn    = "arn:aws:events:eu-west-1:086243371668:rule/cwr-parcels-np-dv1-euwe01-hn-schedule-lmb-notification-dch-retry"
}

  resource "aws_cloudwatch_event_rule" "hn_lmb_dch_retry_rule_dv1" {
    name        = "cwr-parcels-np-dv1-euwe01-hn-schedule-lmb-notification-dch-retry"
    schedule_expression =  "rate(10 minutes)"
}

  resource "aws_cloudwatch_event_target" "hn_lmb_dch_retry_schedule_dv1" {
    target_id = "hn_lmb_dch_retry_schedule_dv1"
    rule      = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule_dv1.name
    arn       = aws_lambda_function.hn_lmb_dch_retry_dv1.arn
  }

  resource "aws_lambda_function" "hn_lmb_dch_retry_st1" {
  s3_bucket = aws_s3_bucket.hn-general-st1-lambda-repo.id
  s3_key = "lmb-parcels-np-st1-euwe01-hn-notification-dch-retry-01/app.zip"
  function_name = "lmb-parcels-np-st1-euwe01-hn-notification-dch-retry-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "build/src/index.handler"
  runtime = "nodejs12.x"
  timeout = 300

 vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }

  environment {
    variables = {
	          AWS_REGION_DEFAULT = "eu-west-1"
            #NOTIFICATION_GLOBAL_CONFIG_TABLE = aws_dynamodb_table.hn-ddb-notification-global-config-dv1.id
            #NOTIFICATION_RETRY_CONFIG_TABLE = aws_dynamodb_table.hn-ddb-notification-retry-config-dv1.id
            COGNITO_SERVICE_USER_AWS_SECRET = "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE = "ddb-hn-parcels-st1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET = aws_s3_bucket.dchpayloadmessagesst1.id
            DCH_SERVICE_URL = "https://communicationhub.stbpost.be/DEV/sendnotifications"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
            DB_NAME = "st1_notification"
            DB_REPLICA_SECRET = "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-st1-euwe01-hn-notification-dch-retry-01"
        }
  }

resource "aws_cloudwatch_event_rule" "hn_lmb_dch_retry_rule_st1" {
    name        = "cwr-parcels-np-st1-euwe01-hn-schedule-lmb-notification-dch-retry"
    schedule_expression =  "rate(10 minutes)"
}

resource "aws_lambda_permission" "allow_cw_rule_trigger_st1" {
  statement_id  = "AllowExecutionFromCWR"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_dch_retry_st1.function_name
  principal     = "events.amazonaws.com"
  #source_arn    = "arn:aws:events:eu-west-1:086243371668:rule/cwr-parcels-np-st1-euwe01-hn-schedule-lmb-notification-dch-retry"
  source_arn =aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule_st1.arn
}



  resource "aws_cloudwatch_event_target" "hn_lmb_dch_retry_schedule_st1" {
    target_id = "hn_lmb_dch_retry_schedule_st1"
    rule      = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule_st1.name
    arn       = aws_lambda_function.hn_lmb_dch_retry_st1.arn
  }

  resource "aws_lambda_function" "hn_lmb_dch_retry_ac1" {
  s3_bucket = aws_s3_bucket.hn-general-ac1-lambda-repo.id
  s3_key = "lmb-parcels-np-ac1-euwe01-hn-notification-dch-retry-01/app.zip"
  function_name = "lmb-parcels-np-ac1-euwe01-hn-notification-dch-retry-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "build/src/index.handler"
  runtime = "nodejs12.x"
  timeout = 300

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }

  environment {
    variables = {
	          AWS_REGION_DEFAULT = "eu-west-1"
            COGNITO_SERVICE_USER_AWS_SECRET = "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET = aws_s3_bucket.dchpayloadmessagesac1.id
            DCH_SERVICE_URL = "https://communicationhub.stbpost.be/DEV/sendnotifications"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
            DB_NAME = "ac1_notification"
            DB_REPLICA_SECRET = "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-ac1-euwe01-hn-notification-dch-retry-01"
        }
  }

resource "aws_lambda_permission" "allow_cw_rule_trigger_ac1" {
  statement_id  = "AllowExecutionFromCWR"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_dch_retry_ac1.function_name
  principal     = "events.amazonaws.com"
  source_arn    = "arn:aws:events:eu-west-1:086243371668:rule/cwr-parcels-np-ac1-euwe01-hn-schedule-lmb-notification-dch-retry"
}

resource "aws_cloudwatch_event_rule" "hn_lmb_dch_retry_rule_ac1" {
    name        = "cwr-parcels-np-ac1-euwe01-hn-schedule-lmb-notification-dch-retry"
    schedule_expression =  "rate(10 minutes)"
}

  resource "aws_cloudwatch_event_target" "hn_lmb_dch_retry_schedule_ac1" {
    target_id = "hn_lmb_dch_retry_schedule_ac1"
    rule      = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule_ac1.name
    arn       = aws_lambda_function.hn_lmb_dch_retry_ac1.arn
  }