resource "aws_secretsmanager_secret" "hn_notif_rds_secret" {
  description = "Secret for RDS HN notification"
  #kms_key_id          = aws_kms_key.hn_rds_kms_key_dv1.arn
  name = "scr-parcels-np-euwe01-hn-notification-rds-connection-details-01"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-euwe01-hn-notification-rds-connection-details-01"
  }
}


resource "aws_secretsmanager_secret_version" "hn_notif_rds_secret" {
  # depends_on      = [random_password.rpassword_hn_rds_dv1]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_secret.id
  secret_string = <<EOF
    {
    "master_username": "notiadmin",
    "master_password": "NewDelhi#26",
    "username": "",
    "password": "",
    "engine" : "aurora",
    "host" : "",
    "port": "3306",
    "dbClusterIdentifier": ""
  }
  EOF
}


/* resource "random_password" "rpassword_hn_rds_dv1" {
  length = 16
  special = true
  override_special = "$-*"
} */

resource "aws_db_subnet_group" "hn_notif_rds_subnet_group" {
  name       = "rsn-parcels-np-euwe01-hn-notif-01"
  subnet_ids = ["subnet-0b43ff380a425d9aa", "subnet-05a33b81b6ea84ff4"]
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "rsn-parcels-np-euwe01-hn-notif-01"
  }
}

resource "aws_rds_cluster_parameter_group" "hn_notif_rds_pg" {
  name   = "rpg-parcels-np-euwe01-hn-notif-01"
  family = "aurora5.6"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "rpg-parcels-np-euwe01-hn-notif-01"
  }
}

resource "aws_security_group" "hn_notif_rds_security_group" {
  name   = "rsg-parcels-np-euwe01-hn-01"
  vpc_id = "vpc-03d2ff9bf50d3ff5b"
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.78.136.0/23", "10.78.134.0/23", "10.76.47.105/32", "10.71.120.62/32"]
  }
 ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.71.112.0/23","10.71.114.0/23"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "rsg-parcels-np-euwe01-hn-01"
  }
}

resource "aws_rds_cluster" "hn_notif_rds_aurora_mysql_serverless" {
  cluster_identifier              = "rdi-parcels-np-euwe01-hn-notification-01"
  engine                          = "aurora"
  engine_mode                     = "serverless"
  engine_version                  = "5.6.10a"
  availability_zones              = ["eu-west-1a", "eu-west-1b"]
  master_username                 = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret.secret_string)["master_username"]
  master_password                 = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret.secret_string)["master_password"]
  backup_retention_period         = 1
  preferred_backup_window         = "04:53-05:23"
  copy_tags_to_snapshot           = true
  final_snapshot_identifier       = "rdc-parcels-np-euwe01-hn-notification-01"
  skip_final_snapshot             = false
  preferred_maintenance_window    = "sat:00:01-sat:00:31"
  port                            = 3306
  vpc_security_group_ids          = [aws_security_group.hn_notif_rds_security_group.id]
  storage_encrypted               = true
  apply_immediately               = false
  deletion_protection             = true
  db_subnet_group_name            = aws_db_subnet_group.hn_notif_rds_subnet_group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.hn_notif_rds_pg.id
  kms_key_id                      = aws_kms_key.hn_rds_kms_key_dv1.arn
  scaling_configuration {
    auto_pause               = false
    max_capacity             = 8
    min_capacity             = 2
    seconds_until_auto_pause = 300
    timeout_action           = "RollbackCapacityChange"
  }
  lifecycle {
    ignore_changes = [availability_zones]
  }
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "rdi-parcels-np-euwe01-hn-notification-01"
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_rw_secret_dv1" {
  description = "Secret for RDS HN notification - RW user"
  name        = "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
  }
}

resource "aws_secretsmanager_secret_version" "hn_notif_rds_rw_secret_dv1" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_rw_secret_dv1.id
  secret_string = <<EOF
    {
    "user": "notidbwriteUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "dv1_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_ro_secret_dv1" {
  description = "Secret for RDS HN notification - RO user"
  name        = "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
  }
}

resource "aws_secretsmanager_secret_version" "hn_notif_rds_ro_secret_dv1" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_ro_secret_dv1.id
  secret_string = <<EOF
    {
    "user": "notidbroUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "dv1_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_rw_secret_st1" {
  description = "Secret for RDS HN notification - RW user"
  name        = "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
  }
}

resource "aws_secretsmanager_secret_version" "hn_notif_rds_rw_secret_st1" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_rw_secret_st1.id
  secret_string = <<EOF
    {
    "user": "notidbwriteUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "st1_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_ro_secret_st1" {
  description = "Secret for RDS HN notification - RO user"
  name        = "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"
  }
}

resource "aws_secretsmanager_secret_version" "hn_notif_rds_ro_secret_st1" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_ro_secret_st1.id
  secret_string = <<EOF
    {
    "user": "notidbroUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "st1_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_rw_secret_ac1" {
  description = "Secret for RDS HN notification - RW user"
  name        = "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
  }
}

resource "aws_secretsmanager_secret_version" "hn_notif_rds_rw_secret_ac1" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_rw_secret_ac1.id
  secret_string = <<EOF
    {
    "user": "notidbwriteUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "ac1_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_ro_secret_ac1" {
  description = "Secret for RDS HN notification - RO user"
  name        = "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"
  tags = {
    "ApplicationAcronym"   = "HN"
    "ApplicationName"      = "Hybrid Network"
    "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
    "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
    "Backup"               = "False"
    "CloudServiceProvider" = "AWS"
    "DRLevel"              = "2"
    "DataProfile"          = "Confidential"
    "Environment"          = "Non Production"
    "ManagedBy"            = "TCS"
    "Name"                 = "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"
  }
}

resource "aws_secretsmanager_secret_version" "hn_notif_rds_ro_secret_ac1" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_ro_secret_ac1.id
  secret_string = <<EOF
    {
    "user": "notidbroUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "ac1_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "mysql_user" "hn_rds_notification_ro_user" {
  user               = "notidbroUser1"
  host               = "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com"
  plaintext_password = "readuser@123"
}
