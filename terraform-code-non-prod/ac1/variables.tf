variable "lambdaExecutionRole" {
    type=string
    description="this variable is used to execute the lambda  in non prod (dv1,st1,ac1)"
    default="arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
}

variable "kmsMasterKeyID" {
  type=string
    description="this variable is used to associate KMS key to s3 bucket"
    default="arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
}

variable "kms" {
  type=string
  default="aws:kms"
  
}

variable "dbName" {
  type=map
  default= {
    dv1: "dv1_notification"
    st1: "st1_notification"
    ac1: "ac1_notification"
  }
}

variable "dbMasterSecret" {
  type=map
  default= {
    dv1: "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
    st1: "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
    ac1: "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
  }
}

variable "dbReplicaSet" {
  type=map
  default= {
    dv1: "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
    st1: "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"
    ac1: "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"
  }
}
 
variable "lambdaHandler" {
  type=string
  default="hn-notification-feedback-service/src/FeedbackLambda.handler"
}


variable "s3bucketacl" {
  type=string
  default="private"
  
}

variable "keyType"{
  type=string
  default="API_KEY"
}
