######################### NAMING CONVENTION #######################
resourceType = {secretmanager :"asm", lambda : "lmb", s3bucket : "s3b", snstopic: "sns",apigateway: "apg",apiusage:"apu",apikey:"apk",
sqsqueue: "sqs",
    ecstaskdef: "ctd",
    ecssvc: "ecs"
    }
appcluster=["parcels"]
envNamingConvention = ["np"]
categoryEnvironment = ["ac1"] 
region= "euwe01"

#######################TAGGING ######################
applicationAcronym= "HN"
applicationName= "Hybrid Network"
applicationOwner = "Pieter.BISSCHOPS@bpost.be"
applicationSupport = "SindhyaEsther.Selvin.ext@bpost.be"
backups = "False"
cloudServiceProvider="AWS"
drLevel = "2"
dataProfile= "Confidential"  #Confidential
envTaggingConvention= "Non Production" #for production can use =Prod
managedBy = "TCS"
#for which aws service the resource will be consume for exmaple s3 bucket consuming by DynamoDB then add as "hn-s3"
#s3b Naming Convention <resource type>-<app cluster>-<environment>-<region>-<<application acronym>-<sequence number>
####### NAMING AND TAGGING CONVENTION VARIABLES ###########################
/* Names and Acronyms to add while using terraform vars file
Resource Type{ secretmanager :"asm", lambda : "lmb", s3bucket : "s3b", snstopic: "sns"} -->Can add to map one by one once started using further in the scripts. 
envNamingConvention = ["np"]  --> np,pr1
categoryEnvironment = ["ac1"]  --> dv1 for dev,st1 for system testing,ac1 for acceptance testing.
appcluster=["parcels"]  --> for PALO ==>parcels
region= "euwe01"  --> AWS region using
sequencenumber= 1 --->Increment as per the requirement
applicationAcronym= "HN" --->for Hybrid Network we are using as HN
applicationName= "Hybrid Network"  ---> 
applicationOwner = "Pieter.BISSCHOPS@bpost.be" ---> as mentioned for HN
applicationSupport = "SindhyaEsther.Selvin.ext@bpost.be" -->as mentioned for HN
backups = "False" --?as per taggng standards
cloudServiceProvider="AWS" --
drLevel = "2"
dataProfile= "Confidential"
envTaggingConvention= "Non Prod" #for production can use =Prod
managedBy = "TCS"
servicePurpose= "feedback"   #add prupose for resource,for example lambda creating for DB access can be suffix with "dbaccess"
targetService= "hn-intake-api" #for which aws service the resource will be consume for exmaple s3 bucket consuming by DynamoDB then add as "hn-s3"

*/

############# RESOURCE VARIABLES ########################
lambdaExecutionRole="arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
kmsMasterKeyID= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"



