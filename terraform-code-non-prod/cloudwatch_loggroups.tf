resource "aws_cloudwatch_log_group" "hn_service_svc_loggroup_dv1" {
  name = "/ecs/ctd-parcels-dv1-euwe01-hn-release-01"

  tags = merge(local.common_tags,map("Name","ctd-parcels-dv1-euwe01-hn-release-01"))
}