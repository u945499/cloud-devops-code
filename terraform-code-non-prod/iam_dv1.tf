resource "aws_iam_role" "iar-ParcelDeveloper" {
  name = "iar-ParcelDeveloper1"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = merge(local.common_tags,map("Name",join(local.iarName,["iar-ParcelDeveloper-role1"])))
}