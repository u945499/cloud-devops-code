#################### ROLE  FOR LAMBDA FUNCTION # ################
####iar-parcels-np-euwe01-hn-lambda--notification-role ####### needs to be imported

############################DATABASES ###############################
resource "aws_rds_cluster" "hn_notif_rds_aurora_mysql_serverless_ac2" {
  
  cluster_identifier              = join("-",[local.rdsName,"notification-01"])
  engine                          = var.rdsengine
  engine_mode                     = var.engine_mode
  engine_version                  = var.engine_version
  availability_zones              = var.availability_zones
  master_username                 = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret_ac2.secret_string)["master_username"]
  master_password                 = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret_ac2.secret_string)["master_password"]
  backup_retention_period         = var.db_backup_retention_period
  preferred_backup_window         = var.db_backup_window
  copy_tags_to_snapshot           = var.copytagstosnapshot
  final_snapshot_identifier       = join("-",[local.rdsName,"rdc-notification-01"])
  skip_final_snapshot             = var.skipfinalsnapshot
  preferred_maintenance_window    = var.maintenance_window
  port                            = var.rdsport[0]
  vpc_security_group_ids          = [aws_security_group.hn_notif_rds_security_group_ac2.id]
  storage_encrypted               = true
  apply_immediately               = false  #######needs to check this
  deletion_protection             = var.deletion_protection
  db_subnet_group_name            = aws_db_subnet_group.hn_notif_rds_subnet_group_ac2.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.hn_notif_rds_pg_ac2.id
  kms_key_id                      = aws_kms_key.hn_rds_kms_key_ac2.arn
  scaling_configuration {
    auto_pause               = false
    max_capacity             = 8
    min_capacity             = 2
    seconds_until_auto_pause = 300
    timeout_action           = "RollbackCapacityChange"
  }
  lifecycle {
    ignore_changes = [availability_zones]
  }
  tags = merge(local.common_tags,map("Name",join("-",[local.rdsName,"notification-01"])))
}


################### RDS SECRETS #############################
resource "aws_secretsmanager_secret" "hn_notif_rds_secret_ac2" {
  description = "Secret for RDS HN notification for production"
  #kms_key_id          = aws_kms_key.hn_rds_kms_key_dv1.arn
  name = join("-",[local.asmName,"notification-rds-connection-details-01"])
  tags =  merge(local.common_tags,map("Name",join("-",[local.asmName,"notification-rds-connection-details-01"])))
   #"scr-parcels-np-euwe01-hn-notification-rds-connection-details-01"
  }


resource "aws_secretsmanager_secret_version" "hn_notif_rds_secret_ac2" {
  # depends_on      = [random_password.rpassword_hn_rds_dv1]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_secret_ac2.id
  secret_string = jsonencode(var.rdsdbsecretStringforConnection)
}

resource "aws_secretsmanager_secret" "hn_notif_rds_rw_secret_ac2" {
  description = "Secret for RDS HN notification - RW user"
  name = join("-",[local.asmName,"notification-rds-rw-user-details-02"])
  tags =  merge(local.common_tags,map("Name",join("-",[local.asmName,"notification-rds-rw-user-details-02"])))
  
}


resource "aws_secretsmanager_secret_version" "hn_notif_rds_rw_secret_ac2" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless_ac2]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_rw_secret_ac2.id
  secret_string = jsonencode(var.rdsdbsecretStringforReadWriteUser)

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_ro_secret_ac2" {
  description = "Secret for RDS HN notification - RO user"
   name = join("-",[local.asmName,"notification-rds-ro-user-details-02"])
  tags =  merge(local.common_tags,map("Name",join("-",[local.asmName,"notification-rds-ro-user-details-02"])))
  }

resource "aws_secretsmanager_secret_version" "hn_notif_rds_ro_secret_ac2" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless_ac2]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_ro_secret_ac2.id
  secret_string = <<EOF
    {
    "user": "notidbroUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-ac2-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-ac2-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "ac2_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}


resource "random_password" "rpassword_hn_rds" {
  length = 16
  special = true
  override_special = "$-*"
}

############################### RDS CONNECTIONS ######################

resource "aws_db_subnet_group" "hn_notif_rds_subnet_group_ac2" {
  name = join("-",[local.dbsubnetName,"notification-01"])
   subnet_ids = var.db_subnet_ids
  tags =  merge(local.common_tags,map("Name",join("-",[local.dbsubnetName,"notification-01"])))
 }


resource "aws_rds_cluster_parameter_group" "hn_notif_rds_pg_ac2" {
  name   = join("-",[local.dbcpgName,"notification-01"])
  family = var.dbrdsfamily
  tags =  merge(local.common_tags,map("Name",join("-",[local.dbcpgName,"notification-01"])))
  
}

resource "aws_security_group" "hn_notif_rds_security_group_ac2" {
   name = join("-",[local.dbsgName,"01"])
  vpc_id = var.vpc_id[0] 
  
    ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.78.136.0/23", "10.78.134.0/23", "10.76.47.105/32", "10.71.120.62/32"]
  }
 ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.71.112.0/23","10.71.114.0/23"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags =merge(local.common_tags,map("Name",join("-",[local.dbsgName,"01"])))
}


################### S3 BUCKETS ####################
resource "aws_s3_bucket" "incomingnotification_ac2" {
  bucket = join("-",[local.s3bName,"logincomingnotif-messages-01"])
  acl    = "private"
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"logincomingnotif-messages-01"])))
     versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
		}
    }
	}
   }

   resource "aws_s3_bucket" "dchpayloadmessages_ac2" {
  bucket = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  acl    = "private"
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"log-dch-payloadmessages-01"])))
    versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
		}
    }
}
logging {
    target_bucket = aws_s3_bucket.hn-general-ac2-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  }
}

resource "aws_s3_bucket_object" "hn_s3key_notification_dch_retry_ac2" {
  key        = join("/",[join("-",[local.lmbName,"notification-dch-retry-01"]),"app.zip"])
  bucket     = aws_s3_bucket.hn-general-ac2-lambda-repo.id
  source     = "./dch-retry/app.zip"
  kms_key_id = var.kmsMasterKeyID
}

resource "aws_s3_bucket_object" "hn_s3key_notification_dch_connector_ac2" {
  key        =  join("/",[join("-",[local.lmbName,"notification-dch-connector-01"]),"app.zip"])
  bucket     = aws_s3_bucket.hn-general-ac2-lambda-repo.id
 source     = "./dch-connector/app.zip"
  kms_key_id = var.kmsMasterKeyID
}

resource "aws_s3_bucket_object" "hn-s3key_notification-publisher_ac2" {
  key        =  join("/",[join("-",[local.lmbName,"notification-dch-publisher-01"]),"app.zip"])
  bucket     = aws_s3_bucket.hn-general-ac2-lambda-repo.id
 source     = "./dch-publisher/app.zip"
  kms_key_id = var.kmsMasterKeyID
}

######################### LAMBDA FOR JUNE #################
resource "aws_lambda_function" "hn_lmb_dch_retry_ac2" {
  s3_bucket = aws_s3_bucket.hn-general-ac2-lambda-repo.id
  s3_key = aws_s3_bucket_object.hn_s3key_notification_dch_retry_ac2.id
  function_name = join("-",[local.lmbName,"notification-dch-retry-01"])
  role          = var.lambdaExecutionRole
  handler       = "build/src/index.handler"
  runtime = "nodejs12.x"
  timeout = 300

 vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }

  environment {
    variables = {
	        AWS_REGION_DEFAULT = "eu-west-1"
            #NOTIFICATION_GLOBAL_CONFIG_TABLE = aws_dynamodb_table.hn-ddb-notification-global-config-dv1.id
            #NOTIFICATION_RETRY_CONFIG_TABLE = aws_dynamodb_table.hn-ddb-notification-retry-config-dv1.id
            COGNITO_SERVICE_USER_AWS_SECRET = "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE = "ddb-hn-parcels-st1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET = aws_s3_bucket.dchpayloadmessages_ac2.id
            DCH_SERVICE_URL = "https://communicationhub.stbpost.be/DEV/sendnotifications"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = var.dbMasterSecret["ac2"]
            DB_NAME = var.dbName["ac2"]
            DB_REPLICA_SECRET = var.dbReplicaSet["ac2"]
        
            
    }
  }
  tags      = merge(local.common_tags,map("Name",join("-",[local.lmbName,"notification-dch-retry-01"])))
  }



resource "aws_lambda_function" "hn_lmb_notification_dch_connector_ac2" {
  s3_bucket = aws_s3_bucket.hn-general-ac2-lambda-repo.id
  s3_key = aws_s3_bucket_object.hn_s3key_notification_dch_connector_ac2.id
  #"DV1_dch-connector/app.zip"
  function_name = join("-",[local.lmbName,"notification-dch-connector-01"])
  role          = var.lambdaExecutionRole
  handler       = "hn-notification-service/src/NotificationPushLambda.handler"
  runtime = "nodejs12.x"
  timeout = 120 

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }
        
  environment {
    variables = {
            DCH_EMAIL_ADDRESS                     = "DCH_Team@bpost.be"
            COGNITO_SERVICE_USER_AWS_SECRET	= "asm-parcels-np-euwe01-cognito-api-details-cred-01"
            #DCH_NOTIFICATION_SENT_TRACKING_TABLE	= "ddb-hn-parcels-dv1-euwe01-dch_notification_sent_tracking-01"
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET	= aws_s3_bucket.dchpayloadmessages_ac2.id
            DCH_SERVICE_SERVICE_URL =   "https://communicationhub.stbpost.be/DEV/sendnotifications"
            DCH_TRIGGER_ENABLED	= "true"
            #INCOMING_NOTIFICATION_EVENT_TABLE	= "ddb-hn-parcels-dv1-euwe01-incoming_notification_events-01"
            #NOTIFICATION_EVENT_CODE_CONFIG_TABLE	= "ddb-hn-parcels-dv1-euwe01-notification_event_code_configuration-01"
            NOTIFICATION_INCOMING_MESSAGES_BUCKET	= aws_s3_bucket.incomingnotification_ac2.id
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = var.dbMasterSecret["ac2"]
            DB_NAME = var.dbName["ac2"]
            DB_REPLICA_SECRET = var.dbReplicaSet["ac2"]
            TRACK_N_TRACE_URL = "https://track-ac2.bpost.cloud/btr/web"
            ###NEEDS TO CHECK TNT URL
            REQUEST_TIME_TO_LIVE = "3600"
            REQUEST_RETRY_COUNT = "1"
    }
	
  }
    tags  = merge(local.common_tags,map("Name",join("-",[local.lmbName,"notification-dch-connector-01"]))) 
      
  }

resource "aws_lambda_function" "hn_lmb_notification_dch_publisher_ac2" {
  s3_bucket = aws_s3_bucket.hn-general-ac2-lambda-repo.id
  s3_key =aws_s3_bucket_object.hn-s3key_notification-publisher_ac2.id
   #"ac-publisher/app.zip"
  function_name = join("-",[local.lmbName,"notification-publisher-connector-01"])
  role          = var.lambdaExecutionRole
  handler       = "build/src/index.handler"
  
  
   runtime = "nodejs12.x"
timeout =120
  environment {
    variables = {
    AWS_REGION_DEFAULT     = "eu-west-1"
	AWS_API_VERSION	= "2012-11-05"
	NOTIFICATION_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-ac2-euwe01-notification-01"
	  
	  
    }
  }
  tags  = merge(local.common_tags,map("Name",join("-",[local.lmbName,"notification-publisher-connector-01"]))) 
 
  }

#########################CLOUD WATCH ############################
resource "aws_cloudwatch_event_rule" "hn_lmb_dch_retry_rule_ac2" {
    name        = join("-",[local.cwrName,"schedule-lmb-notification-dch-retry"])
    schedule_expression =  "rate(10 minutes)"
}

resource "aws_lambda_permission" "allow_cw_rule_trigger_ac2" {
  statement_id  = "AllowExecutionFromCWR"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_dch_retry_ac2.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule_ac2.arn
  #"arn:aws:events:eu-west-1:086243371668:rule/cwr-parcels-np-st1-euwe01-hn-schedule-lmb-notification-dch-retry"
}



  resource "aws_cloudwatch_event_target" "hn_lmb_dch_retry_schedule_ac2" {
    target_id = "hn_lmb_dch_retry_schedule_ac2"
    rule      = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule_ac2.name
    arn       = aws_lambda_function.hn_lmb_dch_retry_ac2.arn
  }

  resource "aws_lambda_permission" "notification-publisher-connector-SNS-trigger-ac2" {
    statement_id = "AllowExecutionFromSNS"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.hn_lmb_notification_dch_publisher_ac2.arn
    #"arn:aws:lambda:eu-west-1:086243371668:function:lmd-parcels-hn-ac1-euwe01-notification-publisher-connector-01"
    principal = "sns.amazonaws.com"
    source_arn = aws_sns_topic.hn_common_events.arn
    #"arn:aws:sns:eu-west-1:086243371668:sns-hn-np-ac2-euwe01-common-events"
}

######################SQS NOTIFICATIONS ############################
resource "aws_sqs_queue" "sqs_hn_notificationsqs_ac2" {
  name                      = join("-",[local.sqsName,"notification-01"])
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  redrive_policy = jsonencode({
    deadLetterTargetArn =  aws_sqs_queue.hn_sqs_iampolicy_notification_dlq_ac2.arn
    maxReceiveCount     = 4
  })
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= var.kmsMasterKeyID
 tags = merge(local.common_tags,map("Name",join("-",[local.sqsName,"notification-01"])))
  }

data "aws_iam_policy_document" "hn_sqs_iampolicy_notificationsqs_ac2" {
    policy_id = join("/", [aws_sqs_queue.sqs_hn_notificationsqs_ac2.arn, "SQSDefaultPolicy"])
    statement {
        
        actions = ["SQS:*"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers = ["*"]
        }
        resources=[aws_sqs_queue.sqs_hn_notificationsqs_ac2.arn]
        condition {
                   test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lmb_notification_dch_publisher_ac2.arn]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_sqs_policy_notificationsqs_ac2" {
  queue_url = aws_sqs_queue.sqs_hn_notificationsqs_ac2.id
  policy = data.aws_iam_policy_document.hn_sqs_iampolicy_notificationsqs_ac2.json
}

resource "aws_sqs_queue" "hn_sqs_iampolicy_notification_dlq_ac2" {
  name                      = join("-",[local.sqsName,"notification-dlq-01"])
  delay_seconds             = 30
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 120
  max_message_size          = 262144
  kms_master_key_id                 = var.kmsMasterKeyID
  kms_data_key_reuse_period_seconds = 3600

 tags = merge(local.common_tags,map("Name",join("-",[local.sqsName,"notification-01"])))
}


  ##################### event source maapin for SQS #####################################
  resource "aws_lambda_event_source_mapping" "hn_lambda_trigger_sqs_notif_dch_connector_ac2" {
  event_source_arn  = aws_sqs_queue.sqs_hn_notificationsqs_ac2.arn
  function_name     = aws_lambda_function.hn_lmb_notification_dch_connector_ac2.arn
}

  #############################secrets ####################
  resource "aws_secretsmanager_secret" "hn-cognito-api-details-cred-01_secret_ac2" {
  name = join("-",[local.asmName,"notification-01"])
  description = "Secret for HN to cognito API"
   tags = merge(local.common_tags,map("Name",join("-",[local.asmName,"cognito-api-details-cred-01"])))
  }

#######################KMS #####################

resource "aws_kms_key" "hn_rds_kms_key_ac2" {
  description             = "KMS key RDS database for HN"
  customer_master_key_spec= "SYMMETRIC_DEFAULT"
  key_usage               = "ENCRYPT_DECRYPT"
  enable_key_rotation     = "true"
  is_enabled              = "true"
policy = <<EOF
{
    "Id": "key-consolepolicy-3",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
            },
            "Action": "kms:*",
            "Resource": "*"
        },
        {
            "Sid": "Allow access for Key Administrators",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-MigrationTeam"
            },
            "Action": [
                "kms:Create*",
                "kms:Describe*",
                "kms:Enable*",
                "kms:List*",
                "kms:Put*",
                "kms:Update*",
                "kms:Revoke*",
                "kms:Disable*",
                "kms:Get*",
                "kms:Delete*",
                "kms:TagResource",
                "kms:UntagResource",
                "kms:ScheduleKeyDeletion",
                "kms:CancelKeyDeletion"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-ParcelDeveloper",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-Reader"
                ]
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow attachment of persistent resources",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-ParcelDeveloper",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-Reader"
                ]
            },
            "Action": [
                "kms:CreateGrant",
                "kms:ListGrants",
                "kms:RevokeGrant"
            ],
            "Resource": "*",
            "Condition": {
                "Bool": {
                    "kms:GrantIsForAWSResource": "true"
                }
            }
        }
    ]
}
EOF
  deletion_window_in_days = 7
 tags = merge(local.common_tags,map("Name",join("-",[local.kmsName,"rds-01"])))
}


resource "aws_kms_alias" "hn_rds_kms_key_alias_ac2" {
  depends_on      = [aws_kms_key.hn_rds_kms_key_ac2]
  # name = join("alias\",join("-",[local.kmsName,"rds-01"]))
  name="alias/kms-parcels-pr-euwe01-hn-rds-01"
  target_key_id = aws_kms_key.hn_rds_kms_key_ac2.key_id
}

#################### SNS ##################### NO COMMON EVENTS CHANGES IN PRODUCTIONS##############

resource "aws_sns_topic_subscription" "hn_sns_ac2_publisher_01" {
  topic_arn = aws_sns_topic.hn_common_events.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lmb_notification_dch_publisher_ac2.arn
 
}


###############################ECS RELEASE SERVICE #########################
resource "aws_ecs_task_definition" "hn_release_svc_ac2" {
    execution_role_arn = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRole"])
    task_role_arn  = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRoleSFM"])
    family = join("-",[local.ctdName,"release-01"])
    requires_compatibilities = ["FARGATE"]
    network_mode = "awsvpc"
    cpu = 256
    memory = 512
    ######## INITIAL TASK DEFINITION ==> TO BE REDEPLOYED BY DEV ########
    container_definitions = <<TASK_DEFINITION
    [
        {
            "logConfiguration": {
                "logDriver": "awslogs",
                "secretOptions": null,
                "options": {
                    "awslogs-group": "/ecs/ctd-parcels-ac2-euwe01-hn-release-01",
                    "awslogs-region": "eu-west-1",
                    "awslogs-stream-prefix": "event-release-ac2"
                }
            },
            "cpu": 0,
            "environment": [
                {
                    "name": "AWS_DEFAULT_REGION",
                    "value": "eu-west-1"
                },
               
                {
                    "name": "DB_CONNECTION_LIMIT",
                    "value": "100"
                },
                {
                    "name": "DB_NAME",
                    "value": "sc2_notification"
                },
                {
                    "name": "DB_REPLICA_SECRET",
                    "value": "scr-parcels-np-ac2-euwe01-hn-notification-rds-ro-user-details-02"
                },
                {
                    "name": "LOG_LEVEL",
                    "value": "verbose"
                },
                {
                    "name": "MAPPER_CACHE",
                    "value": "3600000"
                },
                {
                    "name": "MAX_INFLIGHT_MESSAGES",
                    "value": "20"
                },
                
               {
                "name": "DB_MASTER_SECRET",
                "value": "scr-parcels-np-ac2-euwe01-hn-notification-rds-connection-details-01"
               },
                           
            {
                "name": "LOG_LEVEL",
                "value": "verbose"
            }
           
        ],

            "image": "086243371668.dkr.ecr.eu-west-1.amazonaws.com/ecr-hn-np-euwe01-release-01:AC2-572",
            "name": "Release"
        }       
    ]
    TASK_DEFINITION
    lifecycle {
        ignore_changes = [container_definitions]
    }
}

/*
resource "aws_ecs_service" "hn_release_svc_ac2" {
    name            = join("-",[local.ecsName,"release-01"])
    cluster         = var.HNECSC
    task_definition = aws_ecs_task_definition.hn_release_svc_ac2.arn
    desired_count   = 1
    launch_type = "FARGATE"
    network_configuration {
        subnets = [
            var.TrustedSubnetAZ1,
            var.TrustedSubnetAZ2
        ]
        security_groups = [
            var.ECSSecurityGroup
        ] 
    }

    lifecycle {
        ignore_changes = [desired_count]
    }
}
*/
