resource "aws_sqs_queue" "hn_sqs_push_in" {
  name                      = join("-",[local.sqsName,"push-in"])
  delay_seconds             = 10
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 120
  max_message_size          = 262144
  kms_master_key_id                 = var.kmsMasterKeyID
  kms_data_key_reuse_period_seconds = 3600
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.hn_sqs_push_in_dlq.arn
    maxReceiveCount     = 1
  })

 tags = merge(local.common_tags,map("Name",join("-",[local.sqsName,"push-in"])))
}

data "aws_iam_policy_document" "hn_sqs_push_in" {
    policy_id = join("/", [aws_sqs_queue.hn_sqs_push_in.arn, "SQSDefaultPolicy"])
    statement {
        actions = ["SQS:*"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers = ["*"]
        }
        resources=[aws_sqs_queue.hn_sqs_push_in.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [join(":",["arn:aws:sns",data.aws_region.current.name,var.OldFoundationAccountId,var.SNSTopic_ContextMsg])]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_sqs_push_in" {
  queue_url = aws_sqs_queue.hn_sqs_push_in.id
  policy = data.aws_iam_policy_document.hn_sqs_push_in.json
}

resource "aws_sqs_queue" "hn_sqs_push_in_dlq" {
  name                      = join("-",[local.sqsName,"push-in-dlq"])
  delay_seconds             = 30
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 120
  max_message_size          = 262144
  kms_master_key_id                 = var.kmsMasterKeyID
  kms_data_key_reuse_period_seconds = 3600

 tags = merge(local.common_tags,map("Name",join("-",[local.sqsName,"push-in-dlq"])))
}



