

######################################### S3 Buckets Auditing and REPOS  #####################
resource "aws_s3_bucket" "hn-general-ac2-lambda-repo" {
  bucket = join("-",[local.s3bName,"lambda-repo-01"])
  acl    = "private"
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"lambda-repo-01"])))
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
		}
    }	
  }		
 logging {
    target_bucket = aws_s3_bucket.hn-general-ac2-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"lambda-repo-01"])
  } 
}

resource "aws_s3_bucket" "hn-general-ac2-bucket-auditing" {
  bucket = join("-",[local.s3bName,"bucket-audits-01"])
  
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"bucket-audits-01"])))
  acl    = "log-delivery-write"
   
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
      }
    }
	}
}

#############JULY RELEASE #################
/*
resource "aws_s3_bucket_object" "hn-notification-feedback_s3key_ac2" {
  key        =  join("-",[local.lmbName,"notification-feedback-01"])
  bucket     = aws_s3_bucket.hn-general-ac2-lambda-repo.id
  source     = "../app.zip"
  kms_key_id = var.kmsMasterKeyID
}

resource "aws_s3_bucket" "dchpayloadmessages_ac2" {
  bucket = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  acl    = "private"
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"log-dch-payloadmessages-01"])))
    versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
		}
    }
}
logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  }
}	

resource "aws_s3_bucket" "hn_fdbck_msg_bucket_ac2" {
  bucket = join("-",[local.s3bName,"notificationfeedbackmessage-01"])
  acl    = "private"
  tags   =  merge(local.common_tags,map("Name",join("-",[local.s3bName,"notificationfeedbackmessage-01"])))
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"notificationfeedbackmessage-01"])
  }
}


resource "aws_s3_bucket" "hn_fdbck_evnt_bucket_ac2" {
    bucket = join("-",[local.s3bName,"notificationfeedbackevent-01"])
    acl    = "private"
    tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"notificationfeedbackevent-01"])))
    
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default {
                kms_master_key_id = var.kmsMasterKeyID
                sse_algorithm     = var.kms
            }
        }
    }
    logging {
        target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
        target_prefix = join("-",[local.s3bName,"notificationfeedbackevent-01"])
  } 
}
*/


