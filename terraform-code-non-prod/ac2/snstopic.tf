resource "aws_sns_topic" "hn_common_events" {
  name = join("-",[local.snsName,"common-events"])
  tags = merge(local.common_tags,map("Name",join("-",[local.snsName,"common-events"])))
}

data "aws_iam_policy_document" "hn_common_events" {
  policy_id = "__default_policy_ID"
  version = "2008-10-17"
  statement {
    actions = [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.current.account_id,
      ]
    }

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      aws_sns_topic.hn_common_events.arn,
    ]

    sid = "__default_statement_ID"
  }
}

resource "aws_sns_topic_policy" "hn_common_events" {
  arn = aws_sns_topic.hn_common_events.arn
  policy = data.aws_iam_policy_document.hn_common_events.json
}

resource "aws_sns_topic" "hn_monitoring" {
  name = join("-",[local.snsName,"monitoring-01"])
}

resource "aws_sns_topic_policy" "hn_monitoring" {
  arn    = aws_sns_topic.hn_monitoring.arn
  policy = data.aws_iam_policy_document.hn_monitoring.json
}

data "aws_iam_policy_document" "hn_monitoring" {
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [aws_sns_topic.hn_monitoring.arn]
  }
}
