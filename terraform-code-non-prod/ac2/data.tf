data "aws_caller_identity" "current" {}
output "account_id" {
  value = "data.aws_caller_identity.current.account_id"
}
data "aws_region" "current" {}
output "name" {
  value = "data.aws_region.current.name"
}
resource "aws_iam_role" "hn-lambdaexecutionrole"{
  description = "Allows Lambda functions to call AWS services on your behalf."
  name = "iar-parcels-np-euwe01-hn-lambda--notification-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags      = merge(local.common_tags,map("Name",join(local.iarName,["lambda-notification-role"])))
}
