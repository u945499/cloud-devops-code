resource "aws_ecs_task_definition" "hn_publisher_svc" {
    execution_role_arn = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRole"])
    task_role_arn  = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRoleSFM"])
    family = join("-",[local.ctdName,"publisher-01"])
    requires_compatibilities = ["FARGATE"]
    network_mode = "awsvpc"
    cpu = 256
    memory = 512
    ######## INITIAL TASK DEFINITION ==> TO BE REDEPLOYED BY DEV ########
    container_definitions = <<TASK_DEFINITION
    [
        {
            "logConfiguration": {
                "logDriver": "awslogs",
                "secretOptions": null,
                "options": {
                    "awslogs-group": "/ecs/ctd-parcels-ac2-euwe01-hn-publisher-01",
                    "awslogs-region": "eu-west-1",
                    "awslogs-stream-prefix": "event-publisher-ac2"
                }
            },
            "cpu": 0,
            "environment": [
                {
                    "name": "AWS_DEFAULT_REGION",
                    "value": "eu-west-1"
                },
                {
                    "name": "AWS_SNS_ACCOUNT_NUMBER",
                    "value": "086243371668"
                },
                {
                    "name": "DB_CONNECTION_LIMIT",
                    "value": "100"
                },
                {
                    "name": "DB_NAME",
                    "value": "heimdallAC2"
                },
                {
                    "name": "DB_REPLICA_SECRET",
                    "value": "scr-parcels-np-euwe01-hn-rds-rouser"
                },
                {
                    "name": "LOG_LEVEL",
                    "value": "verbose"
                },
                {
                    "name": "MAPPER_CACHE",
                    "value": "3600000"
                },
                {
                    "name": "MAX_INFLIGHT_MESSAGES",
                    "value": "20"
                },
                {
                    "name": "PUBLISHER_MESSAGES_PER_READ",
                    "value": "10"
                },
                {
                    "name": "PUBLISHER_QUEUE_URL",
                    "value": "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-np-ac2-euwe01-hn-push-in"
                }
            ],
            "image": "086243371668.dkr.ecr.eu-west-1.amazonaws.com/ecr-hn-np-euwe01-publisher-01:AC1-572",
            "name": "EventPublisher"
        }       
    ]
    TASK_DEFINITION
    lifecycle {
        ignore_changes = [container_definitions]
    }
}

resource "aws_ecs_service" "hn_publisher_svc" {
    name            = join("-",[local.ecsName,"publisher-01"])
    cluster         = var.HNECSC
    task_definition = aws_ecs_task_definition.hn_publisher_svc.arn
    desired_count   = 1
    launch_type = "FARGATE"
    network_configuration {
        subnets = [
            var.TrustedSubnetAZ1,
            var.TrustedSubnetAZ2
        ]
        security_groups = [
            var.ECSSecurityGroup
        ] 
    }

    lifecycle {
        ignore_changes = [desired_count,task_definition]
    }
}


     