variable "availability_zones" {
 description="for the availibility zones"
 type= list(string)
 default=["eu-west-1a", "eu-west-1b"]
} 
variable "kmsMasterKeyID" {
  type=string
    description="this variable is used to associate KMS key to s3 bucket"
    default="arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
}

variable "CommonVPC" {
    type=string
    description="VPC used"
    default="vpc-03d2ff9bf50d3ff5b"
}

variable "TrustedSubnetAZ1" {
    type=string
    description="Subnet Trusted Availibility Zone 1"
    default="subnet-0b43ff380a425d9aa"
}

variable "TrustedSubnetAZ2" {
    type=string
    description="Subnet Trusted Availibility Zone 2"
    default="subnet-05a33b81b6ea84ff4"
}

variable "ECSSecurityGroup" {
    type=string
    description="Security Group for the EC services"
    default="sg-026b1251e705a5a91"
}

variable "HNECSC" {
    type=string
    description="HN ECS Cluster"
    default="ecf-parcel-np-euwe01-sfm-01"
}

variable "OldFoundationAccountId" {
    type=string
    description="HN Old Foundation AWS Account id"
    default="255796380223"
}

variable "OldFoundationDBName" {
    type=string
    description="HN Old Foundation DB name"
    default="heimdallAC2"
}

variable "OldFoundationDBSecret" {
    type=string
    description="HN Old Foundation DB secret"
    default="scr-parcels-np-euwe01-hn-rds-rouser"
}

variable "SNSTopic_ContextMsg" {
    type=string
    description="HN context message SNS topic"
    default="SNS-HN-NP-AC2-WE1-IN-CONTEXTMSG"
}

variable "kms" {
  type=string
  default="aws:kms"
  
}
##################### RDS DB Variables #######################
variable "dbName" {
  type=map
  default= {
    dv1: "dv1_notification"
    st1: "st1_notification"
    ac1: "ac1_notification"
    ac2: "ac2_notification"
  }
}

variable "dbMasterSecret" {
  type=map
  default= {
    dv1: "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
    st1: "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
    ac1: "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
    ac2: "scr-parcels-np-ac2-euwe01-hn-notification-rds-rw-user-details-01"
  }
}

variable "dbReplicaSet" {
  type=map
  default= {
    dv1: "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
    st1: "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"
    ac1: "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"
    ac2: "scr-parcels-np-ac2-euwe01-hn-notification-rds-ro-user-details-01"
  }
}

variable "rdsdbsecretStringforConnection"{
  default={
    "master_username": "ac2admin",
    "master_password": "NewDelhi#26",
    "username": "",
    "password": "",
    "engine" : "aurora",
    "host" : "",
    "port": "3306",
    "dbClusterIdentifier": ""
  }
}
variable "rdsdbsecretStringforReadWriteUser"{
  default={
    "user": "ac2dbwriteUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-ac2-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-ac2-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "ac2_notification"
  }
  
}

variable "rdsengine" {
  description="for rds aurora engine"
  type=string
  default="aurora"
}

variable "rdsengineMode" {
  description="for rds aurora engine"
  type=string
  default="serverless"
}

variable "engine_mode" {
  description="for rds aurora engine"
  type=string
  default="serverless"
}
variable "engine_version" {
  description="for rds aurora engine"
  type=string
  default="5.6.10a"
}

variable "db_backup_retention_period"{
  type=string
  description="database retention period for prod"
  default="1"

}

variable "db_backup_window"{
  type=string
  description="database backup window period for prod"
  default="04:53-05:23"

}

variable "copytagstosnapshot"{
  type=string
  description="copy tags to snapshots"
  default=true
}

variable "skipfinalsnapshot"{
  type=string
  description="skip snapshots"
  default=false
}
variable "maintenance_window"{
  type=string
  description="maintenance windows for DB"
  default="sat:00:01-sat:00:31"
}

variable "rdsport" {
  type=list(number)
  default=[3306]
}

variable "deletion_protection"{
  type= string
  default=true
}

variable "dbrdsfamily"{
  type=string
  default="aurora5.6"
}

###############################LAMBDA VARIABLES ########################
 
variable "lambdaHandler" {
  type=string
  default="hn-notification-feedback-service/src/FeedbackLambda.handler"
}

variable "lambdaExecutionRole" {
  type=string
  default="arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
   
  }


variable "s3bucketacl" {
  type=string
  default="private"
  
}

#################### NETWORKING VARIABLES #################
variable "vpc_id"{
type=list(string)
default=["vpc-03d2ff9bf50d3ff5b"]
}

variable "cidr_blocks"{
type=list(string)
default=["10.78.136.0/23", "10.78.134.0/23", "10.76.47.105/32", "10.71.120.62/32","10.71.112.0/23","10.71.114.0/23"]
}

variable "db_subnet_ids"{
type=list(string)
  default=["subnet-0b43ff380a425d9aa", "subnet-05a33b81b6ea84ff4"]
}