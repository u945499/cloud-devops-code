resource "aws_cloudwatch_log_group" "hn_publisher_svc" {
  name = "/ecs/ctd-parcels-ac2-euwe01-hn-publisher-01"

  tags = merge(local.common_tags,map("Name","ctd-parcels-ac2-euwe01-hn-publisher-01"))
}