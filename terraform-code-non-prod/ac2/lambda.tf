/*  Flex Release
AWS Components
1.Secret Manager - 
    - 1 for Nia & LRS Integration (secret + secret version)
    - 1 for Hn HN notification

2. S3 Bucket-
  3 buckets for 

3.Lambda 
    - 1 for HN Notifcation ( lambda + lambda permission)


*/


############# JULY Flex - HN Notification Lambda #################
/*
resource "aws_lambda_function" "hn_lmb_notification_feedback" {
  s3_bucket     = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key        = aws_s3_bucket_object.hn-notification-feedback_s3key.id
  function_name = join("-",[local.lmbName,"notification-feedback-01"])
  role          = var.lambdaExecutionRole
  handler       = "build/src/index.handler"
  runtime       = "nodejs12.x"
  timeout       = 300

     #security groups needs to check for prod.
  vpc_config  { 
    security_group_ids = ["sg-0429b6a02a15d681a"]
    subnet_ids = [
      "subnet-05a33b81b6ea84ff4",
      "subnet-0b43ff380a425d9aa"
    ]
  }

  environment {
    variables = {
      INTAKE_SECRET               = join("-",[local.asmName,"feedback-hn-intake-api-01"])
      DB_MASTER_SECRET            = var.dbMasterSecret["pr"]
      DB_NAME                     = var.dbName["pr"]
      DB_REPLICA_SECRET           = var.dbReplicaSet["pr"]
      FEEDBACK_MESSAGES_BUCKET    = aws_s3_bucket.hn_fdbck_msg_bucket.id
      NOTIFICATION_MESSAGE_BUCKET = aws_s3_bucket.dchpayloadmessages.id
      FEEDBACK_EVENT_BUCKET       = aws_s3_bucket.hn_fdbck_evnt_bucket.id
    }
  }
  tags = merge(local.common_tags,map("Name",local.lmbName))
}


resource "aws_lambda_permission" "hn_lmb_notification_feedback_perm" {
   depends_on = [aws_api_gateway_rest_api.hn_rest_api]
   statement_id  = "AllowExecutionFromApiGateway"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.hn_lmb_notification_feedback.function_name
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.hn_rest_api.execution_arn}/*//*"
}
  
*/