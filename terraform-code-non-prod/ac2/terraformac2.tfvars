######################### NAMING CONVENTION #######################
resourceType = {secretmanager :"asm", lambda : "lmb", s3bucket : "s3b", snstopic : "sns",apikey : "apk",apiusage :
"apu",apigateway:"apg", 
sqsqueue:"sqs", ecstaskdef: "ctd", ecssvc: "ecs", snstopic: "sns"
iamrole:"iar"
cloudwatchrule:"cwr"}
appcluster=["parcels"]
envNamingConvention = ["np"]
categoryEnvironment = ["ac2"] 
region= "euwe01"

#######################TAGGING ######################
applicationAcronym= "HN"
applicationName= "Hybrid Network"
applicationOwner = "Pieter.BISSCHOPS@bpost.be"
applicationSupport = "SindhyaEsther.Selvin.ext@bpost.be"
backups = "False"
cloudServiceProvider="AWS"
drLevel = "2"
dataProfile= "Confidential"
envTaggingConvention= "Non Prod" #for production can use =Prod
managedBy = "TCS"
#for which aws service the resource will be consume for exmaple s3 bucket consuming by DynamoDB then add as "hn-s3"
#s3b Naming Convention <resource type>-<app cluster>-<environment>-<region>-<<application acronym>-<sequence number>
####### NAMING AND TAGGING CONVENTION VARIABLES ###########################
/* Names and Acronyms to add while using terraform vars file
Resource Type{ secretmanager :"asm", lambda : "lmb", s3bucket : "s3b", snstopic: "sns"} -->Can add to map one by one once started using further in the scripts. 
envNamingConvention = ["np"]  --> np,pr1
categoryEnvironment = ["ac1"]  --> dv1 for dev,st1 for system testing,ac1 for acceptance testing.
appcluster=["parcels"]  --> for PALO ==>parcels
region= "euwe01"  --> AWS region using
sequencenumber= 1 --->Increment as per the requirement
applicationAcronym= "HN" --->for Hybrid Network we are using as HN
applicationName= "Hybrid Network"  ---> 
applicationOwner = "Pieter.BISSCHOPS@bpost.be" ---> as mentioned for HN
applicationSupport = "SindhyaEsther.Selvin.ext@bpost.be" -->as mentioned for HN
backups = "False" --?as per taggng standards
cloudServiceProvider="AWS" --
drLevel = "2"
dataProfile= "AWS"
envTaggingConvention= "Non Prod" #for production can use =Prod
managedBy = "TCS"
servicePurpose= "feedback"   #add prupose for resource,for example lambda creating for DB access can be suffix with "dbaccess"
targetService= "hn-intake-api" #for which aws service the resource will be consume for exmaple s3 bucket consuming by DynamoDB then add as "hn-s3"

*/

############# RESOURCE VARIABLES ########################
lambdaExecutionRole="arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
kmsMasterKeyID= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
HNECSC="ecf-parcel-np-euwe01-sfm-01"
OldFoundationAccountId="255796380223"
SNSTopic_ContextMsg="SNS-HN-NP-AC2-WE1-IN-CONTEXTMSG"
OldFoundationDBName="heimdallAC2"
OldFoundationDBSecret="scr-parcels-np-euwe01-hn-rds-rouser"
CommonVPC="vpc-03d2ff9bf50d3ff5b"
TrustedSubnetAZ1="subnet-0b43ff380a425d9aa"
TrustedSubnetAZ2t="subnet-05a33b81b6ea84ff4"
ECSSecurityGroup="sg-026b1251e705a5a91"



