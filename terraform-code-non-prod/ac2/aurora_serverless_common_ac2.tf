/*
RDS Aurora Serverless-HN Notifications_ac2
JUNE RELEASE



resource "aws_rds_cluster" "hn_notif_rds_aurora_mysql_serverless" {
  cluster_identifier              = join("-",[local.rdsName,"notification-01"])
  engine                          = var.rdsengine
  engine_mode                     = var.engine_mode
  engine_version                  = var.engine_version
  availability_zones              = var.availability_zones
  master_username                 = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret.secret_string)["master_username"]
  master_password                 = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret.secret_string)["master_password"]
  backup_retention_period         = var.db_backup_retention_period
  preferred_backup_window         = var.db_backup_window
  copy_tags_to_snapshot           = var.copytagstosnapshot
  final_snapshot_identifier       = join("-",[local.rdsName,"rdc-notification-01"])
  skip_final_snapshot             = var.skipfinalsnapshot
  preferred_maintenance_window    = var.maintenance_window
  port                            = var.rdsport[0]
  vpc_security_group_ids          = [aws_security_group.hn_notif_rds_security_group.id]
  storage_encrypted               = true
  apply_immediately               = false  #######needs to check this
  deletion_protection             = var.deletion_protection
  db_subnet_group_name            = aws_db_subnet_group.hn_notif_rds_subnet_group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.hn_notif_rds_pg.id
  kms_key_id                      = aws_kms_key.hn_rds_kms_key.arn
  scaling_configuration {
    auto_pause               = false
    max_capacity             = 8
    min_capacity             = 2
    seconds_until_auto_pause = 300
    timeout_action           = "RollbackCapacityChange"
  }
  lifecycle {
    ignore_changes = [availability_zones]
  }
  tags = merge(local.common_tags,map("Name",join("-",[local.rdsName,"notification-01"])))
}


################### RDS SECRETS #############################
resource "aws_secretsmanager_secret" "hn_notif_rds_secret" {
  description = "Secret for RDS HN notification for production"
  #kms_key_id          = aws_kms_key.hn_rds_kms_key_dv1.arn
  name = join("-",[local.asmName,"notification-rds-connection-details-01"])
  tags =  merge(local.common_tags,map("Name",join("-",[local.asmName,"notification-rds-connection-details-01"])))
   #"scr-parcels-np-euwe01-hn-notification-rds-connection-details-01"
  }


resource "aws_secretsmanager_secret_version" "hn_notif_rds_secret" {
  # depends_on      = [random_password.rpassword_hn_rds_dv1]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_secret.id
  secret_string = jsonencode(var.rdsdbsecretStringforConnection)
}

resource "aws_secretsmanager_secret" "hn_notif_rds_rw_secret" {
  description = "Secret for RDS HN notification - RW user"
  name = join("-",[local.asmName,"notification-rds-rw-user-details-02"])
  tags =  merge(local.common_tags,map("Name",join("-",[local.asmName,"notification-rds-rw-user-details-02"])))
  
}


resource "aws_secretsmanager_secret_version" "hn_notif_rds_rw_secret" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_rw_secret.id
  secret_string = jsonencode(var.rdsdbsecretStringforReadWriteUser)

  lifecycle {
    ignore_changes = [secret_string]
  }
}

resource "aws_secretsmanager_secret" "hn_notif_rds_ro_secret" {
  description = "Secret for RDS HN notification - RO user"
   name = join("-",[local.asmName,"notification-rds-ro-user-details-02"])
  tags =  merge(local.common_tags,map("Name",join("-",[local.asmName,"notification-rds-ro-user-details-02"])))
  }

resource "aws_secretsmanager_secret_version" "hn_notif_rds_ro_secret" {
  depends_on    = [aws_rds_cluster.hn_notif_rds_aurora_mysql_serverless]
  secret_id     = aws_secretsmanager_secret.hn_notif_rds_ro_secret.id
  secret_string = <<EOF
    {
    "user": "notidbroUser",
    "password": "****",
    "dbClusterIdentifier" : "rdi-parcels-np-euwe01-hn-notification-01",
    "driver" : "mysql",
    "port": "3306",
    "host": "rdi-parcels-np-euwe01-hn-notification-01.cluster-c1jh6xm5arbx.eu-west-1.rds.amazonaws.com",
    "dbname": "ac2_notification"
  }
  EOF

  lifecycle {
    ignore_changes = [secret_string]
  }
}


resource "random_password" "rpassword_hn_rds" {
  length = 16
  special = true
  override_special = "$-*"
}

############################### RDS CONNECTIONS ######################

resource "aws_db_subnet_group" "hn_notif_rds_subnet_group" {
  name = join("-",[local.dbsubnetName,"notification-01"])
   subnet_ids = var.db_subnet_ids
  tags =  merge(local.common_tags,map("Name",join("-",[local.dbsubnetName,"notification-01"])))
 }


resource "aws_rds_cluster_parameter_group" "hn_notif_rds_pg" {
  name   = join("-",[local.dbcpgName,"notification-01"])
  family = var.dbrdsfamily
  tags =  merge(local.common_tags,map("Name",join("-",[local.dbcpgName,"notification-01"])))
  
}

resource "aws_security_group" "hn_notif_rds_security_group" {
   name = join("-",[local.dbsgName,"01"])
  vpc_id = var.vpc_id[0] 
  ######needs to check for prod
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.78.136.0/23", "10.78.134.0/23", "10.76.47.105/32", "10.71.120.62/32"]
  }
 ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.71.112.0/23","10.71.114.0/23"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags =merge(local.common_tags,map("Name",join("-",[local.dbsgName,"01"])))
}


*/