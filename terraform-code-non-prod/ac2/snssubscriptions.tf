####### Email protocol is unsupported ###############

resource "aws_sns_topic_subscription" "hn_sns_subs_push_in_to_context_msg" {
  topic_arn = join(":",["arn:aws:sns",data.aws_region.current.name,var.OldFoundationAccountId,var.SNSTopic_ContextMsg])
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.hn_sqs_push_in.arn
  raw_message_delivery = true
}


