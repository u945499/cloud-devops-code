resource "aws_cloudwatch_event_rule" "hn_publisher_task_stopped" {
  name        = join("-",[local.cwrName,"publisher-task-stopped-01"])
  description = "Capture when task is stopped"

  event_pattern = <<PATTERN
{
      "source": [ "aws.ecs" ],
      "detail-type": [
        "ECS Task State Change"
      ],
      "detail": {
        "clusterArn": [
            "arn:aws:ecs:eu-west-1:086243371668:cluster/ecf-parcel-np-euwe01-sfm-01"
          ],
        "group": [ "service:ecs-parcels-np-ac2-euwe01-hn-publisher-01" ],
        "lastStatus": [ "STOPPED" ],
        "stoppedReason" : ["Essential container in task exited"]
    }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "hn_publisher_task_stopped" {
  rule      = aws_cloudwatch_event_rule.hn_publisher_task_stopped.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.hn_monitoring.arn
}



