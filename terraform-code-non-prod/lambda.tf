

resource "aws_lambda_function" "hn_lmb_notif_feedback_dv1" {
  s3_bucket = aws_s3_bucket.hn-general-dv1-lambda-repo.id
  s3_key = "lmb-parcels-np-dv1-euwe01-hn-notification-feedback-01/app.zip"
  function_name = "lmb-parcels-np-dv1-euwe01-hn-notification-feedback-01"
  role          = "arn:aws:iam::086243371668:role/iar-parcels-np-euwe01-hn-lambda--notification-role"
  handler       = "hn-notification-feedback-service/src/FeedbackLambda.handler"
  runtime = "nodejs12.x"
  timeout = 300

  vpc_config {
          security_group_ids = ["sg-0429b6a02a15d681a"]
          subnet_ids         = [
              "subnet-05a33b81b6ea84ff4",
              "subnet-0b43ff380a425d9aa"
            ]
        }
        
  environment {
    variables = {
        INTAKE_SECRET = "asm-parcels-np-dv1-euwe01-hn-feedback-hn-intake-api-01"
        DB_MASTER_SECRET = "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
        DB_NAME = "dv1_notification"
        DB_REPLICA_SECRET = "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
        FEEDBACK_MESSAGES_BUCKET = aws_s3_bucket.hn_fdbck_msg_bucket_dv1.id
        NOTIFICATION_MESSAGE_BUCKET = aws_s3_bucket.dchpayloadmessagesdv1.id
        FEEDBACK_EVENT_BUCKET = aws_s3_bucket.hn_fdbck_evnt_bucket_dv1.id
          }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-np-dv1-euwe01-hn-notification-feedback-01"
        }
  }


resource "aws_lambda_permission" "hn_lmb_notif_feedback_perm_dv1" {
   depends_on = [aws_api_gateway_rest_api.hn_rest_api]
   statement_id  = "AllowExecutionFromApiGateway"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.hn_lmb_notif_feedback_dv1.function_name
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.hn_rest_api.execution_arn}/*/POST/*"
 }

