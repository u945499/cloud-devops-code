resource "aws_secretsmanager_secret" "hn_fdbck_hn_intake_api_dv1" {
  description         = "Secret for HN Intake API access by HN notification"
  name = "asm-parcels-np-dv1-euwe01-hn-feedback-hn-intake-api-01"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-np-dv1-euwe01-hn-feedback-hn-intake-api-01"
        }
}


resource "aws_secretsmanager_secret_version" "hn_fdbck_hn_intake_api_secret_dv1" {
  secret_id     = aws_secretsmanager_secret.hn_fdbck_hn_intake_api_dv1.id
  secret_string = <<EOF
    {
    "ENDPOINT" : "https://hn-api-dv1.bposthn.net/event",
    "X_API_KEY" : "ahppNBETHy3nTNkBbvuc251876VpsaAa2msqkkLa"
  }
  EOF
} 