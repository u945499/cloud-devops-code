/* 

!!!!!! SECTION TO BE IMPORTED FIRST INTO STATE FILE BEFORE APPLYING !!!!!

*/

#### To be able to manage it via terraform, those resources need to be imported first and then configuration can be done and then compared with the configuration of AWS with the terraform plan


resource "aws_sqs_queue" "hn_brokerservice_queue_st1" {
  name                      = "sqs-parcels-np-st1-euwe01-brokerservice-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 30
  kms_master_key_id                 = "alias/kms-parcels-hn-np-euwe01-sqs-01"
  kms_data_key_reuse_period_seconds = 300
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.hn_brokerservice_dlq_queue_st1.arn
    maxReceiveCount     = 1
  })
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "sqs-parcels-np-st1-euwe01-brokerservice-01"
        }

}

data "aws_iam_policy_document" "hn_brokerservice_queue_st1-policy" {
    policy_id = join("/", [aws_sqs_queue.hn_brokerservice_queue_st1.arn, "SQSDefaultPolicy"])
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_st1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:086243371668:sns-hn-np-st1-euwe01-push-brk"]
        }
        sid = "Sid1580719568711"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_st1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-ST1-WE1-COMMON-EVENTS"]
        }
        sid = "Sid1588602509256"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_st1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_sqs_queue.hn_brokerservice_dlq_queue_st1.arn]
        }
        sid = "Sid1588860905098"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_st1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lambda_filter_common_events_msg_for_NeRO_st1.arn]
        }
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_st1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_st1.arn]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_brokerservice_queue_policy_st1" {
  queue_url = aws_sqs_queue.hn_brokerservice_queue_st1.id
  policy = data.aws_iam_policy_document.hn_brokerservice_queue_st1-policy.json
}

resource "aws_sqs_queue" "hn_brokerservice_dlq_queue_st1" {
  name                      = "sqs-parcels-np-st1-euwe01-brokerservice-dlq-01"
  delay_seconds             = 10
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "sqs-parcels-np-st1-euwe01-brokerservice-dlq-01"
        }
}

resource "aws_sqs_queue" "hn_brokerservice_queue_ac1" {
  name                      = "sqs-parcels-np-ac1-euwe01-brokerservice-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 120
  kms_master_key_id                 = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
  kms_data_key_reuse_period_seconds = 3600
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.hn_brokerservice_dlq_queue_ac1.arn
    maxReceiveCount     = 3
  })
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "sqs-parcels-np-ac1-euwe01-brokerservice-01"
        }

}

data "aws_iam_policy_document" "hn_brokerservice_queue_ac1-policy" {
    policy_id = join("/", [aws_sqs_queue.hn_brokerservice_queue_ac1.arn, "SQSDefaultPolicy"])
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_ac1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:086243371668:sns-hn-np-ac1-euwe01-push-brk"]
        }
        sid = "Sid1580719285729"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_ac1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:086243371668:sns-hn-np-ac1-euwe01-common-events"]
        }
        sid = "Sid1588602872495"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_ac1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lambda_filter_common_events_msg_for_NeRO_ac1.arn]
        }
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_ac1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_ac1.arn]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_brokerservice_queue_policy_ac1" {
  queue_url = aws_sqs_queue.hn_brokerservice_queue_ac1.id
  policy = data.aws_iam_policy_document.hn_brokerservice_queue_ac1-policy.json
}

resource "aws_sqs_queue" "hn_brokerservice_dlq_queue_ac1" {
  name                      = "sqs-parcels-np-ac1-euwe01-brokerservice-dlq-01"
  delay_seconds             = 10
  message_retention_seconds = 1209600
  visibility_timeout_seconds = 120
  receive_wait_time_seconds = 10
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "sqs-parcels-np-ac1-euwe01-brokerservice-dlq-01"
        }
}

resource "aws_lambda_permission" "notification-publisher-connector-SNS-trigger" {
    statement_id = "AllowExecutionFromSNS"
    action = "lambda:InvokeFunction"
    function_name = "arn:aws:lambda:eu-west-1:086243371668:function:lmd-parcels-hn-ac1-euwe01-notification-publisher-connector-01"
    principal = "sns.amazonaws.com"
    source_arn = "arn:aws:sns:eu-west-1:086243371668:sns-hn-np-ac1-euwe01-common-events"
}

resource "aws_sqs_queue" "hn_brokerservice_queue_dv1" {
  name                      = "sqs-parcels-np-dv1-euwe01-brokerservice-01"
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 30
  kms_master_key_id                 = "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
  kms_data_key_reuse_period_seconds = 300
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.hn_brokerservice_dlq_queue_dv1.arn
    maxReceiveCount     = 1
  })
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "sqs-parcels-np-dv1-euwe01-brokerservice-01"
        }

}

data "aws_iam_policy_document" "hn_brokerservice_queue_dv1-policy" {
    policy_id = join("/", [aws_sqs_queue.hn_brokerservice_queue_dv1.arn, "SQSDefaultPolicy"])
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_dv1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:086243371668:sns-hn-np-dv1-euwe01-push-brk"]
        }
        sid = "Sid1580719568711"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_dv1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-dv1-WE1-COMMON-EVENTS"]
        }
        sid = "Sid1588602509256"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_dv1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:255796380223:SNS-HN-NP-DV1-WE1-PUSH-MANDATE-EXEC"]
        }
        sid = "Sid1588860905098"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_dv1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["lmb-parcels-np-dv1-euwe01-hn-sns-msg-broker-neroevents-01"]
        }
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue_dv1.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents_dv1.arn]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_brokerservice_queue_policy_dv1" {
  queue_url = aws_sqs_queue.hn_brokerservice_queue_dv1.id
  policy = data.aws_iam_policy_document.hn_brokerservice_queue_dv1-policy.json
}

resource "aws_sqs_queue" "hn_brokerservice_dlq_queue_dv1" {
  name                      = "sqs-parcels-np-dv1-euwe01-brokerservice-dlq-01"
  delay_seconds             = 10
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "sqs-parcels-np-dv1-euwe01-brokerservice-dlq-01"
        }
}

data "aws_iam_policy_document" "hn_commonevents_sns_policy_ac1" {
  policy_id = "__default_policy_ID"
  version = "2008-10-17"
  statement {
    actions = [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.current.account_id,
      ]
    }

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      aws_sns_topic.brokersnsac1.arn,
    ]

    sid = "__default_statement_ID"
  }
  statement {
    actions = [
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Receive"
    ]

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::367765929612:root"]
    }

    resources = [
      aws_sns_topic.brokersnsac1.arn,
    ]

    sid = "_Allow_account_367765929612"
  }
}

resource "aws_sns_topic_policy" "hn_commonevents_sns_policy_ac1" {
  arn = aws_sns_topic.brokersnsac1.arn
  policy = data.aws_iam_policy_document.hn_commonevents_sns_policy_ac1.json
}


