variable "lambdaExecutionRole" {
    type=string
    description="this variable is used to execute the lambda  in non prod (dv1,st1,ac1)"
    default="arn:aws:iam::119394011513:role/iar-LambdaExecutionRole"
}

variable "kmsMasterKeyID" {
  type=string
    description="this variable is used to associate KMS key to s3 bucket"
    default="arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
}

variable "kms" {
  type=string
  default="aws:kms"
  
}

variable "dbName" {
  type=map
  default= {
    dv1: "dv1_notification"
    st1: "st1_notification"
    ac1: "st1_notification"
  }
}

variable "dbMasterSecret" {
  type=map
  default= {
    dv1: "scr-parcels-np-dv1-euwe01-hn-notification-rds-rw-user-details-02"
    st1: "scr-parcels-np-st1-euwe01-hn-notification-rds-rw-user-details-01"
    ac1: "scr-parcels-np-ac1-euwe01-hn-notification-rds-rw-user-details-01"
  }
}

variable "dbReplicaSet" {
  type=map
  default= {
    dv1: "scr-parcels-np-dv1-euwe01-hn-notification-rds-ro-user-details-02"
    st1: "scr-parcels-np-st1-euwe01-hn-notification-rds-ro-user-details-01"
    ac1: "scr-parcels-np-ac1-euwe01-hn-notification-rds-ro-user-details-01"
  }
}
 
variable "lambdaHandler" {
  type=string
  default="hn-notification-feedback-service/src/FeedbackLambda.handler"
}


variable "s3bucketacl" {
  type=string
  default="private"
  
}

variable "TrustedSubnetAZ1" {
    type=string
    description="Subnet Trusted Availibility Zone 1"
    default="subnet-0b43ff380a425d9aa"
}

variable "TrustedSubnetAZ2" {
    type=string
    description="Subnet Trusted Availibility Zone 2"
    default="subnet-05a33b81b6ea84ff4"
}

variable "ECSSecurityGroup" {
    type=string
    description="Security Group for the EC services"
    default="sg-0429b6a02a15d681a"
}

variable "HNECSC" {
    type=string
    description="HN ECS Cluster"
    default="ecf-parcel-np-euwe01-sfm-01"
}
