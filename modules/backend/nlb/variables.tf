variable "App_EnvironmentType" {
  description = "Provide the profile name where credentails are stored"
}
variable "App_AcronymCode" {
  description = "Provide the Application Acronymcode"
}
variable "App_ClusterCode" {
  description = "Provide the Application ClusterCode"
}
variable "Default_Tags" {
  description = "Mapping of tags to assign to the resource"
}
variable "Subnet_ID_AZ1_Trusted" {
  description = "Provide eu-west-1a(AZ1) Availability Zone trusted subnet ID"
}
variable "Subnet_ID_AZ2_Trusted" {
  description = "Provide eu-west-1b(AZ2) Availability Zone trusted subnet ID"
}
variable "Subnet_ID_AZ3_Trusted" {
  description = "Provide eu-west-1c(AZ3) Availability Zone trusted subnet ID"
}
variable "NLB_Internal" {
  description = "Specify if NLB is internal or external"
}
variable "NLB_Type" {
  description = "Specify Type of load balancer to deploy"
}
variable "NLB_Access_Logs" {
  description = "Specify if access logs needs to be enabled for the load balancer"
}
variable "S3_Log_Bucket_ID" {
  description = "provide S3 Log bucket ID to store logs"
}
variable "NLB_Cross_Zone" {
  description = "Specify if cross zone load balancing needs to be enabled"
}
variable "Deletion_Protection" {
  description = "Specify if deletion protection needs to be enabled for the resource"
}


