resource "aws_lb" "NLB" {
  name = "nlb-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-int-1"
  internal = var.NLB_Internal
  load_balancer_type = var.NLB_Type
  subnet_mapping {
    subnet_id = var.Subnet_ID_AZ1_Trusted
  }
  subnet_mapping {
    subnet_id = var.Subnet_ID_AZ2_Trusted
  }
  subnet_mapping {
    subnet_id = var.Subnet_ID_AZ3_Trusted
  }
  enable_cross_zone_load_balancing = var.NLB_Cross_Zone
  enable_deletion_protection = var.Deletion_Protection
  access_logs {
    bucket = var.S3_Log_Bucket_ID
    prefix = "nlb${var.App_AcronymCode}log"
    enabled = var.NLB_Access_Logs
  }
  tags = merge(var.Default_Tags, map("Name","nlb-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-int-1"), map("Environment",var.App_EnvironmentType))
}