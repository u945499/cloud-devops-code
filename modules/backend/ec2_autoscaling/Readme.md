#Terraform Version 0.12.24
#Terraform Provider Version 2.58

This Module will create autoscaling instances with launch configuration,load balancer, Security groups and target group


#######   PreRequisites   ############

1) Should have the details for security group of domain members already available in the account, VPC, Restricted subnet ,IP range etc
2) As per my use case used , t2.small instance type and AMI- You can select based on the project requirement
3) Should have the certificate generated in ACM in order to attach it to the load balancer listener.
4) Should have the key and instance profile to be used to launch the instance.

========================================================================================================================
App_Cluster              = ""  Must -  your cluster like hr,es, cc02,parcels
========================================================================================================================
App_Environment          = "test" # allowed values are dv1/dv2/st1/st2/ac1/ac2/pr
========================================================================================================================
App_Acronym              = "test"
========================================================================================================================
authorized_aws_accounts  = "179337217599"
THis is required as it used in provider file
========================================================================================================================
profile                  = ""  your access profile details
========================================================================================================================
AWS_REGION               = "eu-west-1"
================================================================
VPC ID is required for Security group creation
vpc_id                   = "vpc-0054433c3ac29b80b"
================================================================
We are placing the instance in trusted subnet , need to have the subnet details.
before the implementation
subnet_id_az1 = "subnet-0aa363febb15dc5f5"
subnet_id_az2 = "subnet-0f2d8136309af704a"

========================================================================================================================
Trusted IP Range is required for the Load balancer and instance security group, with this we can allow the users to connect the
EC2 instances in trusted subnet. We can use the Security group as well instead of IP Ranges

trusted_cidr_iprange_01 = "10.1.32.0/19"
trusted_cidr_iprange_02 = "10.100.0.0/16"
trusted_cidr_iprange_03 = "10.192.45.161/32"
trusted_cidr_iprange_04 = "10.192.45.162/32"
outbound_SG = "sg-0c4ca9f73e9bf330e"
========================================================================================================================
There are 12 Tags which is mandatory for any resource , Tags should have below informations

default_tags = {
  ApplicationName      = "MUST"
  ApplicationAcronym   = "MUST"
  ApplicationOwner     = ""MUST"@bpost.be"
  ApplicationSupport   = ""MUST"@bpost.be"
  ManagedBy            = "MUST"
  Environment          = "Development / Production"
  CloudServiceProvider = "AWS"
  DRLevel              = "MUST"   #You can discuss with the Solution designer and get the DR Level ,
  Based on the DR level you can select the Availability Zone
  Backup               = "True"
  DataProfile          = "Restricted/Internal "
  SubnetType           = "Private/Public"
  Availability         = "True"
}
========================================================================================================================
instance_type = "t2.small"
Based on the project requirement specify the instance type.
========================================================================================================================

key_name = "key-hr-np-euwe01-01"
Should have the key generated and its name detail for the account
========================================================================================================================

iam_instance_profile = "iar-SsmAdmin"
Should have the instance profile created and its detail for the account
========================================================================================================================

security_group_01 = "sg-0c4ca9f73e9bf330e"
security_group_02 = "sg-00a5314df3c61c028"
security_group_03 = "sg-0d959add0890d3df1"
Should have the security group details which need to attach to the instance for doing domain join
========================================================================================================================

associate_public_ip_address = false
If public ip address is required, will give true, since it's a intranet application mentioned false.
========================================================================================================================

user_data = " "
If user data is need to be passed, will mention.
========================================================================================================================
enable_monitoring = true
Monitoring is enabled for the instances
========================================================================================================================

ebs_optimized = false
If ebs volume is need to be optimized, mention it as true.
========================================================================================================================

internal           = true
If it's a internal load balancer type give true.
========================================================================================================================

load_balancer_type = "application"
Based on the load balancer type (application/Network) give the value.
========================================================================================================================
subnets = ["subnet-0aa363febb15dc5f5","subnet-0f2d8136309af704a"]
If you have selected 2 availability Zone then mention two Subnet for the trusted subnet to launch the instances in AZ1 & AZ2.
========================================================================================================================

port     = 443
Based on the requirement of the port(443/80) in which the URL listen, specify the port range.
========================================================================================================================

protocol = "HTTPS"
Based on the requirement of the protocol(HTTP/HTTPS) in which the URL listen, specify the port range.

========================================================================================================================

vpc_id   = "vpc-03160bcf35d619334"
Should have the vpc_id available for that account
========================================================================================================================

target_type = "instance"
Based on the target type (instance/ip) give the value.
========================================================================================================================

path = "/" 
Mentioned the  default path. Based on the project type it will change.
========================================================================================================================

certificate_arn   = "arn:aws:acm:eu-west-1:863800670833:certificate/ab087de4-3870-4cb7-8bc0-1899700c79f5"
Should have the certificate generated in ACM and certificate arn details should be available in that account.
========================================================================================================================

max_size = 2
maximum size of autoscaling setup is 2. Based on the project requirement specify the value.
========================================================================================================================

min_size = 1
minimum size of autoscaling setup is 1. Based on the project requirement specify the value.
========================================================================================================================
 
desired_capacity = 2
desired capacity of autoscaling setup is 2. Based on the project requirement specify the value.
========================================================================================================================

default_cooldown = 1200
default cooldown period is 1200. Based on the project requirement specify the value.
========================================================================================================================

health_check_grace_period = 150
healthcheck graceperiod is 150. Based on the project requirement specify the value.
========================================================================================================================

health_check_type = "ELB"
healthcheck type is ELB. Based on the project requirement specify the ELB or Instances. It's always better to have ELB.
========================================================================================================================

metrics_granularity = "1Minute"
Enabled cloud watch metrics for every 1 minute. Based on the project requirement specify the 1 or 5 minute. 
========================================================================================================================

enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
  Enabled cloud watch metrics for above attributes. Based on the project requirement specify the value. 
========================================================================================================================

wait_for_capacity_timeout = "10m"
Mentioned 10 minutes duration that Terraform should wait for ASG instances to be healthy before timing out. Based on the project requirement specify the value. 

========================================================================================================================


TEST RESULT

module.ec2_autoscaling.aws_security_group.lb_security_group: Creating...
module.ec2_autoscaling.aws_lb_target_group.this: Creating...
module.ec2_autoscaling.aws_lb_target_group.this: Creation complete after 0s [id=arn:aws:elasticloadbalancing:eu-west-1:863800670833:targetgroup/alt-hr-dv1-euwe01-bdg-01/dfaa0f7b358e335e]

module.ec2_autoscaling.aws_security_group.lb_security_group: Creation complete after 1s [id=sg-042b777c41341d4af]
module.ec2_autoscaling.aws_security_group.lc_security_group: Creating...
module.ec2_autoscaling.aws_security_group.lc_security_group: Creation complete after 1s [id=sg-002d8ec4af7b8c1ed]
module.ec2_autoscaling.aws_lb.this: Creating...
module.ec2_autoscaling.aws_launch_configuration.this: Creating...
module.ec2_autoscaling.aws_launch_configuration.this: Creation complete after 0s [id=elc-hr-dv1-euwe01-bdg-01]
module.ec2_autoscaling.aws_autoscaling_group.this: Creating...
module.ec2_autoscaling.aws_lb.this: Still creating... [10s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [10s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [20s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [20s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [30s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [30s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [40s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [40s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [50s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [50s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [1m0s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [1m0s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [1m10s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still creating... [1m10s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Creation complete after 1m18s [id=asg-hr-dv1-euwe01-bdg-01]
module.ec2_autoscaling.aws_autoscaling_attachment.targetattach: Creating...
module.ec2_autoscaling.aws_autoscaling_attachment.targetattach: Creation complete after 0s [id=asg-hr-dv1-euwe01-bdg-01-20200430064222222300000001]
module.ec2_autoscaling.aws_lb.this: Still creating... [1m20s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [1m30s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [1m40s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [1m50s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [2m0s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [2m10s elapsed]
module.ec2_autoscaling.aws_lb.this: Still creating... [2m20s elapsed]
module.ec2_autoscaling.aws_lb.this: Creation complete after 2m21s [id=arn:aws:elasticloadbalancing:eu-west-1:863800670833:loadbalancer/app/alb-hr-dv1-euwe01-bdg-01/eb2492794270f43e]
module.ec2_autoscaling.aws_lb_listener.this: Creating...
module.ec2_autoscaling.aws_lb_listener.this: Creation complete after 1s [id=arn:aws:elasticloadbalancing:eu-west-1:863800670833:listener/app/alb-hr-dv1-euwe01-bdg-01/eb2492794270f43e/147
7b7b369416b3e]

Apply complete! Resources: 8 added, 0 changed, 0 destroyed.
========================================================================================================================
Tested the Destroy as wll

module.ec2_autoscaling.aws_autoscaling_attachment.targetattach: Destroying... [id=asg-hr-dv1-euwe01-bdg-01-20200430064222222300000001]
module.ec2_autoscaling.aws_lb_listener.this: Destroying... [id=arn:aws:elasticloadbalancing:eu-west-1:863800670833:listener/app/alb-hr-dv1-euwe01-bdg-01/eb2492794270f43e/1477b7b3694
16b3e]
module.ec2_autoscaling.aws_lb_listener.this: Destruction complete after 0s
module.ec2_autoscaling.aws_lb.this: Destroying... [id=arn:aws:elasticloadbalancing:eu-west-1:863800670833:loadbalancer/app/alb-hr-dv1-euwe01-bdg-01/eb2492794270f43e]
module.ec2_autoscaling.aws_autoscaling_attachment.targetattach: Destruction complete after 0s
module.ec2_autoscaling.aws_autoscaling_group.this: Destroying... [id=asg-hr-dv1-euwe01-bdg-01]
module.ec2_autoscaling.aws_lb.this: Destruction complete after 1s
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 10s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 20s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 30s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 40s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 50s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 1m0s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 1m10s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 1m20s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Still destroying... [id=asg-hr-dv1-euwe01-bdg-01, 1m30s elapsed]
module.ec2_autoscaling.aws_autoscaling_group.this: Destruction complete after 1m34s
module.ec2_autoscaling.aws_launch_configuration.this: Destroying... [id=elc-hr-dv1-euwe01-bdg-01]
module.ec2_autoscaling.aws_lb_target_group.this: Destroying... [id=arn:aws:elasticloadbalancing:eu-west-1:863800670833:targetgroup/alt-hr-dv1-euwe01-bdg-01/dfaa0f7b358e335e]
module.ec2_autoscaling.aws_lb_target_group.this: Destruction complete after 0s
module.ec2_autoscaling.aws_launch_configuration.this: Destruction complete after 0s
module.ec2_autoscaling.aws_security_group.lc_security_group: Destroying... [id=sg-002d8ec4af7b8c1ed]
module.ec2_autoscaling.aws_security_group.lc_security_group: Destruction complete after 0s
module.ec2_autoscaling.aws_security_group.lb_security_group: Destroying... [id=sg-042b777c41341d4af]
module.ec2_autoscaling.aws_security_group.lb_security_group: Destruction complete after 1s

Destroy complete! Resources: 8 destroyed.




