#Common
variable "AWS_REGION" {}
variable "profile" {}
variable "default_tags" {}
variable "App_Cluster" {}
variable "App_Environment" {}
variable "App_Acronym" {}
variable "authorized_aws_accounts" {}

variable "trusted_cidr_iprange_01" {}
variable "trusted_cidr_iprange_02" {}
variable "trusted_cidr_iprange_03" {} 
variable "trusted_cidr_iprange_04" {}
variable "toolserver_ip" {}
variable "security_group_01" {}
variable "security_group_02" {}


variable "EC2_autoscaling_group_tags" {
  default = [
    {
      key                 = "ApplicationName"
      value               = "Badging Unitime"
      propagate_at_launch = true
    },
    {
      key                 = "ApplicationAcronym"
      value               = "BDG"
      propagate_at_launch = true
    },
	{
      key                 = "ApplicationOwner"
      value               = "andy.dentandt@bpost.be"
      propagate_at_launch = true
    },
	{
      key                 = "ApplicationSupport"
      value               = "umagayathri.v.ext@bpost.be"
      propagate_at_launch = true
    },
	{
      key                 = "ManagedBy"
      value               = "TCS"
      propagate_at_launch = true
    },
	{
      key                 = "Environment"
      value               = "Development"
      propagate_at_launch = true
    },
 	{
      key                 = "CloudServiceProvider"
      value               = "AWS"
      propagate_at_launch = true
    },
	{
      key                 = "DRLevel"
      value               = "2"
      propagate_at_launch = true
    },
	{
      key                 = "Backup"
      value               = "True"
      propagate_at_launch = true
    },
	{
      key                 = "DataProfile"
      value               = "Restricted"
      propagate_at_launch = true
    },
	{
      key                 = "SubnetType"
      value               = "Private"
      propagate_at_launch = true
    },
  ]
}

#Launch Configuration

variable "EC2_LC_image_id" {
  type = string
}
variable "EC2_LC_instance_type" {
  type = string
}
variable "EC2_LC_key_name" {
  type = string
}
variable "EC2_LC_iam_instance_profile" {
  type = string
}

variable "EC2_LC_subnet_id" {
  type = list(string)
}

variable "EC2_LC_associate_public_ip_address" {
  description = "Associate a public ip address with an instance in a VPC"
  type        = bool
  default     = false
}

variable "EC2_LC_user_data" {
  description = "The user data to provide when launching the instance"
  type        = string
  default     = " "
}

variable "EC2_LC_enable_monitoring" {
  description = "Enables/disables detailed monitoring. This is enabled by default."
  type        = bool
  default     = true
}
variable "EC2_LC_ebs_optimized" {
  type        = bool
  description = "If true, the launched EC2 instance will be EBS-optimized"
  default     = false
}
variable "EC2_root_block_device" {
  description = "Specify volumes to attach to the instance besides the volumes specified by the AMI"

  type = list(object({
    device_name  = string
    no_device    = bool
    virtual_name = string
    root = object({
      delete_on_termination = bool
      encrypted             = bool
      iops                  = number
      kms_key_id            = string
      snapshot_id           = string
      volume_size           = number
      volume_type           = string
    })
  }))

  default = []
}

variable "EC2_ebs_block_device" {
  description = "Specify volumes to attach to the instance besides the volumes specified by the AMI"

  type = list(object({
    device_name  = string
    no_device    = bool
    virtual_name = string
    ebs = object({
      delete_on_termination = bool
      encrypted             = bool
      iops                  = number
      kms_key_id            = string
      snapshot_id           = string
      volume_size           = number
      volume_type           = string
    })
  }))

  default = []
}

#Load Balancer
variable "EC2_load_balancer_type_facing" {
  description = "Load balancer is internal or internet"
  type        = bool
}
variable "EC2_load_balancer_type" {
  description = "The maximum size of the auto scale group"
  type        = string
  default = "application"
}
variable "EC2_load_balancer_subnets" {
  description = "A list of subnet id's"
  type        = list(string)
  default     = []
}
variable "EC2_target_group_port" {
  description = "The port on which targets receive traffic"
  type        = string
}
variable "EC2_target_group_healthcheck_path" {
  description = "The health check path"
  type        = string
}
variable "EC2_target_group_protocol" {
  description = "The protocol to use for routing traffic to the targets"
  type        = string
}

#Target group
variable "EC2_target_group_vpc_id" {
  description = "The Identifier of the VPC in which to create the target group"
  type        = string
}
variable "EC2_target_group_type" {
  description = "The type of target with this target group,instance ID or IP"
  type        = string
}

variable "EC2_target_group_certificate_arn" {
description = "The ARN of the default SSL server certificate"
type        = string
}

#AutoScaling Group
variable "EC2_autoscaling_group_max_size" {
  description = "The maximum size of the auto scale group"
  type        = string
}

variable "EC2_autoscaling_group_min_size" {
  description = "The minimum size of the auto scale group"
  type        = string
}

variable "EC2_autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"
  type        = string
}

variable "EC2_autoscaling_group_default_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes before another scaling activity can start"
  type        = number
  default     = 300
}

variable "EC2_autoscaling_group_health_check_grace_period" {
  description = "Time (in seconds) after instance comes into service before checking health"
  type        = number
  default     = 300
}
variable "EC2_autoscaling_group_health_check_type" {
  description = "Controls how health checking is done. Values are - EC2 and ELB"
  type        = string
}

variable "EC2_autoscaling_group_force_delete" {
  description = "Allows deleting the autoscaling group without waiting for all instances in the pool to terminate. You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally, Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially leaves resources dangling"
  type        = bool
  default     = false
}

variable "EC2_LB_targetgroup_arns" {
  description = "A list of aws_alb_target_group ARNs, for use with Application Load Balancing"
  type        = list(string)
  default     = []
}

variable "EC2_autoscaling_group_termination_policies" {
  description = "A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are OldestInstance, NewestInstance, OldestLaunchConfiguration, ClosestToNextInstanceHour, Default"
  type        = list(string)
  default     = ["Default"]
}

variable "EC2_autoscaling_group_suspended_processes" {
  description = "A list of processes to suspend for the AutoScaling Group. The allowed values are Launch, Terminate, HealthCheck, ReplaceUnhealthy, AZRebalance, AlarmNotification, ScheduledActions, AddToLoadBalancer. Note that if you suspend either the Launch or Terminate process types, it can prevent your autoscaling group from functioning properly."
  type        = list(string)
  default     = []
}

variable "EC2_placement_group" {
  description = "The name of the placement group into which you'll launch your instances, if any"
  type        = string
  default     = ""
}

variable "EC2_metrics_granularity" {
  description = "The granularity to associate with the metrics to collect. The only valid value is 1Minute"
  type        = string
  default     = "1Minute"
}

variable "EC2_enabled_metrics" {
  description = "A list of metrics to collect. The allowed values are GroupMinSize, GroupMaxSize, GroupDesiredCapacity, GroupInServiceInstances, GroupPendingInstances, GroupStandbyInstances, GroupTerminatingInstances, GroupTotalInstances"
  type        = list(string)
  default = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}

variable "EC2_ASG_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. (See also Waiting for Capacity below.) Setting this to '0' causes Terraform to skip all Capacity Waiting behavior."
  type        = string
  default     = "10m"
}

variable "EC2_min_elb_capacity" {
  description = "Setting this causes Terraform to wait for this number of instances to show up healthy in the ELB only on creation. Updates will not wait on ELB instance number changes"
  type        = number
  default     = 0
}

variable "EC2_wait_for_elb_capacity" {
  description = "Setting this will cause Terraform to wait for exactly this number of healthy instances in all attached load balancers on both create and update operations. Takes precedence over min_elb_capacity behavior."
  type        = number
  default     = null
}



