# Security Group for Load Balancer

resource "aws_security_group" "lb_security_group" {
  name        = "rsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-03"
  description = "Security group for Load balancer"
  vpc_id      = var.EC2_target_group_vpc_id
  tags        = merge(var.default_tags, map("Name", "rsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-03"))

  #inbound form trusted cidr iprange enabled
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = [var.trusted_cidr_iprange_01,var.trusted_cidr_iprange_02,var.trusted_cidr_iprange_03,var.trusted_cidr_iprange_04]
    description = "Inbound rule open"
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = [var.trusted_cidr_iprange_01,var.trusted_cidr_iprange_02,var.trusted_cidr_iprange_03,var.trusted_cidr_iprange_04]
    description = "Inbound rule open"
  }
  
  
}

#Launch Configuration Security Group

resource "aws_security_group" "lc_security_group" {
  name        = "rsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-04"
  description = "Security group for launch configuration and instances"
  vpc_id      = var.EC2_target_group_vpc_id
  tags        = merge(var.default_tags, map("Name", "rsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-04"))

  #inbound form tool server ip enabled
  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "TCP"
    cidr_blocks = [var.toolserver_ip]
    description = "Inbound rule open"
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    security_groups = ["${aws_security_group.lb_security_group.id}"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
}


#Launch Configuration
resource "aws_launch_configuration" "EC2_launch_configuration" {
  name        = "elc-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
  image_id                    = var.EC2_LC_image_id
  instance_type               = var.EC2_LC_instance_type
  iam_instance_profile        = var.EC2_LC_iam_instance_profile
  key_name                    = var.EC2_LC_key_name
  user_data                   = var.EC2_LC_user_data
  security_groups             = ["${aws_security_group.lb_security_group.id}","${aws_security_group.lc_security_group.id}",var.security_group_01,var.security_group_02]
  associate_public_ip_address = var.EC2_LC_associate_public_ip_address
  enable_monitoring           = var.EC2_LC_enable_monitoring
  ebs_optimized               = var.EC2_LC_ebs_optimized
  lifecycle {
    create_before_destroy = true
  }
}


# Load Balancer
resource "aws_lb" "EC2_alb" {
  name        = "alb-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
  tags        = merge(var.default_tags, map("Name", "alb-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"))
  internal           = var.EC2_load_balancer_type_facing
  load_balancer_type = var.EC2_load_balancer_type
  security_groups    = ["${aws_security_group.lb_security_group.id}","${aws_security_group.lc_security_group.id}"]
  subnets            = var.EC2_load_balancer_subnets
  lifecycle {
    create_before_destroy = true
  }
}

#Target Group
resource "aws_lb_target_group" "EC2_autoscaling_target_group" {
  name        = "alt-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
  tags        = merge(var.default_tags, map("Name", "alt-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"))
  port     = var.EC2_target_group_port
  protocol = var.EC2_target_group_protocol
  vpc_id   = var.EC2_target_group_vpc_id
  target_type = var.EC2_target_group_type
  health_check {    
    healthy_threshold   = 5    
    unhealthy_threshold = 2    
    timeout             = 5    
    interval            = 30    
    path                = var.EC2_target_group_healthcheck_path
    port                = var.EC2_target_group_port
    protocol = var.EC2_target_group_protocol
  }
 lifecycle {
    create_before_destroy = true
  }
}

#Listener
resource "aws_lb_listener" "EC2_lb_listener" {
  load_balancer_arn = "${aws_lb.EC2_alb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.EC2_target_group_certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.EC2_autoscaling_target_group.arn
  }
 lifecycle {
    create_before_destroy = true
  }

}



resource "aws_autoscaling_group" "EC2_autoscaling_group" {
  name        = "asg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
  tags = "${concat(
  list(
      map("key", "name", "value", "asg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01", "propagate_at_launch", true), 
    ),
    var.EC2_autoscaling_group_tags)
  }"
  
  launch_configuration = aws_launch_configuration.EC2_launch_configuration.id
  vpc_zone_identifier  = var.EC2_LC_subnet_id
  max_size             = var.EC2_autoscaling_group_max_size
  min_size             = var.EC2_autoscaling_group_min_size
  desired_capacity     = var.EC2_autoscaling_group_desired_capacity
  health_check_grace_period = var.EC2_autoscaling_group_health_check_grace_period
  health_check_type         = var.EC2_autoscaling_group_health_check_type
  min_elb_capacity          = var.EC2_min_elb_capacity
  wait_for_elb_capacity     = var.EC2_wait_for_elb_capacity
  target_group_arns         = ["${aws_lb_target_group.EC2_autoscaling_target_group.arn}"]
  default_cooldown          = var.EC2_autoscaling_group_default_cooldown
  force_delete              = var.EC2_autoscaling_group_force_delete
  termination_policies      = var.EC2_autoscaling_group_termination_policies
  placement_group           = var.EC2_placement_group
  enabled_metrics           = var.EC2_enabled_metrics
  metrics_granularity       = var.EC2_metrics_granularity
  wait_for_capacity_timeout = var.EC2_ASG_wait_for_capacity_timeout
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_attachment" "EC2_autoscaling_targetattachment" {
  autoscaling_group_name = aws_autoscaling_group.EC2_autoscaling_group.id
  alb_target_group_arn   = aws_lb_target_group.EC2_autoscaling_target_group.arn
}







