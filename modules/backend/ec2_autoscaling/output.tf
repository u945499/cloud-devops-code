output "EC2_Launch_Configuration_Name" {
  value = aws_launch_configuration.EC2_launch_configuration.name
}

output "EC2_Load_Balancer_Name" {
  value = aws_lb.EC2_alb.name
}

output "EC2_Target_Group_Name" {
  value = aws_lb_target_group.EC2_autoscaling_target_group.name
}

output "EC2_Autoscaling_Group_Name" {
  value = aws_autoscaling_group.EC2_autoscaling_group.name
}
