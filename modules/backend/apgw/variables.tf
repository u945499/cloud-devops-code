variable "APP_OPENAPI_TEMPLATE_FILE" {
  description = "API Gateway Open API version 3.0 template file"
}
variable "APP_API_KEY_NAME" {
  description = "API key name for the API gateway"
}
variable "APP_Lambda_name1" {
  description = "API Gateway trigger for lambda function"
}
variable "APP_environment_type" {
  description = "The Environment type to be deployed"
}
variable "APP_app_acronym_codename" {
  description = "Application Acronym Name"
}
variable "APP_appcluster_name" {
  description = "Application Cluster Name"
}
variable "APP_tag_app_owner" {
  description = "IT Application Owner e-mail address"
}
variable "APP_tag_app_support" {
  description = "IT Application Owner or Distribution List of  Application Team"
}
variable "APP_tag_dataProfile" {
  description = "Data Profile"
}
variable "APP_tag_app_name" {
  description = "Application Full Name"
}
variable "APP_tag_managedby" {
  description = "Company Name who is responsible for CloudOps"
}
variable "APP_tag_env" {
  description = "Environment in which Resources are hosted"
}
variable "APP_tag_cloudserviceprovider" {
  description = "cloud service provider"
}
variable "APP_tag_DRlevel" {
  description = "Disaster Recovery Level of the Application"
}
variable "APP_tag_backup" {
  description = "Define backup enabled"
}
