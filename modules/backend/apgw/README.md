Terraform API Gateway/README.md

      1) This API Gateway script is designed to fetch a JSON file which has methods and requests with Lambda AWS proxy.
	  
	  2)  In this module we have a lambda function created earlier and used the lambda function for integration.
	  
	  3)  The JSON file doesn't have any parameterized values, values such as API gateway name and Description and lambda function name inside API gateway to be hardcoded.
	  
	  4) Also this module created API key and attach it to the usage plan.
	  
	  5) This API Gateway is regional.