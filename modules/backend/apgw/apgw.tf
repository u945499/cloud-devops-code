## APIGATEWAY 
#Terraform Version 0.12.20
#Provider Verson 2.52

# AWS API gateway with Open API version 3.0

#Owner  : Ajay.S.ext@bpost.be

#Adapted by :  value ( To be filled later)

#TERRAFORM version : v0.12.20

#AWS provider version : v2.56.0

#Resource Scope : Regional

#DR Applicable : NA

#Availability zone : NA
 

data "template_file" "api_gateway_openapi_spec" {
 template = file(var.APP_OPENAPI_TEMPLATE_FILE)
}

resource "aws_api_gateway_rest_api" "rest_api" {
  description    = "API Gateway for ${var.APP_tag_app_name}"
  name        = join("-", ["apg", var.APP_appcluster_name, var.APP_environment_type, var.APP_app_acronym_codename, "01"])
  body = data.template_file.api_gateway_openapi_spec.rendered
  endpoint_configuration {
    types = ["REGIONAL"]
	}

  tags = {
Name = join("-", ["apg", var.APP_appcluster_name, var.APP_environment_type, var.APP_app_acronym_codename, "01"])
Environment = var.APP_tag_env
ApplicationName = var.APP_tag_app_name
ApplicationOwner = var.APP_tag_app_owner
ManagedBy = var.APP_tag_managedby
CloudServiceProvider = var.APP_tag_cloudserviceprovider
ApplicationAcronym = var.APP_app_acronym_codename
ApplicationSupport = var.APP_tag_app_support
DRLevel = var.APP_tag_DRlevel
Backup = var.APP_tag_backup
DataProfile = var.APP_tag_dataProfile
}
}

resource "aws_lambda_permission" "APP_lambda_permission1" {
   depends_on = [aws_api_gateway_rest_api.rest_api]
   statement_id  = "AllowExecutionFromApiGateway"
   action        = "lambda:InvokeFunction"
   function_name = join("-", ["lmb", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, var.APP_Lambda_name1, "01"])
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.rest_api.execution_arn}/*/POST/reservation"
 }

resource "aws_api_gateway_deployment" "APP_API_deployment" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  stage_name  = "stage"
}

resource "aws_api_gateway_api_key" "APP_API_Key" {
  name=join("-", ["apk", var.APP_appcluster_name, var.APP_environment_type, var.APP_app_acronym_codename, var.APP_API_KEY_NAME])
}

resource "aws_api_gateway_usage_plan" "APP_API_UsagePlan" {
  name=join("-", ["apu", var.APP_appcluster_name, var.APP_environment_type, var.APP_app_acronym_codename, var.APP_API_KEY_NAME])
  description  = "Usage plan for ${var.APP_API_KEY_NAME}"
  api_stages {
    api_id = aws_api_gateway_rest_api.rest_api.id
    stage  = aws_api_gateway_deployment.APP_API_deployment.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "APP_API_KeyUsagePlan" {
  depends_on = [aws_api_gateway_api_key.APP_API_Key, aws_api_gateway_usage_plan.APP_API_UsagePlan]
  key_id        = aws_api_gateway_api_key.APP_API_Key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.APP_API_UsagePlan.id
}
