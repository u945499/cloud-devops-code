output "apikey" {
  value = aws_api_gateway_api_key.APP_API_Key.id
  description = "API key id"
}
output "apikey-value" {
  value = aws_api_gateway_api_key.APP_API_Key.value
  description = "API key value"
}