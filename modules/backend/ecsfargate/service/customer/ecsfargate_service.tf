data "aws_subnet" "Subnet_AZ1_Trusted" {
  id = var.Subnet_ID_AZ1_Trusted
}

data "aws_subnet" "Subnet_AZ2_Trusted" {
  id = var.Subnet_ID_AZ2_Trusted
}

data "aws_subnet" "Subnet_AZ3_Trusted" {
  id = var.Subnet_ID_AZ3_Trusted
}

resource "aws_lb_target_group" "NLB_ECSFargate_Target_Group" {
  name = "nlt-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service_NLB}-1"
  port = var.ECSFargate_Container_Port
  protocol = var.NLB_ECSFargate_Target_Group_Protocol
  deregistration_delay = var.NLB_ECSFargate_Target_Group_Deregistration_Delay
  target_type = var.NLB_ECSFargate_Target_Group_Target_Type
  vpc_id = var.VPC_ID
  health_check {
    healthy_threshold = var.NLB_ECSFargate_Target_Group_Healthy_Threshold
    unhealthy_threshold = var.NLB_ECSFargate_Target_Group_Healthy_Threshold
    protocol = "TCP"
  }
  tags = merge(var.Default_Tags, map("Name","nlt-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service_NLB}-1"), map("Environment",var.App_EnvironmentType))
}

resource "aws_lb_listener" "NLB_ECSFargate_Listener" {
  load_balancer_arn = var.NLB_ID
  port = var.ECSFargate_Container_Port
  protocol = var.NLB_ECSFargate_Listener_Protocol
  default_action {
    target_group_arn = aws_lb_target_group.NLB_ECSFargate_Target_Group.id
    type = var.NLB_ECSFargate_Listener_Default_Action_Type
  }
}

resource "aws_cloudwatch_log_group" "CloudWatch_ECSFargate_Log_Group" {
  name = "/ecs/ctd-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"
  retention_in_days = var.CloudWatch_Log_Group_Retention
  tags = merge(var.Default_Tags, map("Name","ctd-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-cwlog-01"), map("Environment",var.App_EnvironmentType))
}

resource "aws_ecs_task_definition" "ECSFargate_Task_Definition" {
  depends_on = [aws_cloudwatch_log_group.CloudWatch_ECSFargate_Log_Group,]
  family = "ctd-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"
  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu = var.ECSFargate_Task_Definition_CPU
  memory = var.ECSFargate_Task_Definition_Memory
  execution_role_arn = var.IAM_ECSFargate_Task_Role_ARN
  task_role_arn = var.IAM_ECSFargate_Task_Role_ARN
  container_definitions = <<DEFINITION
  [
    {
      "name" : "ctr-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01",
      "image" : "${var.ECR_Repository_URL}",
      "cpu" : ${var.ECSFargate_Task_Definition_CPU},
      "memory" : ${var.ECSFargate_Task_Definition_Memory},
      "environment": [
            {"name": "SERVICE_SECRET", "value": "${var.Secret_Application_ARN}"},
            {"name": "SERVER_PORT", "value": "${var.ECSFargate_Container_Port}"}
      ],
      "portMappings": [
        {
          "protocol" : "tcp",
          "containerPort" : ${var.ECSFargate_Container_Port},
          "hostPort" : ${var.ECSFargate_Container_Port}
        }
      ],
      "logConfiguration" : {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : "/ecs/ctd-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01",
          "awslogs-region" : "${var.AWS_Region}",
          "awslogs-stream-prefix" : "ecs"
        }
      }
    }
  ]
DEFINITION
tags = merge(var.Default_Tags, map("Name","ctd-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"), map("Environment",var.App_EnvironmentType))
}

resource "aws_ecs_service" "ECSFargate_Service" {
  depends_on = [aws_lb_listener.NLB_ECSFargate_Listener,]
  name = "ecs-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"
  cluster = var.ECSFargate_Cluster_ID
  task_definition = aws_ecs_task_definition.ECSFargate_Task_Definition.arn
  desired_count = var.ECSFargate_Task_Desired_Count
  launch_type = "FARGATE"
  deployment_maximum_percent = var.ECSFargate_Deployment_Max_Percent
  deployment_minimum_healthy_percent = var.ECSFargate_Deployment_Min_Healthy_Percent
  health_check_grace_period_seconds  = var.ECSFargate_Health_Check_Grace_Period
  network_configuration {
    security_groups = [var.SG_ECSFargate_Service_ID]
    subnets = [var.Subnet_ID_AZ1_Trusted,var.Subnet_ID_AZ2_Trusted,var.Subnet_ID_AZ3_Trusted]
    assign_public_ip = var.ECSFargate_Assign_Public_IP
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.NLB_ECSFargate_Target_Group.id
    container_name = "ctr-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"
    container_port = var.ECSFargate_Container_Port
  }
  lifecycle {
    ignore_changes = [desired_count]
  }
}

resource "aws_appautoscaling_target" "ECSFargate_Service_Autoscale_Target" {
  service_namespace = "ecs"
  resource_id = "service/${var.ECSFargate_Cluster_Name}/${aws_ecs_service.ECSFargate_Service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity = var.ECSFargate_Servic_Min_Capacity
  max_capacity = var.ECSFargate_Servic_Max_Capacity
  role_arn = var.IAM_ECSFargate_Autoscale_Role_ARN
  lifecycle {
    ignore_changes = [role_arn]
  }
}

resource "aws_appautoscaling_policy" "ECSFargate_Service_Autoscale_Target_Policy_CPU" {
  name = "asp-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-cpu-01"
  policy_type = "TargetTrackingScaling"
  service_namespace = aws_appautoscaling_target.ECSFargate_Service_Autoscale_Target.service_namespace
  scalable_dimension = aws_appautoscaling_target.ECSFargate_Service_Autoscale_Target.scalable_dimension
  resource_id = aws_appautoscaling_target.ECSFargate_Service_Autoscale_Target.resource_id
  target_tracking_scaling_policy_configuration {
      predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = 80
    scale_in_cooldown = "60"
    scale_out_cooldown = "60"
  }
}

resource "aws_appautoscaling_policy" "ECSFargate_Service_Autoscale_Target_Policy_Memory" {
  name = "asp-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-mem-01"
  policy_type = "TargetTrackingScaling"
  service_namespace = aws_appautoscaling_target.ECSFargate_Service_Autoscale_Target.service_namespace
  scalable_dimension = aws_appautoscaling_target.ECSFargate_Service_Autoscale_Target.scalable_dimension
  resource_id = aws_appautoscaling_target.ECSFargate_Service_Autoscale_Target.resource_id
  target_tracking_scaling_policy_configuration {
      predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value = 80
    scale_in_cooldown = "60"
    scale_out_cooldown = "60"
  }
}

resource "aws_cloudwatch_metric_alarm" "Alarm_ECSFargate_Service_CPU_High" {
  alarm_name = "cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-ecsfarsvc-cpuhigh-01"
  alarm_description = "ECS Fargate service CPU utilization is over 80%"
  metric_name = "CPUUtilization"
  namespace = "AWS/ECS"
  period = "300"
  statistic = "Average"
  threshold = "80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  alarm_actions = [var.SNS_Alarm_ARN]
  dimensions = {
    ClusterName = var.ECSFargate_Cluster_Name
    ServiceName = aws_ecs_service.ECSFargate_Service.name
  }
  tags = merge(var.Default_Tags, map("Name","cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-ecsfarsvc-cpuhigh-01"), map("Environment",var.App_EnvironmentType))
}

resource "aws_cloudwatch_metric_alarm" "Alarm_ECSFargate_Service_Mem_High" {
  alarm_name = "cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-ecsfarsvc-memhigh-01"
  alarm_description = "ECS Fargate service memory utilization is over 80%"
  metric_name = "MemoryUtilization"
  namespace = "AWS/ECS"
  period = "300"
  statistic = "Average"
  threshold = "80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  alarm_actions = [var.SNS_Alarm_ARN]
  dimensions = {
    ClusterName = var.ECSFargate_Cluster_Name
    ServiceName = aws_ecs_service.ECSFargate_Service.name
  }
  tags = merge(var.Default_Tags, map("Name","cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-ecsfarsvc-memhigh-01"), map("Environment",var.App_EnvironmentType))
}

resource "aws_cloudwatch_metric_alarm" "Alarm_NLB_Unhealthyhost" {
  alarm_name = "cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-nlb-unhealthyhost-01"
  alarm_description = "Host registered to NLB is Unhealthy"
  metric_name = "UnHealthyHostCount"
  namespace = "AWS/NetworkELB"
  period = "60"
  statistic = "Maximum"
  threshold = "1"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  alarm_actions = [var.SNS_Alarm_ARN]
  dimensions = {
    TargetGroup = aws_lb_target_group.NLB_ECSFargate_Target_Group.arn_suffix
    LoadBalancer = var.NLB_ARNsuffix
  }
  tags = merge(var.Default_Tags, map("Name","cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-nlb-unhealthyhost-01"), map("Environment",var.App_EnvironmentType))
}
