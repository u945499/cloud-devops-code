output "SG_ECSFargate_Service_ID" {
  description = "Security group ID for ECS Fargate Services."
  value = aws_security_group.SG_ECSFargate_Service.id
}
output "IAM_ECSFargate_Task_Role_ARN" {
  description = "ECS Fargate IAM Task Role ARN."
  value = aws_iam_role.IAM_ECSFargate_Task_Role.arn
}
output "IAM_ECSFargate_Autoscale_Role_ARN" {
  description = "ECS Fargate IAM Autoscale Role ARN."
  value = aws_iam_role.IAM_ECSFargate_Autoscale_Role.arn
}
output "ECSFargate_Cluster_ID" {
  description = "ECS Fargate Cluster ID."
  value = aws_ecs_cluster.ECSFargate_Cluster.id
}
output "ECSFargate_Cluster_Name" {
  description = "ECS Fargate Cluster Name."
  value = aws_ecs_cluster.ECSFargate_Cluster.name
}