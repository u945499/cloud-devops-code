data "aws_subnet" "Subnet_AZ1_Trusted" {
  id = var.Subnet_ID_AZ1_Trusted
}

data "aws_subnet" "Subnet_AZ2_Trusted" {
  id = var.Subnet_ID_AZ2_Trusted
}

data "aws_subnet" "Subnet_AZ3_Trusted" {
  id = var.Subnet_ID_AZ3_Trusted
}

resource "aws_security_group" "SG_ECSFargate_Service" {
  name = "csg-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"
  description = "Security group for ${var.App_AcronymCode} application hosted in ECS Fargate Services"
  vpc_id = var.VPC_ID
  ingress {
    description = "Access to AZ1 subnet of NLB"
    from_port = var.ECSFargate_Port_AppService01
    to_port = var.ECSFargate_Port_AppService01
    protocol = "tcp"
    cidr_blocks = [data.aws_subnet.Subnet_AZ1_Trusted.cidr_block]
  }
  ingress {
    description = "Access to AZ2 subnet of NLB"
    from_port = var.ECSFargate_Port_AppService01
    to_port = var.ECSFargate_Port_AppService01
    protocol = "tcp"
    cidr_blocks = [data.aws_subnet.Subnet_AZ2_Trusted.cidr_block]
  }
  ingress {
    description = "Access to AZ3 subnet of NLB"
    from_port = var.ECSFargate_Port_AppService01
    to_port = var.ECSFargate_Port_AppService01
    protocol = "tcp"
    cidr_blocks = [data.aws_subnet.Subnet_AZ3_Trusted.cidr_block]
  }
  ingress {
    description = "Access to AZ1 subnet of NLB"
    from_port = var.ECSFargate_Port_AppService02
    to_port = var.ECSFargate_Port_AppService02
    protocol = "tcp"
    cidr_blocks = [data.aws_subnet.Subnet_AZ1_Trusted.cidr_block]
  }
  ingress {
    description = "Access to AZ2 subnet of NLB"
    from_port = var.ECSFargate_Port_AppService02
    to_port = var.ECSFargate_Port_AppService02
    protocol = "tcp"
    cidr_blocks = [data.aws_subnet.Subnet_AZ2_Trusted.cidr_block]
  }
  ingress {
    description = "Access to AZ3 subnet of NLB"
    from_port = var.ECSFargate_Port_AppService02
    to_port = var.ECSFargate_Port_AppService02
    protocol = "tcp"
    cidr_blocks = [data.aws_subnet.Subnet_AZ3_Trusted.cidr_block]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(var.Default_Tags, map("Name","csg-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"), map("Environment",var.App_EnvironmentType))
}

resource "aws_iam_role" "IAM_ECSFargate_Task_Role" {
  name = "iar-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-ecsfargatetask"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "ecsfargatetaskrole",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      }
    }
  ]
}
EOF
  tags = merge(var.Default_Tags, map("Name","iar-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-ecsfarexecutionrole"), map("Environment",var.App_EnvironmentType))
}

resource "aws_iam_policy" "IAM_ECSFargate_Task_policy" {
  name        = "iap-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-ecsfargatetask"
  description = "Policy that allows ${var.App_AcronymCode} application access AWS services"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "dynamodb",
      "Effect": "Allow",
      "Action": [
        "dynamodb:CreateTable",
        "dynamodb:UpdateTimeToLive",
        "dynamodb:PutItem",
        "dynamodb:DescribeTable",
        "dynamodb:ListTables",
        "dynamodb:DeleteItem",
        "dynamodb:GetItem",
        "dynamodb:Scan",
        "dynamodb:Query",
        "dynamodb:UpdateItem",
        "dynamodb:UpdateTable"
      ],
      "Resource": "*"
    },
    {
      "Sid": "secretsmanager",
      "Effect": "Allow",
      "Action": [
        "secretsmanager:GetSecretValue"
      ],
      "Resource": "*"
    },
    {
      "Sid": "kms",
      "Effect": "Allow",
      "Action": [
        "kms:Decrypt",
        "kms:GenerateDataKey"
      ],
      "Resource": "*"
    },
    {
      "Sid": "ECSTaskExecution",
      "Effect": "Allow",
      "Action": [
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "IAM_ECSFargate_Task_policy" {
  role = aws_iam_role.IAM_ECSFargate_Task_Role.name
  policy_arn = aws_iam_policy.IAM_ECSFargate_Task_policy.arn
}

resource "aws_iam_role" "IAM_ECSFargate_Autoscale_Role" {
  name = "iar-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-ecsfarautoscalerole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "ecsfarautoscalerole"
    }
  ]
}
EOF
  tags = merge(var.Default_Tags, map("Name","iar-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-ecsfarautoscalerole"), map("Environment",var.App_EnvironmentType))
}
resource "aws_iam_role_policy_attachment" "IAM_ECSFargate_Autoscale_Policy" {
  role = aws_iam_role.IAM_ECSFargate_Autoscale_Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole"
}
 resource "aws_ecs_cluster" "ECSFargate_Cluster" {
  name = "ecf-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"
  tags = merge(var.Default_Tags, map("Name","ecf-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"), map("Environment",var.App_EnvironmentType))
}
