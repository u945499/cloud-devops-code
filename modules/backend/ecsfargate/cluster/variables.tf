variable "App_EnvironmentType" {
  description = "Provide the profile name where credentails are stored"
}
variable "App_AcronymCode" {
  description = "Provide the Application Acronymcode"
}
variable "App_ClusterCode" {
  description = "Provide the Application ClusterCode"
}
variable "Default_Tags" {
  description = "Mapping of tags to assign to the resource"
}
variable "Subnet_ID_AZ1_Trusted" {
  description = "Provide eu-west-1a(AZ1) Availability Zone trusted subnet ID"
}
variable "Subnet_ID_AZ2_Trusted" {
  description = "Provide eu-west-1b(AZ2) Availability Zone trusted subnet ID"
}
variable "Subnet_ID_AZ3_Trusted" {
  description = "Provide eu-west-1c(AZ3) Availability Zone trusted subnet ID"
}
variable "VPC_ID" {
  description = "Provide VPC ID in which resouces will be hosted"
}
variable "ECSFargate_Port_AppService01" {
  description = "Specify the port on the container service to associate with the load balancer."
}
variable "ECSFargate_Port_AppService02" {
  description = "Specify the port on the container service to associate with the load balancer."
}
