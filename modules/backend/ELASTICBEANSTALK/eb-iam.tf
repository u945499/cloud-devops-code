# iam roles
resource "aws_iam_role" "app-ec2-role" {
  name               = "iar-elasticbeanstakappec2role-${var.App_Acronym}-${var.App_Environment}"
  tags               = merge(var.default_tags, map("Name", "iar-elasticbeanstakappec2role-${var.App_Acronym}-${var.App_Environment}"))
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_instance_profile" "app-ec2-instanceprofile" {
  name = "iar-ebec2instanceprofile-${var.App_Acronym}-${var.App_Environment}"
  role = aws_iam_role.app-ec2-role.name
}

# service

resource "aws_iam_role" "elasticbeanstalk-service-role" {
  name               = "iar-elasticbeanstakservicerole-${var.App_Acronym}-${var.App_Environment}"
  tags               = merge(var.default_tags, map("Name", "iar-elasticbeanstakservicerole-${var.App_Acronym}-${var.App_Environment}"))
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "elasticbeanstalk.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

#===============================================================================================#
resource "aws_iam_role_policy_attachment" "app" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"
  role       = aws_iam_role.elasticbeanstalk-service-role.name
}
resource "aws_iam_role_policy_attachment" "app2" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
  role       = aws_iam_role.elasticbeanstalk-service-role.name
}
#===============================================================================================#
resource "aws_iam_role_policy_attachment" "app3" {
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
  role       = aws_iam_role.app-ec2-role.name
}
resource "aws_iam_role_policy_attachment" "app4" {
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
  role       = aws_iam_role.app-ec2-role.name
}
resource "aws_iam_role_policy_attachment" "app5" {
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
  role       = aws_iam_role.app-ec2-role.name
}
resource "aws_iam_role_policy_attachment" "app6" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
  role       = aws_iam_role.app-ec2-role.name
}
resource "aws_iam_role_policy_attachment" "app7" {
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
  role       = aws_iam_role.app-ec2-role.name
}