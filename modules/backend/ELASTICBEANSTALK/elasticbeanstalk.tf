#=======================Elastic Beanstalk-Migrated website====================================

resource "aws_elastic_beanstalk_application" "eb-appname" {
  name        = var.App_Acronym
  description = "Elastic Beanstalk Application name - TITLE of the Application"
}

/*
data "aws_elastic_beanstalk_application" "eb-appname" {
  name = var.EB_Application_Name
}
*/
resource "aws_elastic_beanstalk_environment" "eb-env" {
  name = "ebk-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
  #application         = data.aws_elastic_beanstalk_application.eb-appname.name
  application         = aws_elastic_beanstalk_application.eb-appname.name
  solution_stack_name = var.EB_Application_Stack_Name
  cname_prefix        = "${var.App_Acronym}${var.App_Environment}"
  depends_on          = [aws_security_group.Load-Balancer-Security-Group]
  tags                = var.default_tags

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "EnvironmentType"
    value     = var.EB_Application_Type
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = var.EB_Load_Balancer_Type
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = aws_iam_role.elasticbeanstalk-service-role.name
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "Availability Zones"
   value     = var.EB_Availability_Zones
  }

  # Autoscaling Configurations

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "MeasureName"
    value     = var.EB_AutoScaling_Measure_Name
  }
  setting {
    name      = "Unit"
    namespace = "aws:autoscaling:trigger"
    value     = "Percent"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = var.EB_AutoScaling_Minimum_Instance_Count
  }
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = var.EB_AutoScaling_Maximum_Instance_Count
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperThreshold"
    value     = var.EB_AutoScaling_UpperThreshold
  }


  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerThreshold"
    value     = var.EB_AutoScaling_LowerThreshold
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Period"
    value     = var.EB_AutoScaling_Period
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperBreachScaleIncrement"
    value     = var.EB_AutoScaling_Upper_BreachScaleIncrement
  }
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerBreachScaleIncrement"
    value     = var.EB_AutoScaling_Lower_BreachScaleIncrement
  }
  #===========================================================
  #Update -deployemnet details
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateEnabled"
    value     = var.EB_Rolling_Update_Enabled
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateType"
    value     = var.EB_Rolling_Update_Type
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "MinInstancesInService"
    value     = var.EB_Updating_Min_In_Service
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "DeploymentPolicy"
    value     = var.EB_Rolling_Update_Type == "Immutable" ? "Immutable" : "Rolling"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "MaxBatchSize"
    value     = var.EB_Updating_Max_Batch
  }
  #================================================================================

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = var.VPC_ID
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${var.Subnet_ID_AZ1_Trusted},${var.Subnet_ID_AZ2_Trusted}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = var.EB_EC2_AssociatePublicIpAddress
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.app-ec2-instanceprofile.name
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = aws_security_group.eb-security-group.id
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = var.key_pair_name
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = var.EB_Instance_Type
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "RootVolumeType"
    value     = var.EB_Root_Volume_Type
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "ImageId"
    value     = var.EB_AMI_ID
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBScheme"
    value     = var.EB_Elb_Scheme
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "${var.Subnet_ID_AZ1_DMZ},${var.Subnet_ID_AZ2_DMZ}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "AWS_REGION"
    value     = var.AWS_Region
  }
  setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name      = "SystemType"
    value     = var.EB_Health_Reporting
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = var.EB_Setting_Streamlogs
  }
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = var.EB_Cloudwatch_Logs_Delete_On_Terminate
  }
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "RetentionInDays"
    value     = var.EB_CloudWatch_Log_Retention_Days
  }
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs:health"
    name      = "HealthStreamingEnabled"
    value     = var.EB_Health_Streaming_Enabled
  }
  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs:health"
    name      = "DeleteOnTerminate"
    value     = var.EB_Health_Streaming_Delete_on_Terminate
  }


  #http settingts
  setting {
    namespace = "aws:elbv2:loadbalancer"
    name      = "SecurityGroups"
    value     = aws_security_group.Load-Balancer-Security-Group.id
  }
  setting {
    namespace = "aws:elb:loadbalancer"
    name = "ManagedSecurityGroup"
    value = aws_security_group.Load-Balancer-Security-Group.id
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "ListenerEnabled"
    value     = var.EB_Elbv2_Linstner_Enabled_Status
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "Protocol"
    value     = var.EB_Elbv2_Linstner_Protocol
  }
  setting {
    namespace = "aws:elbv2:listener:443"
    name      = "SSLCertificateArns"
    value     = var.EB_Elbv2_Listner_SSLCert_Arn
  }
  setting {
    namespace = "aws:elbv2:listenerrule:default"
    name      = "Process"
    value     = "default"
  }
  setting {
    namespace = "aws:elbv2:loadbalancer"
    name      = "AccessLogsS3Bucket"
    value     = join("", aws_s3_bucket.S3_Log_Bucket.*.id)
  }

  setting {
    namespace = "aws:elbv2:loadbalancer"
    name      = "AccessLogsS3Enabled"
    value     = var.EB_Lb_Log_Status
  }
  # Configure notifications for your environment.
  setting {
    namespace = "aws:elasticbeanstalk:sns:topics"
    name      = "Notification Topic ARN"
    value     = aws_sns_topic.sns.arn
  }
}

#The below S3 Bucket is used for storing LB logs also can be used to save any other logs

resource "aws_s3_bucket" "S3_Log_Bucket" {
  bucket = "s3b-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-logs-01"
  acl    = "private"
  tags   = merge(var.default_tags, map("Name", "s3b-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-logs-01"), map("Environment", var.App_Environment))
  lifecycle_rule {
    enabled = true
    prefix  = "EB_Lb_Logs"
    id      = "logs_3_month_retention"
    expiration {
      days = 90
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "S3_Log_Bucket_Policy" {
  depends_on = [aws_s3_bucket.S3_Log_Bucket]
  bucket     = aws_s3_bucket.S3_Log_Bucket.id
  policy     = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "MyPolicy",
    "Statement": [
        {
        	"Effect": "Deny",
        	"Principal": "*",
        	"Action": "s3:*",
        	"Resource": "arn:aws:s3:::${aws_s3_bucket.S3_Log_Bucket.id}//*",
        	"Condition": {
        		"Bool": {
        			"aws:SecureTransport": "false"
        		}
        	}
        }, {
        	"Effect": "Allow",
        	"Principal": {
        		"AWS": "arn:aws:iam::156460612806:root"
        	},
        	"Action": "s3:PutObject",
        	"Resource": "arn:aws:s3:::${aws_s3_bucket.S3_Log_Bucket.id}/*"
        }, {
        	"Effect": "Allow",
        	"Principal": {
        		"Service": "delivery.logs.amazonaws.com"
        	},
        	"Action": "s3:PutObject",
        	"Resource": "${aws_s3_bucket.S3_Log_Bucket.arn}/*",
        	"Condition": {
        		"StringEquals": {
        			"s3:x-amz-acl": "bucket-owner-full-control"
        		}
        	}
        }, {
        	"Effect": "Allow",
        	"Principal": {
        		"Service": "delivery.logs.amazonaws.com"
        	},
        	"Action": "s3:GetBucketAcl",
        	"Resource": "${aws_s3_bucket.S3_Log_Bucket.arn}"
        }]
    }
POLICY
}
resource "aws_sns_topic" "sns" {
  name = "sns-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
}

resource "aws_sns_topic_policy" "sns_policy" {
  arn    = aws_sns_topic.sns.arn
  policy = data.aws_iam_policy_document.sns_topic_policy.json
}
data "aws_caller_identity" "sns" {}
data "aws_iam_policy_document" "sns_topic_policy" {
  policy_id = "__default_policy_ID"

  statement {
    sid = "__default_statement_ID"

    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    effect = "Allow"
    resources = [
    aws_sns_topic.sns.arn]

    principals {
      type = "AWS"
      identifiers = [
      "*"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.sns.account_id
      ]
    }
  }
}