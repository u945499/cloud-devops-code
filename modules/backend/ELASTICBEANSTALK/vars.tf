#Common
variable "App_Cluster" {}
variable "App_Environment" {}
variable "App_Acronym" {}
variable "AWS_Region" {}
variable "profile" {}
variable "default_tags" {}
variable "Tools_Server_Ip" {}
variable "VPC_ID" {}
variable "AWS_Account_Number" {}

variable "Subnet_ID_AZ1_Trusted" {}
variable "Subnet_ID_AZ2_Trusted" {}
variable "Subnet_ID_AZ3_Trusted" {}
#variable "Subnet_ID_AZ1_Restricted" {}
#variable "Subnet_ID_AZ2_Restricted" {}
#variable "Subnet_ID_AZ3_Restricted" {}
variable "Subnet_ID_AZ1_DMZ" {}
variable "Subnet_ID_AZ2_DMZ" {}
variable "Subnet_ID_AZ3_DMZ" {}

variable "Subnet_ID_AZ1_DMZ_IPRange" {}
variable "Subnet_ID_AZ2_DMZ_IPRange" {}
variable "Subnet_ID_AZ3_DMZ_IPRange" {}
variable "Subnet_ID_AZ1_Trusted_IPrange" {}
variable "Subnet_ID_AZ2_Trusted_IPrange" {}
variable "Subnet_ID_AZ3_Trusted_IPrange" {}

#Application Specific Variable Details

#variable "EB_Environment_Name" {}
variable "EB_EC2_AssociatePublicIpAddress" {}
variable "EB_Application_Stack_Name" {}
variable "EB_Application_Type" {}
variable "EB_Load_Balancer_Type" {}
variable "EB_Availability_Zones" {}
variable "EB_AutoScaling_Minimum_Instance_Count" {}
variable "EB_AutoScaling_Maximum_Instance_Count" {}
variable "EB_AutoScaling_LowerThreshold" {}
variable "EB_AutoScaling_UpperThreshold" {}
variable "EB_AutoScaling_Measure_Name" {}
variable "EB_AutoScaling_Upper_BreachScaleIncrement" {}
variable "EB_AutoScaling_Lower_BreachScaleIncrement" {}
variable "EB_AutoScaling_Period" {}
variable "EB_Instance_Type" {}
variable "EB_Root_Volume_Type" {}
variable "EB_Health_Reporting" {}
variable "EB_CloudWatch_Log_Retention_Days" {}
variable "EB_Lb_Log_Status" {}
variable "EB_AMI_ID" {}
variable "EB_Elbv2_Linstner_Enabled_Status" {}
variable "EB_Elbv2_Linstner_Protocol" {}
variable "EB_Elbv2_Listner_SSLCert_Arn" {}
variable "EB_Elb_Scheme" {}
variable "EB_Cloudwatch_Logs_Delete_On_Terminate" {}
variable "EB_Health_Streaming_Delete_on_Terminate" {}
variable "EB_Rolling_Update_Enabled" {}
variable "EB_Rolling_Update_Type" {}
variable "EB_Updating_Min_In_Service" {}
variable "EB_Updating_Max_Batch" {}
variable "EB_Setting_Streamlogs" {}
variable "EB_Health_Streaming_Enabled" {}


variable "key_pair_name" {}


variable "s3_kms_arn" {}
#variable "secret_kms_arn " {}