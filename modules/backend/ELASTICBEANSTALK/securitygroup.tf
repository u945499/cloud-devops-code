resource "aws_security_group" "eb-security-group" {
  vpc_id      = var.VPC_ID
  name        = "esg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-eb-01"
  description = "security group for my app"
  depends_on  = [aws_security_group.Load-Balancer-Security-Group]
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.Tools_Server_Ip]
  }

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.Load-Balancer-Security-Group.id]
  }

  #Ingress from DMZ Zone
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.Subnet_ID_AZ1_DMZ_IPRange, var.Subnet_ID_AZ2_DMZ_IPRange]
  }
  tags = merge(var.default_tags, map("Name", "esg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-eb-01"))
}

##### ALB security group

resource "aws_security_group" "Load-Balancer-Security-Group" {
  name   = "lsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-applb-01"
  vpc_id = var.VPC_ID

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Inbound calls from  internet"
  }

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "TCP"
    ipv6_cidr_blocks = ["::/0"]
    description      = "Inbound calls from UI via internet"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(var.default_tags, map("Name", "lsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-applb-01"))
}
