variable "App_EnvironmentType" {
  description = "Provide the profile name where credentails are stored"
}
variable "App_AcronymCode" {
  description = "Provide the Application Acronymcode"
}
variable "App_ClusterCode" {
  description = "Provide the Application ClusterCode"
}
variable "Default_Tags" {
description = "Mapping of tags to assign to the resource"
}
variable "Securitygroup_Vpc_Id" {
description = "specify vpc id to attach with securitygroup"
}
variable "Securitygroup_Vpc_CIDR" {
description = "specify vpc CIDR to attach with securitygroup"
}
variable "Securitygroup_Port" {
description = "specify security group port"
}