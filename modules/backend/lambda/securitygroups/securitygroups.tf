resource "aws_security_group" "Securitygroup_Lambda" {
  name        = "lsg-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"
  description = "Security group for lambda"
  vpc_id      = var.Securitygroup_Vpc_Id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = var.Securitygroup_Port
    to_port     = var.Securitygroup_Port
    protocol    = "tcp"
    cidr_blocks = [var.Securitygroup_Vpc_CIDR]
 }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = merge(var.Default_Tags, map("Name","lsg-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"), map("Environment",var.App_EnvironmentType))
}