variable "default_tags" {
    default = { 
       ApplicationOwner = "Nathalie.DECK@bpost.be",
	  ApplicationName = "Bpaid",
	  ApplicationSupport = "vijeshraj.manisserykunnath.ext@bpost.be",
	  ManagedBy = "TCS",
	  CloudServiceProvider = "AWS",
	  DataProfile = "Confidential",
    Backup = "True",
    ApplicationAcronym = "Bpaid",
    Environment = "Non production",
    DRLevel = "2"

  } 
}
variable "App_EnvironmentType" {
  description = "Provide the profile name where credentails are stored"
}
variable "App_AcronymCode" {
  description = "Provide the Application Acronymcode"
}
variable "App_ClusterCode" {
  description = "Provide the Application ClusterCode"
}
variable "Default_Tags" {
description = "Mapping of tags to assign to the resource"
}
variable "Lambda_Runtime" {
 description = "Specify runtime for lambda function"
}
variable "Lambda_Loggroup_Retention" {
 description = "Specify retention period for log groups"
}
variable "Lambda_S3bucket" {
 description = "specify s3 bucket contains zip code"
}
variable "Lambda_S3key" {
 description = "Specify path in s3 bucket contains zip code"
}
variable "Subnet_ID_AZ1_Trusted" {
  description = "Provide eu-west-1a(AZ1) Availability Zone trusted subnet ID"
}
variable "Subnet_ID_AZ2_Trusted" {
  description = "Provide eu-west-1b(AZ2) Availability Zone trusted subnet ID"
}
variable "Lambda_securitygroup_Id" {
   description = "Provide lambda security group id" 
}
variable "SNS_Alarm_ARN" {
 description = "cloudwatch SNS topic ID"
}