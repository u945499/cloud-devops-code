resource "aws_iam_role" "IAMrole_For_Lambda" {
  name = "iar-lambda-new"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_cloudwatch_log_group" "LogGroup_For_Lambda" {
  name              = "/aws/lambda/lmb-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"
  retention_in_days = var.Lambda_Loggroup_Retention
}

resource "aws_iam_policy" "Iampolicy_For_Lambda" {
  name        = "iap-lambda-new"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "ec2:DescribeNetworkInterfaces",
                "ec2:CreateNetworkInterface",
                "ec2:DeleteNetworkInterface",
                "ec2:DescribeInstances",
                "ec2:AttachNetworkInterface"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "Iampolicyattach_For_Lambda" {
  role       = aws_iam_role.IAMrole_For_Lambda.name
  policy_arn = aws_iam_policy.Iampolicy_For_Lambda.arn
}
data "aws_s3_bucket_object" "S3_Lambda_Zipcode" {
  bucket = var.Lambda_S3bucket
  key    = var.Lambda_S3key
}
resource "aws_lambda_function" "Lambda" {
  s3_bucket        = data.aws_s3_bucket_object.S3_Lambda_Zipcode.bucket
	s3_key           = data.aws_s3_bucket_object.S3_Lambda_Zipcode.key
  function_name = "lmb-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"
  role          = aws_iam_role.IAMrole_For_Lambda.arn
 handler       = "index.handler"
 depends_on = [aws_iam_role_policy_attachment.Iampolicyattach_For_Lambda, aws_cloudwatch_log_group.LogGroup_For_Lambda]
  runtime = var.Lambda_Runtime
  vpc_config {
  subnet_ids = [var.Subnet_ID_AZ1_Trusted,var.Subnet_ID_AZ2_Trusted]
  security_group_ids = [var.Lambda_securitygroup_Id]
  }
environment {
    variables = {
      environment = var.App_EnvironmentType
    }
}
    tags = merge(var.Default_Tags, map("Name","s3b-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-01"), map("Environment",var.App_EnvironmentType))
}
resource "aws_cloudwatch_metric_alarm" "Alarm_Lambda_Errors" {
  alarm_name = "cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-lambda-errors-01"
  alarm_description = "Number of errors in lambda"
  metric_name = "Errors"
  namespace = "AWS/Lambda"
  period = "60"
  statistic = "Sum"
  threshold = "0.5"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  alarm_actions = [var.SNS_Alarm_ARN]
  dimensions {
      FunctionName = "${aws_lambda_function.Lambda.function_name}"
      Resource     = "${aws_lambda_function.Lambda.function_name}"
    }
  tags = merge(var.Default_Tags, map("Name","cwa-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-lambda-errors-01"), map("Environment",var.App_EnvironmentType))
}