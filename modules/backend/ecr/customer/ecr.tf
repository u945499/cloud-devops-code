resource "aws_ecr_repository" "ECR" {
  name = "ecr-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"
  tags = merge(var.Default_Tags, map("Name","ecr-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-${var.App_Service}-01"), map("Environment",var.App_EnvironmentType))
}

resource "aws_ecr_lifecycle_policy" "recr_ecsfargate_lifecycle_policy" {
  repository = aws_ecr_repository.ECR.name
  policy = <<EOF
  {
    "rules": [
        {
            "rulePriority": 1,
            "description": "Retain 10 Latest Images",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 10
            },
            "action": {
                "type": "expire"
            }
        }
    ]
  }
EOF
}
