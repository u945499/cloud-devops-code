variable "App_EnvironmentType" {
  description = "Provide the profile name where credentails are stored"
}
variable "App_AcronymCode" {
  description = "Provide the Application Acronymcode"
}
variable "App_ClusterCode" {
  description = "Provide the Application ClusterCode"
}
variable "Default_Tags" {
  description = "Mapping of tags to assign to the resource"
}
variable "AWS_Account_Number" {
  description = "Provide the AWS account number in which resources will be hosted"
}
variable "KMS_Deletion_Window_In_Days" {
  description = "Specify kms Key deletion window in days"
}
variable "KMS_Enable_Key_Rotation" {
  description = "Specify if kms Key needs to be rotated"
}
variable "KMS_Key_Secret_Alias" {
  description = "Provide Secrets Key alias name"
}



