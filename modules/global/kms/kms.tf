resource "aws_kms_key" "KMS_Key_S3" {
  description = "KMS key for S3"
  deletion_window_in_days = var.KMS_Deletion_Window_In_Days
  enable_key_rotation = var.KMS_Enable_Key_Rotation
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "kmskeypolicy",
  "Statement": [
    {
      "Sid": "Enable root User full permissions",
      "Effect": "Allow",
      "Principal": {"AWS": "arn:aws:iam::${var.AWS_Account_Number}:root"},
      "Action": "kms:*",
      "Resource": "*"
    }
  ]
}
EOF
  tags = merge(var.Default_Tags, map("Name","kms-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-secret-01"), map("Environment",var.App_EnvironmentType))
}
resource "aws_kms_alias" "KMS_Key_Secret_Alias" {
  name = "alias/${var.KMS_Key_Secret_Alias}"
  target_key_id = aws_kms_key.KMS_Key_S3.id
}
