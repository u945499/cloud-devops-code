output "KMS_Key_Secret_ARN" {
  description = "The arn of the secrets key"
  value = aws_kms_key.KMS_Key_S3.arn
}

