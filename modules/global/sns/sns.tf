resource "aws_sns_topic" "SNS_Topic_Alarm" {
  name = "sns-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-alarm-01"
  display_name = "alarm-${var.App_ClusterCode}-${var.App_EnvironmentType}-${var.App_AcronymCode}"
  tags = merge(var.Default_Tags, map("Name","sns-${var.App_ClusterCode}-${var.App_EnvironmentType}-euwe01-${var.App_AcronymCode}-alarm-01"), map("Environment",var.App_EnvironmentType))
}