variable "APP_subnet1" {
  description = "Provide the subnet in which resorces will be hosted"
}
variable "APP_subnet2" {
  description = "Provide the subnet in which resorces will be hosted"
}
variable "APP_vpc_id" {
  description = "Provide the VPC ID in which resorces will be hosted"
}
variable "APP_kms_key_arn" {
  description = "Provide the KMS key arn for the database"
}
variable "APP_RDS_family" {
  description = "Provide the RDS family which database will be created"
}
variable "APP_RDS_from_port" {
  description = "Provide the RDS database from port"
}
variable "APP_RDS_to_port" {
  description = "Provide the RDS database to port"
}
variable "APP_RDS_protocol" {
  description = "Provide the RDS protocol for the database"
}
variable "APP_RDS_cidr_blocks1" {
  description = "Provide the cidr_blocks for the RDS security group"
}
variable "APP_RDS_cidr_blocks2" {
  description = "Provide the cidr_blocks for the RDS security group"
}
variable "APP_RDS_cidr_blocks3" {
  description = "Provide the cidr_blocks for the RDS security group"
}
variable "APP_RDS_engine" {
  description = "Provide the RDS Engine of the database"
}
variable "APP_RDS_engine_version" {
  description = "Provide the RDS Engine version of the database"
}
variable "APP_RDS_engine_mode" {
  description = "Provide the RDS Engine mode of the database"
}
variable "APP_availability_zone1" {
  description = "Provide availability zone for which resorces will be hosted"
}
variable "APP_availability_zone2" {
  description = "Provide availability zone for which resorces will be hosted"
}
variable "APP_RDS_cluster_identifier1" {
  description = "Provide cluster identifier name for the database"
}
variable "APP_RDS_database_name1" {
  description = "Provide the database name of the RDS database"
}
variable "APP_RDS_backup_retention_period" {
  description = "Provide the database backup retention period of the RDS database"
}
variable "APP_RDS_preferred_backup_window" {
  description = "Provide the database Preferred backup window of the RDS database"
}
variable "APP_RDS_preferred_maintenance_window" {
  description = "Provide the database Preferred maintenance window of the RDS database"
}
variable "APP_RDS_port" {
  description = "Provide the RDS port of the database"
}
variable "APP_RDS_min_capacity" {
  description = "Provide the RDS min capacity of the database"
}
variable "APP_RDS_max_capacity" {
  description = "Provide the RDS max capacity of the database"
}
variable "aws_region" {
  description = "Provide the region in which resorces will be hosted"
}
variable "profile" {
  description = "Provide the profile name where credentails are stored"
}
variable "APP_environment_type" {
  description = "The Environment type to be deployed"
}
variable "APP_app_acronym_codename" {
  description = "Application Acronym Name"
}
variable "APP_appcluster_name" {
  description = "Application Cluster Name"
}
variable "APP_tag_app_owner" {
  description = "IT Application Owner e-mail address"
}
variable "APP_tag_app_support" {
  description = "IT Application Owner or Distribution List of  Application Team"
}
variable "APP_tag_dataProfile" {
  description = "Data Profile"
}
variable "APP_tag_app_name" {
  description = "Application Full Name"
}
variable "APP_tag_managedby" {
  description = "Company Name who is responsible for CloudOps"
}
variable "APP_tag_env" {
  description = "Environment in which Resources are hosted"
}
variable "APP_tag_cloudserviceprovider" {
  description = "cloud service provider"
}
variable "APP_tag_DRlevel" {
  description = "Disaster Recovery Level of the Application"
}
variable "APP_tag_backup" {
  description = "Define backup enabled"
}
variable "APP_secret_name1"{
  description = "secret name for RDS item database"
}
variable "APP_RDS_master_username1"{
  description = "Master RDS username for RDS item database"
}


