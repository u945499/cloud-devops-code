output "rds-arn1" {
  value = aws_rds_cluster.aurora_postgresql-01.arn
  description = "RDS DB cluster arn"
}
output "rds-port" {
  value = aws_rds_cluster.aurora_postgresql-01.port
  description = "RDS DB port"
}
output "rds-clustername1" {
  value = aws_rds_cluster.aurora_postgresql-01.cluster_identifier
  description = "RDS DB cluster name"
}


