# AWS Aurora postgreSQL compatibile version 10.7 serverless

#Owner  : Ajay.S.ext@bpost.be

#Adapted by :  value ( To be filled later)

#TERRAFORM version : v0.12.20

#AWS provider version : v2.56.0

#Resource Scope : VPC

#DR Applicable : DR2

#Availability zone : AZ1,AZ2

resource "aws_secretsmanager_secret" "secret1" {
  description         = "srvless/aurora/postgresql/10.7"
  kms_key_id          = var.APP_kms_key_arn
  name = join("-", ["scr", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, var.APP_secret_name1, "01"])
  tags = {
Name = join("-", ["scr", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, var.APP_secret_name1, "01"])
Environment = var.APP_tag_env
ApplicationName = var.APP_tag_app_name
ApplicationOwner = var.APP_tag_app_owner
ManagedBy = var.APP_tag_managedby
CloudServiceProvider = var.APP_tag_cloudserviceprovider
ApplicationAcronym = var.APP_app_acronym_codename
ApplicationSupport = var.APP_tag_app_support
DRLevel = var.APP_tag_DRlevel
Backup = var.APP_tag_backup
DataProfile = var.APP_tag_dataProfile
}
}

resource "aws_secretsmanager_secret_version" "secret1" {
  depends_on      = [random_password.rpassword_rds]
  secret_id     = aws_secretsmanager_secret.secret1.id
  secret_string = <<EOF
{
  "master_username": "${var.APP_RDS_master_username1}",
  "master_password": "${random_password.rpassword_rds.result}",
  "username": "",
  "password": "",
  "engine" : "${var.APP_RDS_engine}",
  "host" : "",
  "port": "${var.APP_RDS_port}",
  "dbClusterIdentifier": ""
}
EOF
} 


resource "random_password" "rpassword_rds" {
  length = 16
  special = true
  override_special = "$-*"
}

resource "aws_db_subnet_group" "rds-subnet-group" {
  name       = join("-", ["rsn", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  subnet_ids = [var.APP_subnet1, var.APP_subnet2]
  tags = {
  Name = join("-", ["rsn", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  Environment = var.APP_tag_env
  ApplicationName = var.APP_tag_app_name
  ApplicationOwner = var.APP_tag_app_owner
  ManagedBy = var.APP_tag_managedby
  CloudServiceProvider = var.APP_tag_cloudserviceprovider
  ApplicationAcronym = var.APP_app_acronym_codename
  ApplicationSupport = var.APP_tag_app_support
  DRLevel = var.APP_tag_DRlevel
  Backup = var.APP_tag_backup
  DataProfile = var.APP_tag_dataProfile
  }
  }
  
resource "aws_rds_cluster_parameter_group" "rds-pg" {
  name   = join("-", ["rpg", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  family = var.APP_RDS_family
  }

resource "aws_security_group" "rds_security_group" {
  name        = join("-", ["rsg", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  vpc_id      = var.APP_vpc_id
  ingress {
    from_port   = var.APP_RDS_from_port
    to_port     = var.APP_RDS_to_port
    protocol    = var.APP_RDS_protocol
    cidr_blocks = [var.APP_RDS_cidr_blocks1, var.APP_RDS_cidr_blocks2, var.APP_RDS_cidr_blocks3]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
  Name = join("-", ["rsg", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, "01"])
  Environment = var.APP_tag_env
  ApplicationName = var.APP_tag_app_name
  ApplicationOwner = var.APP_tag_app_owner
  ManagedBy = var.APP_tag_managedby
  CloudServiceProvider = var.APP_tag_cloudserviceprovider
  ApplicationAcronym = var.APP_app_acronym_codename
  ApplicationSupport = var.APP_tag_app_support
  DRLevel = var.APP_tag_DRlevel
  Backup = var.APP_tag_backup
  DataProfile = var.APP_tag_dataProfile
  }
}
resource "aws_rds_cluster" "aurora_postgresql-01" {
  cluster_identifier      = join("-", ["rdi", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, var.APP_RDS_cluster_identifier1, "01"])
  engine                  = var.APP_RDS_engine
  engine_version          = var.APP_RDS_engine_version
  engine_mode             = var.APP_RDS_engine_mode
  availability_zones      = [var.APP_availability_zone1, var.APP_availability_zone2]
  database_name           = var.APP_RDS_database_name1
  master_username         = "${jsondecode(aws_secretsmanager_secret_version.secret1.secret_string)["master_username"]}"
  master_password         = "${jsondecode(aws_secretsmanager_secret_version.secret1.secret_string)["master_password"]}"
  backup_retention_period = var.APP_RDS_backup_retention_period
  preferred_backup_window = var.APP_RDS_preferred_backup_window
  copy_tags_to_snapshot = true
  final_snapshot_identifier = join("-", ["rdc", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, var.APP_RDS_cluster_identifier1, "01"])
  skip_final_snapshot = false
  preferred_maintenance_window = var.APP_RDS_preferred_maintenance_window
  port = var.APP_RDS_port
  vpc_security_group_ids = [aws_security_group.rds_security_group.id]
  storage_encrypted =true
  apply_immediately = false
  deletion_protection = true
  db_subnet_group_name = aws_db_subnet_group.rds-subnet-group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.rds-pg.id
  kms_key_id = var.APP_kms_key_arn
  #iam_roles =
  scaling_configuration {
    auto_pause               = false
    max_capacity             = var.APP_RDS_max_capacity
    min_capacity             = var.APP_RDS_min_capacity
    #seconds_until_auto_pause = 300
    timeout_action           = "ForceApplyCapacityChange"
  }
 lifecycle {
    ignore_changes = [availability_zones]
  }
  tags = {
Name = join("-", ["rdi", var.APP_appcluster_name, var.APP_environment_type, "euwe01", var.APP_app_acronym_codename, var.APP_RDS_cluster_identifier1, "01"])
Environment = var.APP_tag_env
ApplicationName = var.APP_tag_app_name
ApplicationOwner = var.APP_tag_app_owner
ManagedBy = var.APP_tag_managedby
CloudServiceProvider = var.APP_tag_cloudserviceprovider
ApplicationAcronym = var.APP_app_acronym_codename
ApplicationSupport = var.APP_tag_app_support
DRLevel = var.APP_tag_DRlevel
Backup = var.APP_tag_backup
DataProfile = var.APP_tag_dataProfile
}
}
