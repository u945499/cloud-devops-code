Terraform data-storage/README.md

1) This module is designed to create RDS Aurora PostgresSQL (10.7) database serverless.

2) Master password for the database is created with random password generation function and stored in the secret manager.

3) APP user to be created manually and stored in the secret manager. APP user to be shared to the application team for DB connection.

4) DB parameter group is created with default setting.

5) Lifecycle to be added for the availability zones.