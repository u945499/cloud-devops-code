#Common
variable "AWS_REGION" {}
variable "profile" {}
variable "default_tags" {}
variable "App_Cluster" {}
variable "App_Environment" {}
variable "App_Acronym" {}
variable "service" {}
variable "trusted_subnet_id_az1" {}
variable "trusted_subnet_id_az2" {}
variable "trusted_subnet_id_az3" {}
variable "restricted_subnet_id_az1" {}
variable "restricted_subnet_id_az2" {}
variable "restricted_subnet_id_az3" {}
variable "trusted_cidr_iprange_az1" {}
variable "trusted_cidr_iprange_az2" {}
variable "trusted_cidr_iprange_az3" {}
variable "tools_server_ip" {}
variable "vpc_ip_range" {}
variable "vpc_id" {}
variable "authorized_aws_accounts" {}


#RDS - Aurora for Drupal Application
variable "secret_kms_arn" {}
variable "apply_immediately" {}
variable "enabled_cloudwatch_logs_exports" {}
variable "db_port" {}
variable "db_subnet_Group_ids" {
  type = list(string)
}
variable "availability_zones" {
  type        = list(string)
  description = "availability zones"
}
variable "engine" {}
variable "rds_instance_count" {}
variable "engine_version" {}
variable "instance_type" {}
variable "rds_kms_key" {}
variable "maintenance-window" {}
variable "skip_final_snapshot" {}     #Set as true for skipping final snapshot creation while deleting the resource else false
variable "backup_retention_period" {} #he days to retain backups for. Must be between 0 and 35. When creating a Read Replica the value must be greater than 0"
variable "backup_window" {}
variable "copy_tags_to_snapshot" {}
variable "storage_encrypted" {} #Specifies whether the DB cluster is encrypted. The default is `false` for `provisioned` `engine_mode` and `true` for `serverless` `engine_mode
variable "deletion_protection" {}




