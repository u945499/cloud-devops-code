output "SecretManagerName" {
  value = aws_secretsmanager_secret.secrets2.name
}

output "DatabaseName" {
  value = aws_rds_cluster.rdscluster.database_name
}

output "RdsHostName" {
  value = aws_rds_cluster.rdscluster.endpoint
}
