#Master password for rds
resource "aws_secretsmanager_secret" "secrets1" {
  description = "RDS Master user details"
  name        = "asm-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-rdsmaster-01"
  tags        = merge(var.default_tags, map("Name", "asm-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-rdsmaster-01"))
  kms_key_id = var.secret_kms_arn
  recovery_window_in_days = "0"
}

resource "random_password" "master-password" {
  length = 16
  special = true
  override_special = "$-*"
}

resource "aws_secretsmanager_secret_version" "secret-version1" {
  secret_id     = aws_secretsmanager_secret.secrets1.id
  secret_string = jsonencode(local.mastersecrets)
}

locals {
  mastersecrets = {
    masterusername             = "${var.App_Acronym}_${var.App_Environment}_owner"
    masterpassword             = random_password.master-password.result
  }
}

#=================================================================================
resource "aws_secretsmanager_secret" "secrets2" {
  description = "RDS Application user Details"
  name        = "asm-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-rdsappuser-01"
  tags        = merge(var.default_tags, map("Name", "asm-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-rdsappuser-01"))
  kms_key_id = var.secret_kms_arn
  recovery_window_in_days = "0"
}

resource "random_password" "rdsappuser-password" {
  length = 16
  special = true
  override_special = "$-*"
}
resource "aws_secretsmanager_secret_version" "secret-version2" {
  secret_id     = aws_secretsmanager_secret.secrets2.id
  secret_string = jsonencode(local.appusersecrets)
}

locals {
  appusersecrets = {
    username = "${var.App_Acronym}_${var.App_Environment}_user"
    password = random_password.rdsappuser-password.result
    port = var.db_port
    dbname = "db${var.App_Cluster}${var.App_Environment}${var.App_Acronym}01"
    host = aws_rds_cluster.rdscluster.endpoint
  }
}

#RDS
resource "aws_rds_cluster" "rdscluster" {
  cluster_identifier = "rdc-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"
  engine             = var.engine
  engine_version     = var.engine_version
  availability_zones = var.availability_zones
  database_name      = "db${var.App_Cluster}${var.App_Environment}${var.App_Acronym}01"
  master_username    = "${var.App_Acronym}_${var.App_Environment}_owner"
  master_password    = random_password.master-password.result
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.rds-parameter.name
  db_subnet_group_name            = aws_db_subnet_group.subnet_group.name
  vpc_security_group_ids          = [aws_security_group.security_group.id]
  deletion_protection             = var.deletion_protection
  final_snapshot_identifier       = "Finalsnapshot-rdc-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-01"
  skip_final_snapshot             = var.skip_final_snapshot
  preferred_backup_window         = var.backup_window
  backup_retention_period         = var.backup_retention_period
  copy_tags_to_snapshot           = var.copy_tags_to_snapshot
  storage_encrypted               = var.storage_encrypted
  kms_key_id                      = var.rds_kms_key
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  tags = merge(var.default_tags, map("Name","rdc-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"))
  apply_immediately = "true"
  lifecycle {
    ignore_changes = [
      "availability_zones",
    ]
  }
}

resource "aws_rds_cluster_instance" "rdsinstance" {
  count                   = var.rds_instance_count
  instance_class          = var.instance_type
  identifier = "rdi-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-${count.index+01}"
  cluster_identifier      = aws_rds_cluster.rdscluster.id
  ca_cert_identifier      = "rds-ca-2019"
  promotion_tier          = "1"
  db_parameter_group_name = aws_db_parameter_group.rds-parameter.name
  engine         = var.engine
  engine_version = var.engine_version
  auto_minor_version_upgrade = "false"
  preferred_maintenance_window    = var.maintenance-window
  tags           = merge(var.default_tags, map("Name", "rdi-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-${count.index+01}"))

}

resource "aws_db_subnet_group" "subnet_group" {
  name        = "rsn-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"
  description = "Subnet Group for RDS"
  subnet_ids  = var.db_subnet_Group_ids
  tags        = merge(var.default_tags, map("Name", "rsn-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"))
}

resource "aws_security_group" "security_group" {
  name        = "rsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"
  description = "Security group for RDS"
  vpc_id      = var.vpc_id
  tags        = merge(var.default_tags, map("Name", "rsg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"))

  #inbound form trusted zone enabled
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.trusted_cidr_iprange_az1, var.trusted_cidr_iprange_az2, var.trusted_cidr_iprange_az3]
    description = "Inbound rule open"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # tools server access provided to the DB
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.tools_server_ip]
    description = "tool server"
  }
}

resource "aws_rds_cluster_parameter_group" "rds-parameter" {
  name   = "rpg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"
  family = "aurora-mysql5.7"
  tags           = merge(var.default_tags, map("Name", "rpg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-01"))
}
resource "aws_db_parameter_group" "rds-parameter" {
  name   = "rpg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-02"
  family = "aurora-mysql5.7"
  tags           = merge(var.default_tags, map("Name", "rpg-${var.App_Cluster}-${var.App_Environment}-euwe01-${var.App_Acronym}-${var.service}-02"))
}
