#Terraform Version 0.12.20
#Terraform Provider Version 2.58

This Module will have two Secret Manger
1>resource "aws_secretsmanager_secret" "secrets1"
2>resource "aws_secretsmanager_secret" "secrets2"

Secret Manager 1 is for storing the  Master user name and Master password

Secret Manager 2 is for storing the  Application user name and Application user  password , There will be a user name
and random password generated for app user and that will be available in Secret manager 2. Once the Deployment is
completed Open the Second secret manager and create a user in RDS using those details . You can use workbench to connect
the RDS using master user name and password for crating the user . Please use the below query to create the user and
grant access

CREATE USER 'newuser'@'%' IDENTIFIED BY 'password'
GRANT ALL PRIVILEGES ON DBNAME. * TO 'newuser'@'%';

*All the details for app user will be available in second secret manager like DB name , user name , password etc


#######   PreRequisites   ############

1>Need to have the KMS Key for the RDS
2>Should have the details for Trusted subnet , Restricted subnet ,IP range etc
3>Get the Details of the Aurora engine Version after discussing with the Solution Designer
4>For Testing RDS Aurora , DB.t3.Medium is the best - You can select based on the project requirement
========================================================================================================================
App_Cluster              = ""  Must -  your cluster like es, cc02,parcels
========================================================================================================================
App_Environment          = "test" # allowed values are dv1/dv2/st1/st2/ac1/ac2/pr
========================================================================================================================
App_Acronym              = "test"
========================================================================================================================
service = "app"
This we used for identification  , any name you can choose after discussing with the project , else keep "app"
========================================================================================================================
authorized_aws_accounts  = "179337217599"
THis is required as it used in provider file
========================================================================================================================
profile                  = ""  your access profile details
========================================================================================================================
AWS_REGION               = "eu-west-1"
========================================================================================================================
Jump Server / Tools server IP - its always better to add this , you can connect the RDS from this machine using work bench
tools_server_ip          = "10.76.47.105/32"
================================================================
VPC ID is required for Security group creation
vpc_id                   = "vpc-0054433c3ac29b80b"
================================================================
We are placing the RDS always in restricted subnet , unless there is a Special requirement , need to have the subnet details
before the implementation
restricted_subnet_id_az1 = "subnet-09d405b12c61c0f23"
restricted_subnet_id_az2 = "subnet-02a25d6ab3fc3cfa0"
restricted_subnet_id_az3 = "subnet-03eedccc79cafb806"
========================================================================================================================
Trusted IP Range is required for the RDS security group, with this we can allow the EC2 instances to connect the
RDS in restricted subnet We can use the Security group as well instead of IP Ranges
trusted_cidr_iprange_az1 = "10.79.224.0/23"
trusted_cidr_iprange_az2 = "10.79.226.0/23"
trusted_cidr_iprange_az3 = "10.79.228.0/23"
========================================================================================================================
There are 12 Tags which is mandatory for any resource , Tags should have below informations

default_tags = {
  ApplicationName      = "MUST"
  ApplicationAcronym   = "MUST"
  ApplicationOwner     = ""MUST"@bpost.be"
  ApplicationSupport   = ""MUST"@bpost.be"
  ManagedBy            = "MUST"
  Environment          = "Development / Production"
  CloudServiceProvider = "AWS"
  DRLevel              = "MUST"   #You can discuss with the Solution designer and get the DR Level ,
  Based on the DR level you can select the Availability Zone
  Backup               = "True"
  DataProfile          = "Restricted/Internal "
  SubnetType           = "Private/Public"
  Availability         = "True"
}
========================================================================================================================
apply_immediately               = "true"  # If you add apply immediately in Terraform , it will take the action ,
 if restart is required for that change , the RDS will reboot .
========================================================================================================================
availability_zones              = ["eu-west-1a", "eu-west-1b", "eu-west-1c"] #
This is based on DR Level , Its always good to get a confirmation on this and start the project
========================================================================================================================
engine                          = "aurora-mysql"
Engine version- this wont change for RDS AURORA  util there is a note from AMAZON
========================================================================================================================
engine_version                  = "5.7.mysql_aurora.2.03.2"
Version needs to be confirmed according to your requirement
========================================================================================================================
db_port                         = "3306"
========================================================================================================================
rds_instance_count              = "1"
Based on your project requirement
========================================================================================================================
instance_type                   = "db.t3.medium"
Based on your project requirement
========================================================================================================================
skip_final_snapshot             = "true"
========================================================================================================================
backup_retention_period         = "7" #
 Based on the project requirement , The storage costs will increase according to  the days
========================================================================================================================
backup_window                   = "03:00-06:00"#
Based on the project requirement , its an every day activity , make sure that there is no
conflict with the maintenance window
========================================================================================================================
maintenance-window              = "Sat:00:00-Sat:02:00" # Should not be on any working days and sunday Night .
========================================================================================================================
copy_tags_to_snapshot           = "true"
========================================================================================================================
storage_encrypted               = "true"  # Should be True
========================================================================================================================
enabled_cloudwatch_logs_exports = ["error", "slowquery"]
========================================================================================================================
deletion_protection             = "false/ true "
 For Production and UAT the deletion protection should be True , if yu are doing many testing keep it as false
========================================================================================================================
db_subnet_Group_ids             = ["subnet-09d405b12c61c0f23", "subnet-02a25d6ab3fc3cfa0"]
If you have selected 3 availability Zone then mention three Subnet for the restricted subnet .If you need AZ 1a and 1c
then mention ony that
========================================================================================================================
rds_kms_key                     =  RDS KMS Key should be available before the Implementation of this script
secret_kms_arn                 = SECRET KMS Key should be available before the Implementation of this script
========================================================================================================================


TEST RESULT
module.RDSAurora.random_password.rdsappuser-password: Creating...
module.RDSAurora.random_password.master-password: Creating...
module.RDSAurora.random_password.master-password: Creation complete after 0s [id=none]
module.RDSAurora.random_password.rdsappuser-password: Creation complete after 0s [id=none]
module.RDSAurora.aws_secretsmanager_secret.secrets2: Creating...
module.RDSAurora.aws_rds_cluster_parameter_group.rds-parameter: Creating...
module.RDSAurora.aws_secretsmanager_secret.secrets1: Creating...
module.RDSAurora.aws_db_subnet_group.subnet_group: Creating...
module.RDSAurora.aws_db_parameter_group.rds-parameter: Creating...
module.RDSAurora.aws_security_group.security_group: Creating...
module.RDSAurora.aws_secretsmanager_secret.secrets2: Creation complete after 0s [id=arn:aws:secretsmanager:eu-west-1:179337217599:secret:asm-es-test-euwe01-test-app-rdsappuser-01-QZEsmL]
module.RDSAurora.aws_secretsmanager_secret.secrets1: Creation complete after 0s [id=arn:aws:secretsmanager:eu-west-1:179337217599:secret:asm-es-test-euwe01-test-app-rdsmaster-01-2XzITK]
module.RDSAurora.aws_secretsmanager_secret_version.secret-version1: Creating...
module.RDSAurora.aws_secretsmanager_secret_version.secret-version1: Creation complete after 0s [id=arn:aws:secretsmanager:eu-west-1:179337217599:secret:asm-es-test-euwe01-test-app-rdsmaster-01-2XzITK|4D112E6E-A3A3
-48CC-BB1B-D5830D7068F5]
module.RDSAurora.aws_db_subnet_group.subnet_group: Creation complete after 0s [id=rsn-es-test-euwe01-test-app-01]
module.RDSAurora.aws_db_parameter_group.rds-parameter: Creation complete after 0s [id=rpg-es-test-euwe01-test-app-02]
module.RDSAurora.aws_rds_cluster_parameter_group.rds-parameter: Creation complete after 0s [id=rpg-es-test-euwe01-test-app-01]
module.RDSAurora.aws_security_group.security_group: Creation complete after 1s [id=sg-0dd969bef5889e43c]
module.RDSAurora.aws_rds_cluster.rdscluster: Creating...
module.RDSAurora.aws_rds_cluster.rdscluster: Still creating... [10s elapsed]
module.RDSAurora.aws_rds_cluster.rdscluster: Still creating... [20s elapsed]
module.RDSAurora.aws_rds_cluster.rdscluster: Still creating... [30s elapsed]
module.RDSAurora.aws_rds_cluster.rdscluster: Still creating... [40s elapsed]
module.RDSAurora.aws_rds_cluster.rdscluster: Still creating... [50s elapsed]
module.RDSAurora.aws_rds_cluster.rdscluster: Creation complete after 51s [id=rdc-es-test-euwe01-test-app-01]
module.RDSAurora.aws_secretsmanager_secret_version.secret-version2: Creating...
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Creating...
module.RDSAurora.aws_secretsmanager_secret_version.secret-version2: Creation complete after 0s [id=arn:aws:secretsmanager:eu-west-1:179337217599:secret:asm-es-test-euwe01-test-app-rdsappuser-01-QZEsmL|97D018E5-903
3-4490-A680-DC60FD0DFB4F]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [10s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [20s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [30s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [40s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [50s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [1m0s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [1m10s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [1m20s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [1m30s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [1m40s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [1m50s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [2m0s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [2m10s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [2m20s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [2m30s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [2m40s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [2m50s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [3m0s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [3m10s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [3m20s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [3m30s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [3m40s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [3m50s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [4m0s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [4m10s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [4m20s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [4m30s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [4m40s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [4m50s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [5m0s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [5m10s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [5m20s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Still creating... [5m30s elapsed]
module.RDSAurora.aws_rds_cluster_instance.rdsinstance[0]: Creation complete after 5m35s [id=rdi-es-test-euwe01-test-app-1]

Apply complete! Resources: 12 added, 0 changed, 0 destroyed.
========================================================================================================================
Tested the Destroy as wll
module.RDSAurora.aws_rds_cluster_parameter_group.rds-parameter: Destruction complete after 0s
module.RDSAurora.aws_security_group.security_group: Destruction complete after 1s
Destroy complete! Resources: 12 destroyed.

C:\Users\u542497.POST\Desktop\RDS Aurora-Module\Environment\DV2>
========================================================================================================================
Secret manager 1 result
Secret Key                  Secret Value
masterpassword	         password
masterusername	         test_test_owner
========================================================================================================================
Secret manager 2 ( app user ) result
  "dbname": "dbtest01",
  "host": "rdc-es-dv2-euwe01-cbo-app-01.cluster-cuvkpgowgxbs.eu-west-1.rds.amazonaws.com",
  "password": "password",
  "port": "3306",
  "username": "test_dv2_user"
* with the above user name and password , you need to create an RDS user
========================================================================================================================

