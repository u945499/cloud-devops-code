resource "aws_codebuild_project" "dv1deploy" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for build Broker Service docker image"
       name           = "cb-parcels-np-dv1-euwe01-brokerservice-deploy-01"
       queued_timeout = 480
       service_role   = "arn:aws:iam::086243371668:role/service-role/codebuild-CODE-BUILD-HN-SFM-NP-01-service-role"
       lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
                        }
       tags      = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "Na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "cb-parcels-np-dv1-euwe01-brokerservice-deploy-01"
        }

       artifacts {
           encryption_disabled    = false
           override_artifact_name = false
           type                   = "NO_ARTIFACTS"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
           image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = true
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "ECS_TASK_NAME"
               type  = "PLAINTEXT"
               value = "ecs-task-defination-parcels-dv1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "ECS_CLUSTER_NAME"
               type  = "PLAINTEXT"
               value = "ecf-parcel-np-euwe01-sfm-01"
            }
           environment_variable {
               name  = "ECS_SERVICE_NAME"
               type  = "PLAINTEXT"
               value = "ecs-service-parcels-dv1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "AWS_DEFAULT_REGION"
               type  = "PLAINTEXT"
               value = "eu-west-1"
            }
           environment_variable {
               name  = "ECR_REPO"
               type  = "PLAINTEXT"
               value = "ecr-hn-np-euwe01-brokerservice-01"
            }
        }

       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/dv1/build-spec-dv1-deploy.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://u534684@bitbucket.org/bpost_deliveryparcelsint/hn-event-broker.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }


    resource "aws_codebuild_project" "stpromote" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for build borker service docker image"
       name           = "cb-parcels-np-st1-euwe01-brokerservice-01-promote"
       queued_timeout = 480
       service_role   = "arn:aws:iam::086243371668:role/service-role/codebuild-CODE-BUILD-HN-SFM-NP-01-service-role"
       tags           = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "cb-parcels-np-st1-euwe01-brokerservice-01-promote"
        }
        lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
       }
       artifacts {
           encryption_disabled    = false
           override_artifact_name = false
           type                   = "NO_ARTIFACTS"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
           image_pull_credentials_type = "CODEBUILD"
           privileged_mode             =  true
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "ECS_TASK_NAME"
               type  = "PLAINTEXT"
               value = "ecs-task-defination-parcels-st1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "ECS_CLUSTER_NAME"
               type  = "PLAINTEXT"
               value = "ecf-parcel-np-euwe01-sfm-01"
            }
           environment_variable {
               name  = "ECS_SERVICE_NAME"
               type  = "PLAINTEXT"
               value = "ecs-service-parcels-st1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "AWS_DEFAULT_REGION"
               type  = "PLAINTEXT"
               value = "eu-west-1"
            }
           environment_variable {
               name  = "ECR_REPO"
               type  = "PLAINTEXT"
               value = "ecr-hn-np-euwe01-brokerservice-01"
            }
        }

       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/st1/build-spec-st1-promote.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://u534684@bitbucket.org/bpost_deliveryparcelsint/hn-event-broker.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }

    resource "aws_codebuild_project" "st1deploy" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for build borker service docker image"
       name           = "cb-parcels-np-st1-euwe01-brokerservice-01-deploy"
       queued_timeout = 480
       service_role   = "arn:aws:iam::086243371668:role/service-role/codebuild-CODE-BUILD-HN-SFM-NP-01-service-role"
       tags           = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "cb-parcels-np-st1-euwe01-brokerservice-01-deploy"
        }
      lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
       }
       artifacts {
           encryption_disabled    = false
           override_artifact_name = false
           type                   = "NO_ARTIFACTS"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
            image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = true
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "ECS_TASK_NAME"
               type  = "PLAINTEXT"
               value = "ecs-task-defination-parcels-st1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "ECS_CLUSTER_NAME"
               type  = "PLAINTEXT"
               value = "ecf-parcel-np-euwe01-sfm-01"
            }
           environment_variable {
               name  = "ECS_SERVICE_NAME"
               type  = "PLAINTEXT"
               value = "ecs-service-parcels-st1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "AWS_DEFAULT_REGION"
               type  = "PLAINTEXT"
               value = "eu-west-1"
            }
           environment_variable {
               name  = "ECR_REPO"
               type  = "PLAINTEXT"
               value = "ecr-hn-np-euwe01-brokerservice-01"
            }
        }

       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/st1/build-spec-st1-deploy.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://u534684@bitbucket.org/bpost_deliveryparcelsint/hn-event-broker.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }


    resource "aws_codebuild_project" "ac1promote" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for build borker service  docker image"
       name           = "cb-parcels-np-ac1-euwe01-brokerservice-01-promote"
       queued_timeout = 480
       service_role   = "arn:aws:iam::086243371668:role/service-role/codebuild-CODE-BUILD-HN-SFM-NP-01-service-role"
       tags           = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "cb-parcels-np-ac1-euwe01-brokerservice-01-promote"
        }

       artifacts {
           encryption_disabled    = false
           override_artifact_name = false
           type                   = "NO_ARTIFACTS"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }
        lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
       }
       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
            image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = true
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "ECS_TASK_NAME"
               type  = "PLAINTEXT"
               value = "ecs-task-defination-parcels-ac1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "ECS_CLUSTER_NAME"
               type  = "PLAINTEXT"
               value = "ecf-parcel-np-euwe01-sfm-01"
            }
           environment_variable {
               name  = "ECS_SERVICE_NAME"
               type  = "PLAINTEXT"
               value = "ecs-service-parcels-ac1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "AWS_DEFAULT_REGION"
               type  = "PLAINTEXT"
               value = "eu-west-1"
            }
           environment_variable {
               name  = "ECR_REPO"
               type  = "PLAINTEXT"
               value = "ecr-hn-np-euwe01-brokerservice-01"
            }
        }

       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/ac1/build-spec-ac1-promote.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://u534684@bitbucket.org/bpost_deliveryparcelsint/hn-event-broker.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }

    resource "aws_codebuild_project" "ac1deploy" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for build Broker Service docker image"
       name           = "cb-parcels-np-ac1-euwe01-brokerservice-01-deploy"
       queued_timeout = 480
       lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
       service_role   = "arn:aws:iam::086243371668:role/service-role/codebuild-CODE-BUILD-HN-SFM-NP-01-service-role"
       tags           = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "cb-parcels-np-ac1-euwe01-brokerservice-01-deploy"
        }

       artifacts {
           encryption_disabled    = false
           override_artifact_name = false
           type                   = "NO_ARTIFACTS"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
            image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = true
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "ECS_TASK_NAME"
               type  = "PLAINTEXT"
               value = "ecs-task-defination-parcels-ac1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "ECS_CLUSTER_NAME"
               type  = "PLAINTEXT"
               value = "ecf-parcel-np-euwe01-sfm-01"
            }
           environment_variable {
               name  = "ECS_SERVICE_NAME"
               type  = "PLAINTEXT"
               value = "ecs-service-parcels-ac1-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "AWS_DEFAULT_REGION"
               type  = "PLAINTEXT"
               value = "eu-west-1"
            }
           environment_variable {
               name  = "ECR_REPO"
               type  = "PLAINTEXT"
               value = "ecr-hn-np-euwe01-brokerservice-01"
            }
        }

       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/ac1/build-spec-ac1-deploy.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://u534684@bitbucket.org/bpost_deliveryparcelsint/hn-event-broker.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }



  resource "aws_ecs_task_definition" "ecs_task_definitionbrokerservicest1"  {
  family = "ecs-task-defination-parcels-st1-euwe01-brokerservice-01"
  container_definitions = "${file("${path.module}/brokerservicecontainer.json")}"
  task_role_arn= "arn:aws:iam::086243371668:role/ecsTaskExecutionRoleSFM"
  execution_role_arn="arn:aws:iam::086243371668:role/ecsTaskExecutionRole"
  network_mode="awsvpc"
  cpu="256"
  memory="512"
    lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
  requires_compatibilities=["FARGATE"]
}

 resource "aws_ecs_task_definition" "ecs_task_definitionbrokerserviceac1"  {
  family = "ecs-task-defination-parcels-ac1-euwe01-brokerservice-01"
  container_definitions = "${file("${path.module}/brokerservicecontainer.json")}"
  task_role_arn= "arn:aws:iam::086243371668:role/ecsTaskExecutionRoleSFM"
  execution_role_arn="arn:aws:iam::086243371668:role/ecsTaskExecutionRole"
  network_mode="awsvpc"
    lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
  cpu="256"
  memory="512"
  requires_compatibilities=["FARGATE"]
}


resource "aws_ecs_service" "brokerservicest1" {
  name            = "ecs-service-parcels-st1-euwe01-brokerservice-01"
  cluster         = "ecf-parcel-np-euwe01-sfm-01"
  task_definition = "ecs-task-defination-parcels-st1-euwe01-brokerservice-01"
  desired_count   = 1
  launch_type = "FARGATE"
  deployment_maximum_percent = 100
  deployment_minimum_healthy_percent = 80
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
    }
 network_configuration {
  subnets = ["subnet-0b43ff380a425d9aa","subnet-05a33b81b6ea84ff4"]
  security_groups = ["sg-026b1251e705a5a91"]
        }

}


resource "aws_sqs_queue" "brokerserviceac1" {
  name                      = "sqs-parcels-np-ac1-euwe01-brokerservice-01"
  delay_seconds             = 10
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  kms_master_key_id                 = "alias/kms-parcels-hn-np-euwe01-sqs-01"
  kms_data_key_reuse_period_seconds = 300
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
    }
  tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sqs-parcels-np-ac1-euwe01-brokerservice-01"
  }
}


resource "aws_sqs_queue" "brokerservicedlqac1" {
  name                      = "sqs-parcels-np-ac1-euwe01-brokerservice-dlq-01"
  delay_seconds             = 10
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
    }
 tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sqs-parcels-np-ac1-euwe01-brokerservice-dlq-01"
  }
  }


resource "aws_sqs_queue" "brokerservicest1" {
  name                      = "sqs-parcels-np-st1-euwe01-brokerservice-01"
  delay_seconds             = 10
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
    }
  kms_master_key_id                 = "alias/kms-parcels-hn-np-euwe01-sqs-01"
  kms_data_key_reuse_period_seconds = 300
  tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sqs-parcels-np-st1-euwe01-brokerservice-01"
  }
}


resource "aws_sqs_queue" "brokerservicedlqst1" {
  name                      = "sqs-parcels-np-st1-euwe01-brokerservice-dlq-01"
  delay_seconds             = 10
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
 tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sqs-parcels-np-st1-euwe01-brokerservice-dlq-01"
  }
  }



resource "aws_sns_topic" "brokersnsdv1" {
  name = "sns-hn-np-dv1-euwe01-push-brk"
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
  tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-hn-np-dv1-euwe01-push-brk"
  }
}

resource "aws_sns_topic" "brokersnsst1" {
  name = "sns-hn-np-st1-euwe01-push-brk"
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
  tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-hn-np-st1-euwe01-push-brk"
  }
}

resource "aws_sns_topic" "brokersnsac1" {
  name = "sns-hn-np-ac1-euwe01-push-brk"
  lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
        }
  kms_master_key_id= "arn:aws:kms:eu-west-1:086243371668:key/1b09f84c-a0ac-49d5-97b3-1d647d63318a"
  tags = {
          "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Non Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-hn-np-ac1-euwe01-push-brk"
  }
}


resource "aws_codebuild_project" "prpromote" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for build Broker Service docker image"
       name           = "cb-parcels-pr-euwe01-brokerservice-promote"
       queued_timeout = 480
       service_role   = "arn:aws:iam::086243371668:role/service-role/codebuild-CODE-BUILD-HN-SFM-NP-01-service-role"
       lifecycle {
        prevent_destroy = true
        ignore_changes = "*"
                        }
       tags      = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "cb-parcels-pr-euwe01-brokerservice-promote"
        }

       artifacts {
           encryption_disabled    = false
           override_artifact_name = false
           type                   = "NO_ARTIFACTS"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
           image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = true
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "ECS_TASK_NAME"
               type  = "PLAINTEXT"
               value = "ecs-task-defination-parcels-pr-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "ECS_CLUSTER_NAME"
               type  = "PLAINTEXT"
               value = "ecf-parcel-pr-euwe01-sfm-01"
            }
           environment_variable {
               name  = "ECS_SERVICE_NAME"
               type  = "PLAINTEXT"
               value = "ecs-service-parcels-pr-euwe01-brokerservice-01"
            }
           environment_variable {
               name  = "AWS_DEFAULT_REGION"
               type  = "PLAINTEXT"
               value = "eu-west-1"
            }
           environment_variable {
               name  = "ECR_REPO"
               type  = "PLAINTEXT"
               value = "ecr-hn-pr-euwe01-publisher-01"
            }
        }

       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/pr/build-spec-pr-deploy.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://u534684@bitbucket.org/bpost_deliveryparcelsint/hn-event-broker.git"
           report_build_status = false
           type                = "BITBUCKET"
        }
        
    }