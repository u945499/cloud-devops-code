variable "APP_environment_type" {
  description = "The Environment type to be deployed"
  type        = string
  default     = "ds1"
}
variable "APP_app_acronym_codename" {
  description = "Application Acronym Name"
  type        = string
  default     = "myb"
}
variable "APP_appcluster_name" {
  description = "Application Cluster Name"
  type        = string
  default     = "cc"
}

variable "APP_ecs_far_autoscale_target" {
  description = "Provide AutoScaling Target Value which will trigger ECS AutoScaling"
  type        = string
  default     = "80"
}
variable "APP_ecs_far_container_port" {
  description = "Provide the port number the MMS application inside the docker container will be binding"
  type        = number
  default     = 3000
}
variable "APP_ecs_far_container_cpu" {
  description = "Provide the CPU details for container. 1024 is 1 CPU"
  type        = number
  default     = 1024
}
variable "APP_ecs_far_container_mem" {
  description = "Provide memory details in megabytes for container"
  type        = number
  default     = 3072
}
variable "APP_ecs_far_min_contain" {
  description = "Provide minimum number of tasks for Service Auto Scaling"
  type        = number
  default     = 1
}
variable "APP_ecs_far_max_contain" {
  description = "Provide Maximum number of tasks for Service Auto Scaling"
  type        = string
  default     = "2"
}
variable "MMSECRAllowPushPullUserID01" {
  description = "Provide IAM User ID to permit ECR Push and Pull Images"
  type        = string
  default     ="u541732"
}
variable "APP_ecs_far_desired_count" {
  description = "Provide Desired number of tasks for service"
  type        = string
  default     = "1"
}
variable "APP_ecs_far_vpc" {
  description = "Select VPC ID for ECR FAR SIT Environment"
  type        = string
  default     = "vpc-0f791e26d134b60ae"
}
variable "APP_tag_app_owner" {
  description = "IT Application Owner e-mail address"
  type        = string
  default     = "Jatin.Rai.ext@bpost.be"
}
variable "APP_tag_app_support" {
  description = "IT Application Owner or Distribution List of  Application Team"
  type        = string
  default     = "Jatin.Rai.ext@bpost.be"
}
variable "APP_tag_dataProfile" {
  description = "Data Profile"
  type        = string
  default     = "confidental"
}

variable "APP_tag_app_name" {
  description = "Application Full Name"
  type        = string
  default     = "My Bpost App"
}
variable "APP_tag_managedby" {
  description = "Company Name who is responsible for CloudOps"
  type        = string
  default     = "TCS"
}
variable "APP_tag_env" {
  description = "Environment in which Resources are hosted"
  type        = string
  default     = "Non Production"
}
variable "APP_tag_cloudserviceprovider" {
  description = "cloud service provider"
  type        = string
  default     = "AWS"
}
variable "APP_tag_DRlevel" {
  description = "Disaster Recovery Level of the Application"
  type        = string
  default     = "NA"
}
variable "APP_tag_backup" {
  description = "Define backup enabled"
  type        = string
  default     = "False"
}

variable "APP_ecs_far_container_sg" {
  description = "Define backup enabled"
  type        = string
  default     = "esg-parcels-ds1-euwe01-APP-esc-01"
}

variable "s3_bucket_name" {
  description = "Bucket for Lambda Code"
  default = "s3b-cc-np-euwe01-mpbpostapp-lambda-code-01"

}

variable "build_version" {
  default = "LambdaCode"
}

variable "build_file_name" {
  default = "rdscode.zip"
}

variable "handler_name" {
  default = "index.handler"
}

variable "runtime" {
  default = "nodejs10.x"
}


variable "region" {
  #default = "eu-central-1" # Frankfurt
  #default = "us-east-2" # Ohio
  #default = "us-west-2" #Oregon
  #default = "ap-southeast-2"
  #default = "us-east-2"
  #default = "eu-west-1"
  default = "eu-west-1" # Virginia
  description = "the region where you want deploy the solution"
}

variable "prefix" {
    default = "myb"
    description = "The prefix used to build the elements"
}

variable "profile" {
  default = "new"
}

variable "filename" {
  default = "lambda_function.zip"
}


variable "stage" {
  description = "The stage name for the API deployment (production/staging/etc..)"
  default = "ds1"
}

variable "ignore_changes" {
  description = "The list of binary media types supported by the RestApi"
  type        = "list"
  default     = ["*"]
}

variable "binary_type" {
  description = "The list of binary media types supported by the RestApi"
  type        = "list"
  default     = ["*/*"]
}

variable "minimum_compression_size" {
  description = "The list of binary media types supported by the RestApi"
  default     = 0
}

variable "method" {
  description = "The HTTP method"
  default     = "POST"
}

variable "lambda_arn" {
  description = "The lambda arn"
}

variable "lambda_arn_invoke" {
  description = "The lambda invoke uri"
}

variable "getMethod" {
  default = "GET"
}

variable "acl" {
  type        = string
  default     = "private"
  description = "The canned ACL to apply. We recommend `private` to avoid exposing sensitive information"
}


variable "force_destroy" {
  type        = bool
  default     = false
  description = "A boolean string that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable"
}

variable "versioning_enabled" {
  type        = bool
  default     = true
  description = "A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket"
}

variable "kms_master_key_arn" {
  type        = string
  default     = "arn:aws:kms:eu-west-1:067806847764:key/373b3e5b-7f50-44d6-995e-b60536c60d35"
  description = "The AWS KMS master key ARN used for the `SSE-KMS` encryption. This can only be used when you set the value of `sse_algorithm` as `aws:kms`. The default aws/s3 AWS KMS master key is used if this element is absent while the `sse_algorithm` is `aws:kms`"
}

variable "shard_count" {
  default = 2
}

variable "retention_period" {
  default = 24

}

