output "hn_notif_rds_secret_output" {
  value = jsondecode(aws_secretsmanager_secret_version.hn_notif_rds_secret.secret_string)
}