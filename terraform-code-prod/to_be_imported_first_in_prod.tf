/* 

!!!!!! SECTION TO BE IMPORTED FIRST INTO STATE FILE BEFORE APPLYING !!!!!

*/

#### To be able to manage it via terraform, those resources need to be imported first and then configuration can be done and then compared with the configuration of AWS with the terraform plan


resource "aws_sqs_queue" "hn_brokerservice_queue" {
    name                      = "sqs-parcels-pr-euwe01-brokerservice-01"
    delay_seconds             = 10
    max_message_size          = 262144
    message_retention_seconds = 1209600
    receive_wait_time_seconds = 10
    visibility_timeout_seconds = 240
    kms_master_key_id = "arn:aws:kms:eu-west-1:119394011513:key/045f155d-face-409d-9393-783e82f4791c"
    kms_data_key_reuse_period_seconds = 3600
    redrive_policy = jsonencode({
        deadLetterTargetArn = aws_sqs_queue.hn_brokerservice_dlq_queue.arn
        #maxReceiveCount     = 2
        maxReceiveCount     = 3
    })
    tags      = {
            "ApplicationAcronym"   = "HN"
            "ApplicationName"      = "Hybrid Network"
            "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
            "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
            "Backup"               = "False"
            "CloudServiceProvider" = "AWS"
            "DRLevel"              = "2"
            "DataProfile"          = "Confidential"
            "Environment"          = "Production"
            "ManagedBy"            = "TCS"
            "Name"                 = "sqs-parcels-pr-euwe01-brokerservice-01"
            }
}

data "aws_iam_policy_document" "hn_brokerservice_queue-policy" {
    policy_id = join("/", [aws_sqs_queue.hn_brokerservice_queue.arn, "SQSDefaultPolicy"])
    statement {
        actions = ["SQS:*"]
        effect = "Allow"
        resources=[aws_sqs_queue.hn_brokerservice_queue.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = ["arn:aws:sns:eu-west-1:119394011513:sns-hn-pr-euwe01-push-brk"]
        }
        sid = "Sid1581047582297"
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents.arn]
        }
    }
    statement {
        actions = ["SQS:SendMessage"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers =["*"]
        }
        resources=[aws_sqs_queue.hn_brokerservice_queue.arn]
        condition {
          test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_sns_topic.hn_common-events_sns.arn]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_brokerservice_queue_policy" {
  queue_url = aws_sqs_queue.hn_brokerservice_queue.id
  policy = data.aws_iam_policy_document.hn_brokerservice_queue-policy.json
}

resource "aws_sqs_queue" "hn_brokerservice_dlq_queue" {
    name                      = "sqs-parcels-pr-euwe01-brokerservice-dlq-01"
    max_message_size          = 262144
    #max_message_size          = 2048
    delay_seconds             = 10
    #message_retention_seconds = 86400
    message_retention_seconds = 1209600 
    receive_wait_time_seconds = 10
    tags      = {
            "ApplicationAcronym"   = "HN"
            "ApplicationName"      = "Hybrid Network"
            "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
            "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
            "Backup"               = "False"
            "CloudServiceProvider" = "AWS"
            "DRLevel"              = "2"
            "DataProfile"          = "Confidential"
            "Environment"          = "Production"
            "ManagedBy"            = "TCS"
            "Name"                 = "sqs-parcels-pr-euwe01-brokerservice-dlq-01"
            }
}

resource "aws_sns_topic" "hn_common-events_sns" {
    name = "sns-parcels-pr-euwe01-hn-common-events-01"
    lifecycle {
        create_before_destroy = true
    }
    kms_master_key_id = "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
    tags = {
            "ApplicationAcronym"   = "HN"
            "ApplicationName"      = "Hybrid Network"
            "ApplicationOwner"     = "pieter.bisschops@bpost.be"
            "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
            "Backup"               = "False"
            "CloudServiceProvider" = "AWS"
            "DRLevel"              = "2"
            "DataProfile"          = "Confidential"
            "Environment"          = "Production"
            "ManagedBy"            = "TCS"
            "Name"                 = "sns-parcels-pr-euwe01-hn-common-events-01"
    }
}

data "aws_iam_policy_document" "hn_common-events_sns-policy-doc" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive", 
    ]
    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"
      values = [
        var.account-id,
      ]
    }
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    resources = [
      aws_sns_topic.hn_common-events_sns.arn,
    ]

    sid = "__default_statement_ID"
  }
}

resource "aws_sns_topic_policy" "hn_common-events_sns-policy" {
    arn = aws_sns_topic.hn_common-events_sns.arn
    policy = data.aws_iam_policy_document.hn_common-events_sns-policy-doc.json
}


