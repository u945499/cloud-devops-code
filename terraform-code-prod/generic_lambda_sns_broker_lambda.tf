resource "aws_lambda_function" "hn_lmb_filter_sns-msg-broker-commonevents" {
  s3_bucket = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key = "lmb-parcels-pr-euwe01-hn-sns-msg-broker-commonevents-01/app.zip"
  function_name = "lmb-parcels-pr-euwe01-hn-sns-msg-broker-commonevents-01"
  role          = aws_iam_role.hn_lmb_filter_sns-msg-broker-commonevents-role.arn
  handler       = "CommonEventsSubscriptionLambda.handler"
  runtime = "nodejs12.x"
  timeout = 60

  environment {
    variables = {
	      BROKER_QUEUE_URL = aws_sqs_queue.hn_brokerservice_queue.id
    }
  }
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "lmb-parcels-pr-euwe01-hn-sns-msg-broker-commonevents-01"
        }
  }

  resource "aws_lambda_permission" "allow_sns_invoke" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.hn_common-events_sns.arn
}

data "aws_iam_policy_document" "hn_lmb_filter_sns-msg-broker-commonevents-policydoc" {
    statement {
        actions = [
              "logs:CreateLogStream",
              "logs:PutLogEvents", 
              "logs:CreateLogGroup"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "kms:Decrypt",
              "kms:GenerateDataKey"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
                "sqs:SendMessage",
                "sqs:GetQueueAttributes"            
        ]
        effect = "Allow"
        resources=["*"]
    }
  }

data "aws_iam_policy_document" "hn_lmb_filter_sns-msg-broker-commonevents_assumerole-policydoc" {
   statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
   }
}

resource "aws_iam_policy" "hn_lmb_filter_sns-msg-broker-commonevents-policy" {
    name = "iap-parcels-pr-euwe01-hn-lmb-sns-msg-broker-commonevents"  
    policy = data.aws_iam_policy_document.hn_lmb_filter_sns-msg-broker-commonevents-policydoc.json
}

resource "aws_iam_role_policy_attachment" "hn_lmb_filter_sns-msg-broker-commonevents-policy_custom" {
  role       = aws_iam_role.hn_lmb_filter_sns-msg-broker-commonevents-role.name
  policy_arn = aws_iam_policy.hn_lmb_filter_sns-msg-broker-commonevents-policy.arn
}

resource "aws_iam_role" "hn_lmb_filter_sns-msg-broker-commonevents-role" {
  name = "iar-parcels-pr-euwe01-hn-lmb-sns-msg-broker-commonevents"
  path = "/"
  assume_role_policy = data.aws_iam_policy_document.hn_lmb_filter_sns-msg-broker-commonevents_assumerole-policydoc.json
}

/* resource "aws_sns_topic_subscription" "hn_lmb_filter_sns-msg-broker-commonevents-subscription" {
  topic_arn = aws_sns_topic.hn_common-events_sns.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lmb_filter_sns-msg-broker-commonevents.arn
  filter_policy = "{ \"Origin\": [\"NeRO\"] }"
}
 */

  /* resource "aws_sns_topic_subscription" "hn_filter-bis_msg-broker-commonevents-subscription" {
  topic_arn = aws_sns_topic.hn_common-events_sns.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.hn_brokerservice_queue.arn
  filter_policy = "{\n  \"Origin\": [\n    \"bpost\"\n  ],\n  \"ProductCategory\":[\"REGISTERED_LETTER\"],\n  \"TransportCategory\":[\"INBOUND\"]\n}"
}
 */

