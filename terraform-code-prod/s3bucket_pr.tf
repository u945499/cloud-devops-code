/*
Buckets for June Release for HN Notifications
3 buckets 
*/
############# June Release Buckets #################

resource "aws_s3_bucket" "dchpayloadmessages" {
  bucket = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  acl    = "private"
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"log-dch-payloadmessages-01"])))
    versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
		}
    }
}
logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  }
}	

resource "aws_s3_bucket" "incomingnotification" {
  bucket = join("-",[local.s3bName,"logincomingnotif-messages-01"])
  acl    = "private"
  tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"logincomingnotif-messages-01"])))
     versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
		}
    }
	}
      
logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"log-dch-payloadmessages-01"])
  }
}

resource "aws_s3_bucket_object" "hn_s3key_notification_dch_retry" {
  key        = join("/",[join("-",[local.lmbName,"notification-dch-retry-01"]),"app.zip"])
  bucket     = aws_s3_bucket.hn-general-lambda-repo.id
  source     = "./dch-retry/app.zip"
  kms_key_id = var.kmsMasterKeyID
}

resource "aws_s3_bucket_object" "hn_s3key_notification_dch_connector" {
  key        =  join("/",[join("-",[local.lmbName,"notification-dch-connector-01"]),"app.zip"])
  bucket     = aws_s3_bucket.hn-general-lambda-repo.id
 source     = "./dch-connector/app.zip"
  kms_key_id = var.kmsMasterKeyID
}

resource "aws_s3_bucket_object" "hn-s3key_notification-publisher" {
  key        =  join("/",[join("-",[local.lmbName,"notification-publisher-01"]),"app.zip"])
  bucket     = aws_s3_bucket.hn-general-lambda-repo.id
 source     = "./dch-publisher/app.zip"
  kms_key_id = var.kmsMasterKeyID
}


######################### MISSING ==> s3b-hn-parcels-dv1-euwe01-log-incomingnotification-messages-01 #########################

###############################July Release Buckets ############################

/* resource "aws_s3_bucket_object" "hn-notification-feedback_s3key" {
  key        =  join("-",[local.lmbName,"notification-feedback-01"])
  bucket     = aws_s3_bucket.hn-general-lambda-repo.id
  source     = "../app.zip"
  kms_key_id = var.kmsMasterKeyID
} */
# Not part of June Flex
/* resource "aws_s3_bucket" "hn_fdbck_msg_bucket" {
  bucket = join("-",[local.s3bName,"notificationfeedbackmessage-01"])
  acl    = "private"
  tags   =  merge(local.common_tags,map("Name",join("-",[local.s3bName,"notificationfeedbackmessage-01"])))
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.kmsMasterKeyID
        sse_algorithm     = var.kms
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = join("-",[local.s3bName,"notificationfeedbackmessage-01"])
  }
} */

# Not part of June Flex
/* resource "aws_s3_bucket" "hn_fdbck_evnt_bucket" {
    bucket = join("-",[local.s3bName,"notificationfeedbackevent-01"])
    acl    = "private"
    tags = merge(local.common_tags,map("Name",join("-",[local.s3bName,"notificationfeedbackevent-01"])))
    
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default {
                kms_master_key_id = var.kmsMasterKeyID
                sse_algorithm     = var.kms
            }
        }
    }
    logging {
        target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
        target_prefix = join("-",[local.s3bName,"notificationfeedbackevent-01"])
  } 
}
 */