variable "account-id" {
  type = number
  default ="119394011513"
} 
variable "region_var" {
  type = string
  default ="euwe01"
} 
variable "cluster_var" {
  type = string
  default = "parcels"
}
variable "envContext_var" {
  type = string
  default = "pr"
}
variable "env_var" {
  type = string
  default = "pr"
}
variable "envContextAndEnv_var" {
  type = string
  default = "pr"
}

variable "availability_zones" {
 description="for the availibility zones"
 type= list(string)
 default=["eu-west-1a", "eu-west-1b"]
} 
###########KMS Variables###############
variable "kms" {
  type=string
  default= "aws:kms"
}

variable "kmsMasterKeyID" {
  type=string
    description="this variable is used to associate KMS key to s3 bucket"
    default="arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
}

#################### NETWORKING VARIABLES #################
variable "vpc_id"{
type=list(string)
default=["vpc-06b80061126a75604"]
}

variable "cidr_blocks"{
type=list(string)
default=["10.78.136.0/23", "10.78.134.0/23", "10.76.47.105/32", "10.71.120.62/32","10.71.112.0/23","10.71.114.0/23"]
}

variable "db_subnet_ids"{
type=list(string)
  default=["subnet-0151c041799e7d006", "subnet-08a132fb588a737ff"]
}

#############################LAMBDA VARIABLES ####################
variable "lambdaExecutionRole" {
  type=string
  default="arn:aws:iam::119394011513:role/iar-LambdaExecutionRole"
   
  }
##################### RDS DB Variables #######################
variable "dbName" {
  type=map
  default= {
    pr: "pr_notification"
   
  }
}

variable "dbMasterSecret" {
  type=map
  default= {
    pr: "scr-parcels-pr-euwe01-hn-notification-rds-rw-user-details-02"
   }
}

variable "dbReplicaSet" {
  type=map
  default= {
    pr: "scr-parcels-pr-euwe01-hn-notification-rds-ro-user-details-02"
    
  }
}

variable "rdsdbsecretStringforConnection"{}
variable "rdsdbsecretStringforReadWriteUser"{}
variable "rdsdbsecretStringforReadOnlyUser"{}
variable "niaandlrsintegrationSecretstring" {}



variable "rdsengine" {
  description="for rds aurora engine"
  type=string
  default="aurora"
}

variable "rdsengineMode" {
  description="for rds aurora engine"
  type=string
  default="serverless"
}

variable "engine_mode" {
  description="for rds aurora engine"
  type=string
  default="serverless"
}
variable "engine_version" {
  description="for rds aurora engine"
  type=string
  default="5.6.10a"
}

variable "db_backup_retention_period"{
  type=string
  description="database retention period for prod"
  default="1"

}

variable "db_backup_window"{
  type=string
  description="database backup window period for prod"
  default="04:53-05:23"

}

variable "copytagstosnapshot"{
  type=string
  description="copy tags to snapshots"
  default=true
}

variable "skipfinalsnapshot"{
  type=string
  description="skip snapshots"
  default=false
}
variable "maintenance_window"{
  type=string
  description="maintenance windows for DB"
  default="sat:00:01-sat:00:31"
}

variable "rdsport" {
  type=list(number)
  default=[3306]
}

variable "deletion_protection"{
  type= string
  default=true
}

variable "dbrdsfamily"{
  type=string
  default="aurora5.6"
}

