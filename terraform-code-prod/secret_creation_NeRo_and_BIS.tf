variable "tmm-nero-user-detail_var" {
  default = {
    endpoint = "https://webservices.bpost.be/track/retail/event"
    protocol = "http"
    USER = "svc-00124"
    PASSWORD = "****" #manually changed
  }
  type = map
}

resource "aws_secretsmanager_secret" "hn-tmm-nero-user-detail_secret" {
  name = join("-", ["asm", var.cluster_var, var.envContextAndEnv_var, var.region_var,"tmm-nero-user-detail"])
  description = "Secret for HN to NeRo API"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-pr-euwe01-tmm-nero-user-detail"
        }
}

resource "aws_secretsmanager_secret_version" "hn-tmm-nero-user-detail_values" {
  secret_id     = aws_secretsmanager_secret.hn-tmm-nero-user-detail_secret.id
  secret_string = jsonencode(var.tmm-nero-user-detail_var)
}

variable "bis-api-user-detail_var" {
  default = {
    endpoint = "https://test"
    protocol = "http"
    USER = "test"
    PASSWORD = "****" #TBD
  }
  type = map
}

resource "aws_secretsmanager_secret" "hn-bis-api-user-detail_secret" {
  name = join("-", ["asm", var.cluster_var, var.envContextAndEnv_var, var.region_var,"bis-user-detail-01"])
  description = "Secret for HN to BIS API"
  tags      = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-pr-euwe01-bis-user-detail-01"
        }
}

resource "aws_secretsmanager_secret_version" "hn-bis-api-user-detail_values" {
  secret_id     = aws_secretsmanager_secret.hn-bis-api-user-detail_secret.id
  secret_string = jsonencode(var.bis-api-user-detail_var)
}