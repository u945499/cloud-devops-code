#Logging buckets generic for all bucket of HN
resource "aws_s3_bucket" "hn-general-bucket-auditing" {
  bucket = "s3b-parcels-pr-euwe01-hn-bucket-audits-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-pr-euwe01-hn-bucket-audits-01"
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
        sse_algorithm     = "aws:kms"
      }
    }
	}
}

#Repositories bucket generic for all Lambda functions of HN
resource "aws_s3_bucket" "hn-general-lambda-repo" {
  bucket = "s3b-parcels-pr-euwe01-hn-lambda-repo-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "Pieter.BISSCHOPS@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "s3b-parcels-pr-euwe01-hn-lambda-repo-01"   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
        sse_algorithm     = "aws:kms"
		}
    }	
  }		
 logging {
    target_bucket = aws_s3_bucket.hn-general-bucket-auditing.id
    target_prefix = "s3b-parcels-pr-euwe01-hn-lambda-repo-01"
  } 
}