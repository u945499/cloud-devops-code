/*
June release ,2020 for HN notification
*/




###############################ECS RELEASE SERVICE task definition for June release #########################
resource "aws_ecs_task_definition" "hn_release_svc" {
    execution_role_arn = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRole"])
    task_role_arn  = join("",["arn:aws:iam::",data.aws_caller_identity.current.account_id,":role/ecsTaskExecutionRoleSFM"])
    family = join("-",[local.ctdName,"release-01"])
    requires_compatibilities = ["FARGATE"]
    network_mode = "awsvpc"
    cpu = 256
    memory = 512
    ######## INITIAL TASK DEFINITION ==> TO BE REDEPLOYED BY DEV ########
    container_definitions = <<TASK_DEFINITION
    [
        {
            "logConfiguration": {
                "logDriver": "awslogs",
                "secretOptions": null,
                "options": {
                    "awslogs-group": "/ecs/ctd-parcels-pr-euwe01-hn-release-01",
                    "awslogs-region": "eu-west-1",
                    "awslogs-stream-prefix": "event-release-pr"
                }
            },
            "cpu": 0,
            "environment": [
                {
                    "name": "AWS_DEFAULT_REGION",
                    "value": "eu-west-1"
                },
               
                {
                    "name": "DB_CONNECTION_LIMIT",
                    "value": "100"
                },
                {
                    "name": "DB_NAME",
                    "value": "pr_notification"
                },
                {
                    "name": "DB_REPLICA_SECRET",
                    "value": "scr-parcels-pr-euwe01-hn-notification-rds-ro-user-details-01"
                },
                {
                    "name": "LOG_LEVEL",
                    "value": "verbose"
                },
                {
                    "name": "MAPPER_CACHE",
                    "value": "3600000"
                },
                {
                    "name": "MAX_INFLIGHT_MESSAGES",
                    "value": "20"
                },
                
               {
                "name": "DB_MASTER_SECRET",
                "value": "scr-parcels-pr-euwe01-hn-notification-rds-connection-details-01"
               },
                           
            {
                "name": "LOG_LEVEL",
                "value": "verbose"
            }
           
        ],

            "image": "119394011513.dkr.ecr.eu-west-1.amazonaws.com/ecr-hn-pr-euwe01-release-01:PR-572",
            "name": "Release"
        }       
    ]
    TASK_DEFINITION
    lifecycle {
        ignore_changes = [container_definitions]
    }
}