/* June Flex Release
AWS Components
1.Secret Manager - 
    - 1+1 for Nia & LRS Integration (secret + secret version)
    - 1+1 for Hn HN notification
    - 1+1 for RDS Serverless
2.Lambda 
    - 1+1 for HN Notifcation ( lambda + lambda permission)
3.Api Gateway
    - 1+1 for api gateway for HN NOTIFICATION
4.s3 Buckets
    - for dchPAYLOAD
    - for hn feedback,hn event
    - 
5. RDS
    - Security group
    - subnet
    - db parameter
    - rds serverless instance
    - (1+1+1+1) user
6. KMS
    - 1+1 for kms keys

*/

/* Pending 
Endpoint and Key for API
Secrets amd Master key for Prod
*/
######## Secrets for Nia and LRS Integrations ##################

resource "aws_secretsmanager_secret" "hn_nia_lrs_integration_secret" {
  description         = "Secret for NIA and LRS integration for tmm user"
  #name = "asm-parcels-np-ac1-euwe01-hn-events-tmm-user-detail-01"
  name = join("-",[local.asmName,"events-tmm-user-detail-01"])
  tags = merge(local.common_tags,map("Name",join("-",[local.asmName,"events-tmm-user-detail-01"])))
 }


resource "aws_secretsmanager_secret_version" "hn_nia_lrs_integration_secret" {
  
  secret_id     = aws_secretsmanager_secret.hn_nia_lrs_integration_secret.id
    secret_string = jsonencode(var.niaandlrsintegrationSecretstring)
  
    lifecycle {
    ignore_changes         = [secret_string]
  }
} 
################### cognito secret ###################

resource "aws_secretsmanager_secret" "hn-cognito-api-details-cred-01_secret" {
  description         = "Secret for cognito api"
  name = join("-",[local.asmName,"cognito-api-details-cred-01"])
     tags = merge(local.common_tags,map("Name",join("-",[local.asmName,"cognito-api-details-cred-01"])))
  }

############SECRETS FOR HN NOTIFICATIONS  ##############

#Not for june flex
/* resource "aws_secretsmanager_secret" "hn_fdbck_hn_intake_api" {
  description = "Secret for HN Intake API access by HN notification"
  # name = common string + resource purpose-sequnce number
  name        = join("-",[local.asmName,"feedback-hn-intake-api-01"])
  tags = merge(local.common_tags,map("Name",join("-",[local.asmName,"feedback-hn-intake-api-01"])))
}


resource "aws_secretsmanager_secret_version" "hn_fdbck_hn_intake_api_secret" {
  secret_id     = aws_secretsmanager_secret.hn_fdbck_hn_intake_api.id
  secret_string = <<EOF
    {
    "ENDPOINT" : "https://hn-api-ac2.bposthn.net/event",
    "X_API_KEY" : "NIxVHE7gnX5a6Fm9vwh6qa0JxIAYGDKr3Vc2APLu"
  }
  EOF
} */





