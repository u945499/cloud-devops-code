
resource "aws_kinesis_stream" "Prod_stream" {
  name             = "kst-parcels-pr-euwe01-hn-solystics-01"
  shard_count      = 2
  retention_period = 24
  encryption_type = "KMS"
  kms_key_id = "d6c30969-70e7-485f-b72b-c399c2e41052"
  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
"ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
   
  }
}
	
resource "aws_s3_bucket" "solysticsconsumerlambda-repo" {
  bucket = "s3b-parcels-pr-euwe01-repo-hn-solysticsconsumerlambda-01"
  acl    = "private"
  tags = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-parcels-pr-euwe01-log-hn-solysticsconsumerlambda-01" 
   
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
        sse_algorithm     = "aws:kms"
		}
    }
	
  }
		
 logging {
    target_bucket = aws_s3_bucket.solysticsconsumerlambda-audit.id
    target_prefix = "log/"
  } 
}
  
  
resource "aws_s3_bucket" "solysticsconsumerlambda-audit" {
  bucket = "s3b-parcels-pr-euwe01-hn-solysticsconsumerlambda-01"
  acl    = "log-delivery-write"
   tags = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
		   "Name"                 = "s3b-parcels-pr-euwe01-hn-solysticsconsumerlambda-01" 
  }
 server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
        sse_algorithm     = "aws:kms"
      }
    }
	}
	
  }  
  
    
  resource "aws_sns_topic" "commonsnspr" {
  name = "sns-parcels-pr-euwe01-hn-common-events-01"
   lifecycle {
    create_before_destroy = true
  }
    kms_master_key_id= "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
	
  tags = {
          "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-parcels-pr-euwe01-hn-common-events-01"
  }
}

resource "aws_sns_topic" "rmsnspr" {
  name = "sns-parcels-pr-euwe01-hn-rm-events-01"
   lifecycle {
    create_before_destroy = true
  }
    kms_master_key_id= "arn:aws:kms:eu-west-1:119394011513:key/d6c30969-70e7-485f-b72b-c399c2e41052"
	
  tags = {
          "ApplicationAcronym"   = "HN"
           "ApplicationName"      = "Hybrid Network"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
          "Name"                 = "sns-parcels-pr-euwe01-hn-rm-events-01"
  }
}

variable "hn-intake-api-key" {
  default = {
    apiKey = "07vkTXKhHXbzFLh87VLyapd1kPFewi73T3M4ybuc"
  }
  type = map
}

variable "hn-solystics-api-cred" {
  default = {
    username = "svc-00414"
    password = "****"
  }
  type = map
}

resource "aws_secretsmanager_secret" "sfm-intake-api_secret" {
  name = "asm-parcels-pr-euwe01-hn-intake-apikey-01"
  description = "Secret for HN Intake service API"
  tags      = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-pr-euwe01-hn-intake-apikey-01"
        }
}

resource "aws_secretsmanager_secret_version" "sfm-intake-api-secret_values" {
  secret_id     = aws_secretsmanager_secret.sfm-intake-api_secret.id
  secret_string = jsonencode(var.hn-intake-api-key)
}

resource "aws_secretsmanager_secret" "sfm-solystics-api-cred_secret" {
  name = "asm-parcels-pr-euwe01-hn-solystics-api-cred-01"
  description = "Secret for Solystics API"
  tags      = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "pieter.bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "asm-parcels-pr-euwe01-hn-solystics-api-cred-01"
        }
}

resource "aws_secretsmanager_secret_version" "sfm-solystics-api-cred_values" {
  secret_id     = aws_secretsmanager_secret.sfm-solystics-api-cred_secret.id
  secret_string = jsonencode(var.hn-solystics-api-cred)
}

data "aws_iam_policy_document" "sfm-lambda-solystics-policy" {
    statement {
        actions = [
              "logs:CreateLogStream",
              "logs:PutLogEvents", 
              "logs:CreateLogGroup"            
        ]
        effect = "Allow"
        resources=["arn:aws:logs:eu-west-1:119394011513:log-group:/aws/codebuild/cd*-parcels-*-euwe01-hn-*"]
    }
    statement {
        actions = [
              "kms:Decrypt",
              "kms:GenerateDataKey"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "s3:*"            
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "secretsmanager:*"            
        ]
        effect = "Allow"
        resources=["*"]
    }
}
resource "aws_iam_policy" "sfm-lambda-solystics-policy" {
    name = "iap-parcels-pr-euwe01-sfm-lambda-solystics-execution-role"  
    policy = data.aws_iam_policy_document.sfm-lambda-solystics-policy.json
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_custom" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = aws_iam_policy.sfm-lambda-solystics-policy.arn
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_kinesisFullAccess" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFullAccess"
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_LambdeKinesisExecution" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaKinesisExecutionRole"
}

resource "aws_iam_role_policy_attachment" "sfm-lambda-solystics-execution-role_sfm-lambda-solystics-policy_KinesisAnalyticsFullAccess" {
  role       = aws_iam_role.sfm-lambda-solystics-execution-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisAnalyticsFullAccess"
}

data "aws_iam_policy_document" "sfm-lambda-solystics-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "sfm-lambda-solystics-execution-role" {
  name = "iar-parcels-pr-euwe01-sfm-lambda-solystics-execution-role"
  path = "/"
  assume_role_policy = data.aws_iam_policy_document.sfm-lambda-solystics-assume-role.json
}

data "aws_iam_policy_document" "sfm-codebuild-policy" {
  statement {
        actions = [
              "codebuild:CreateReportGroup",
              "codebuild:CreateReport",
              "codebuild:UpdateReport",
              "codebuild:BatchPutTestCases"               
        ]
        effect = "Allow"
        resources=[
          "arn:aws:codebuild:eu-west-1:119394011513:report-group/cd*-parcels-*-euwe01-hn-*"          
        ]
    }
    statement {
        actions = [
              "ssm:GetParameters"               
        ]
        effect = "Allow"
        resources=[
            "arn:aws:ssm:eu-west-1:119394011513:parameter/CodeBuild/*"
        ]
    }
    statement {
        actions = [
              "iam:PassRole",
              "s3:*",
              "ecs:*",
              "ecr:*"              
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "logs:CreateLogStream",
              "logs:PutLogEvents", 
              "logs:CreateLogGroup"            
        ]
        effect = "Allow"
        resources=[
          "arn:aws:logs:eu-west-1:119394011513:log-group:*"
        ]
    }
    statement {
        actions = [
              "lambda:UpdateFunctionCode"        
        ]
        effect = "Allow"
        resources=["*"]
    }
    statement {
        actions = [
              "kms:Decrypt",
              "kms:GenerateDataKey"            
        ]
        effect = "Allow"
        resources=["*"]
    }
}
resource "aws_iam_policy" "sfm-codebuild-policy" {
    name = "iap-parcels-pr-euwe01-sfm-codebuild"  
    policy = data.aws_iam_policy_document.sfm-codebuild-policy.json
}

resource "aws_iam_role_policy_attachment" "sfm-codebuild-srv-role_sfm-codebuild-policy" {
  role       = aws_iam_role.sfm-codebuild-srv-role.name
  policy_arn = aws_iam_policy.sfm-codebuild-policy.arn
}

data "aws_iam_policy_document" "sfm-codebuild-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "sfm-codebuild-srv-role" {
  name = "iar-parcels-pr-euwe01-sfm-codebuild"
  path = "/service-role/"
  assume_role_policy = data.aws_iam_policy_document.sfm-codebuild-assume-role.json
}

resource "aws_codebuild_project" "solysticsconsumerlambda_deploy" {
       badge_enabled  = false
       build_timeout  = 60
       description    = "Code Build created for lambda solystics consumer deploy"
       name           = "cbd-parcels-pr-euwe01-hn-Solystics-01"
       queued_timeout = 480
       service_role   = aws_iam_role.sfm-codebuild-srv-role.arn
       source_version = "master"
       lifecycle {
        prevent_destroy = true
      }
       tags      = {
           "ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter Bisschops"
           "ApplicationSupport"   = "NA"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "Na"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
           "Name"                 = "cbd-parcels-pr-euwe01-hn-Solystics-01"
        }

       artifacts {
           encryption_disabled    = false
           location = "s3b-parcels-pr-euwe01-repo-hn-solysticsconsumerlambda-01"
           name = "cbd-parcels-pr-euwe01-hn-Solystics-01"
           namespace_type = "NONE"
           override_artifact_name = true
           packaging = "ZIP"
           path = "Code"
           type = "S3"
        }

       cache {
           modes = []
           type  = "NO_CACHE"
        }

       environment {
           compute_type                = "BUILD_GENERAL1_SMALL"
           image                       = "aws/codebuild/standard:1.0"
           image_pull_credentials_type = "CODEBUILD"
           privileged_mode             = false
           type                        = "LINUX_CONTAINER"

           environment_variable {
               name  = "user_bitbucket_id"
               type  = "PLAINTEXT"
               value = "Ramkrupakaran"
            }
           environment_variable {
               name  = "bitbucket_password"
               type  = "PLAINTEXT"
               value = "Bpost123"
            }
       }
           
       logs_config {
           cloudwatch_logs {
               status = "ENABLED"
               group_name = "Solystics"
           }
              s3_logs {
               encryption_disabled = false
               status              = "DISABLED"
            }
        }

       source {
          buildspec           = "code-build-config/pr/build-spec-pr-deploy.yml"
           git_clone_depth     = 1
           insecure_ssl        = false
           location            = "https://sidharth.gandhi.ext@bitbucket.org/bpost_deliveryparcelsint/hn-solystics-kinesis-consumer.git"
           report_build_status = false
           type                = "BITBUCKET"
		   git_submodules_config {
              fetch_submodules = false 
            }
        }
        
    }

resource "aws_lambda_function" "prod_lambda" {
  s3_bucket = aws_s3_bucket.solysticsconsumerlambda-repo.id
  s3_key = "lmb-parcels-pr-euwe01-solystics-consumer-01/app.zip"
  function_name = "lmb-parcels-pr-euwe01-solystics-consumer-01"
  role          = aws_iam_role.sfm-lambda-solystics-execution-role.arn
  handler       = "hn-solystics-kinesis-consumer/src/index.handler"
  timeout = 303
  tags = {
"ApplicationAcronym"   = "SFM"
           "ApplicationName"      = "Sign for Me"
           "ApplicationOwner"     = "Pieter.Bisschops@bpost.be"
           "ApplicationSupport"   = "SindhyaEsther.Selvin.ext@bpost.be"
           "Backup"               = "False"
           "CloudServiceProvider" = "AWS"
           "DRLevel"              = "2"
           "DataProfile"          = "Confidential"
           "Environment"          = "Production"
           "ManagedBy"            = "TCS"
    
  }
   runtime = "nodejs12.x"

  environment {
    variables = {

	AWS_API_VERSION	= "2012-11-05"
AWS_REGION_DEFAULT = "eu-west-1"
IMAGES_REQUEST_DELAY_MS ="1100"
IMAGES_WSG_URL	= "https://webservices.bpost.be/ws/mmt/solystic/getMailImages?idTag="
INTAKE_API_SECRET_KEY	= "asm-parcels-pr-euwe01-hn-intake-apikey-01"
INTAKE_EVENT_URL = "https://hn-api.bpost.cloud/event"
INTAKE_WSG_URL = "https://hn-api.bpost.cloud/eventwithbinary"
PARALLEL_PROCESSING	= "1"
PRODUCTXML_BUCKET_NAME =	"s3b-parcels-pr-euwe01-repo-hn-solysticsconsumerlambda-01"
PRODUCTXML_CACHE =	"3600000"
PRODUCTXML_KEY	= "ProductXML.xml"
SOLYSTICS_IMAGES_API_SECRET_KEY	 = "asm-parcels-pr-euwe01-hn-solystics-api-cred-01"
	  
	  
    }
  }

}


