######################### MONITORING #########################

# alerting when tasks are stopped due to unexpected issue (not by user or autoscaling)

resource "aws_sns_topic" "hn_monitoring" {
  name = join("-",[local.snsName,"monitoring-01"])
}

resource "aws_sns_topic_policy" "hn_monitoring" {
  arn    = aws_sns_topic.hn_monitoring.arn
  policy = data.aws_iam_policy_document.hn_monitoring.json
}

data "aws_iam_policy_document" "hn_monitoring" {
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [aws_sns_topic.hn_monitoring.arn]
  }
}
