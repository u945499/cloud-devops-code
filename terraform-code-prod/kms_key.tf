/* 
JUNE  RELEASE FOR HN NOTIFICATION
*/
##################KMS FOR HN NOTIFICATION RDS DATABASE ##########


################ policy to be changed for release service ??????
resource "aws_kms_key" "hn_rds_kms_key" {
  description             = "KMS key RDS database for HN"
  customer_master_key_spec= "SYMMETRIC_DEFAULT"
  key_usage               = "ENCRYPT_DECRYPT"
  enable_key_rotation     = "true"
  is_enabled              = "true"
policy = <<EOF
{
    "Id": "key-consolepolicy-3",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
            },
            "Action": "kms:*",
            "Resource": "*"
        },
        {
            "Sid": "Allow access for Key Administrators",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-MigrationTeam"
            },
            "Action": [
                "kms:Create*",
                "kms:Describe*",
                "kms:Enable*",
                "kms:List*",
                "kms:Put*",
                "kms:Update*",
                "kms:Revoke*",
                "kms:Disable*",
                "kms:Get*",
                "kms:Delete*",
                "kms:TagResource",
                "kms:UntagResource",
                "kms:ScheduleKeyDeletion",
                "kms:CancelKeyDeletion"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-Reader"
                ]
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow attachment of persistent resources",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                     "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/iar-Reader"
                ]
            },
            "Action": [
                "kms:CreateGrant",
                "kms:ListGrants",
                "kms:RevokeGrant"
            ],
            "Resource": "*",
            "Condition": {
                "Bool": {
                    "kms:GrantIsForAWSResource": "true"
                }
            }
        }
    ]
}
EOF
  deletion_window_in_days = 7
 tags = merge(local.common_tags,map("Name",join("-",[local.kmsName,"rds-01"])))
}


resource "aws_kms_alias" "hn_rds_kms_key_alias" {
  depends_on      = [aws_kms_key.hn_rds_kms_key]
  # name = join("alias\",join("-",[local.kmsName,"rds-01"]))
  name="alias/kms-parcels-pr-euwe01-hn-rds-01"
  target_key_id = aws_kms_key.hn_rds_kms_key.key_id
}

#####################################################################################3