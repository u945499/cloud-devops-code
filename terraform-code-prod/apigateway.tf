/*
####################JULY ,2020 RELEASE #########################
data "template_file" "hn_api_gateway_openapi_spec" {
  
  template = file("./Feedback_Swagger_file.json")
  vars = {
    uri_arn = join("/", ["arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions", aws_lambda_function.hn_lmb_notification_feedback.arn, "invocations"]),
    host= "hn-api-prd.bposthn.net"
  }
}



resource "aws_api_gateway_rest_api" "hn_rest_api_ac2" {
  description    = "API Gateway for HN"
  name        = join("-",[local.apgName,"notification-01"])
  body = data.template_file.hn_api_gateway_openapi_spec.rendered
  endpoint_configuration {
    types = ["REGIONAL"]
  }
 
lifecycle {
    ignore_changes         = [body]
  }
  
  tags      = merge(local.common_tags,map("Name",join("-",[local.apgName,"notification-01"])))
}


resource "aws_api_gateway_deployment" "hn_api_deploy_ac2" {
  rest_api_id = aws_api_gateway_rest_api.hn_rest_api.id
  stage_name  = "prd"
}

resource "aws_api_gateway_api_key" "hn_api_key" {
  name = join("-",[local.apkName,"key-01"])
}

resource "aws_api_gateway_usage_plan" "hn_api_UsagePlan" {
  name = join("-",[local.apuName,"key-01"])
  description  = "Usage plan for apk-parcels-np-hn-key-01"
  api_stages {
    api_id = aws_api_gateway_rest_api.hn_rest_api.id
    stage  = aws_api_gateway_deployment.hn_api_deploy.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "hn_api_KeyUsagePlan" {
  depends_on = [aws_api_gateway_api_key.hn_api_key, aws_api_gateway_usage_plan.hn_api_UsagePlan]
  key_id        = aws_api_gateway_api_key.hn_api_key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.hn_api_UsagePlan.id
}
*/