/* June Flex Release
SNS TOPIC FOR DCH PUBLISHER
*/

resource "aws_sns_topic_subscription" "hn_sns_ac2_publisher_01" {
  topic_arn = aws_sns_topic.commonsnspr.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.hn_lmb_notification_dch_publisher.arn
 
}
