/* June Flex Release
AWS Components
Lambda for HN notifcation
    - 1 for HN Notifcation DCH retry ( lambda + lambda permission)
    - 1 for HN Notifcation DCH Publisher ( lambda + lambda permission)
    - 1 for HN Notifcation DCH Connector ( lambda + lambda permission)
*/


############# June Flex - HN Notification Lambda #################

resource "aws_lambda_function" "hn_lmb_dch_retry" {
  s3_bucket = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key = aws_s3_bucket_object.hn_s3key_notification_dch_retry.id
  function_name = join("-",[local.lmbName,"notification-dch-retry-01"])
  role          = var.lambdaExecutionRole
  handler       = "build/src/index.handler"
  runtime = "nodejs12.x"
  timeout = 300

 vpc_config {
          security_group_ids = [aws_security_group.hn_notif_rds_security_group.id]
          subnet_ids         = var.db_subnet_ids
        }

  environment {
    variables = {
	        AWS_REGION_DEFAULT = "eu-west-1"
            COGNITO_SERVICE_USER_AWS_SECRET = aws_secretsmanager_secret.hn-cognito-api-details-cred-01_secret.name
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET = aws_s3_bucket.dchpayloadmessages.id
            DCH_SERVICE_URL = "https://communicationhub.bpost.be/PROD/sendnotifications"
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = var.dbMasterSecret["pr"]
            DB_NAME = var.dbName["pr"]
            DB_REPLICA_SECRET = var.dbReplicaSet["pr"]
        
            
    }
  }
  tags      = merge(local.common_tags,map("Name",join("-",[local.lmbName,"notification-dch-retry-01"])))
  }

 resource "aws_lambda_permission" "allow_cw_rule_trigger" {
  statement_id  = "AllowExecutionFromCWR"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hn_lmb_dch_retry.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule.arn
  
}

resource "aws_lambda_function" "hn_lmb_notification_dch_connector" {
  s3_bucket = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key = aws_s3_bucket_object.hn_s3key_notification_dch_connector.id
  #"DV1_dch-connector/app.zip"
  function_name = join("-",[local.lmbName,"notification-dch-connector-01"])
  role          = var.lambdaExecutionRole
  handler       = "hn-notification-service/src/NotificationPushLambda.handler"
  runtime = "nodejs12.x"
  timeout = 120 

  vpc_config {
          security_group_ids = [aws_security_group.hn_notif_rds_security_group.id]
          subnet_ids         = var.db_subnet_ids
        }
        
  environment {
    variables = {
            DCH_EMAIL_ADDRESS                     = "DCH_Team@bpost.be"
            COGNITO_SERVICE_USER_AWS_SECRET	= aws_secretsmanager_secret.hn-cognito-api-details-cred-01_secret.name
            DCH_PUSH_PAYLOAD_MESSAGES_BUCKET	= aws_s3_bucket.dchpayloadmessages.id
            DCH_SERVICE_SERVICE_URL = "https://communicationhub.bpost.be/PROD/sendnotifications"  
            ####need to confirm
            DCH_TRIGGER_ENABLED	= "true"
            NOTIFICATION_INCOMING_MESSAGES_BUCKET	= aws_s3_bucket.incomingnotification.id
            DB_CONNECTION_LIMIT = "100"
            DB_MASTER_SECRET = var.dbMasterSecret["pr"]
            DB_NAME = var.dbName["pr"]
            DB_REPLICA_SECRET = var.dbReplicaSet["pr"]
            TRACK_N_TRACE_URL = "https://track.bpost.cloud/btr/web"
            ###NEEDS TO CHECK TNT URL
            REQUEST_TIME_TO_LIVE = "3600"
            REQUEST_RETRY_COUNT = "1"
    }
	
  }
    tags  = merge(local.common_tags,map("Name",join("-",[local.lmbName,"notification-dch-connector-01"]))) 
      
  }

resource "aws_lambda_function" "hn_lmb_notification_dch_publisher" {
  s3_bucket = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key =aws_s3_bucket_object.hn-s3key_notification-publisher.id
   #"ac-publisher/app.zip"
  function_name = join("-",[local.lmbName,"notification-publisher-connector-01"])
  role          = var.lambdaExecutionRole
  handler       = "build/src/index.handler"
  
  
   runtime = "nodejs12.x"
   timeout =120
   environment {
    variables = {
    AWS_REGION_DEFAULT     = "eu-west-1"
	  AWS_API_VERSION	= "2012-11-05"
	  NOTIFICATION_QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/086243371668/sqs-parcels-pr-euwe01-notification-01"
	  
	  
    }
  }
  tags  = merge(local.common_tags,map("Name",join("-",[local.lmbName,"notification-publisher-connector-01"]))) 
 
  }

  resource "aws_lambda_permission" "notification-publisher-connector-SNS-trigger" {
    statement_id = "AllowExecutionFromSNS"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.hn_lmb_notification_dch_publisher.arn
       principal = "sns.amazonaws.com"
    source_arn = aws_sns_topic.commonsnspr.arn
    
}

resource "aws_lambda_event_source_mapping" "hn_lambda_trigger_sqs_notif_dch_connector" {
  event_source_arn  = aws_sqs_queue.sqs_hn_notificationsqs.arn
  function_name     = aws_lambda_function.hn_lmb_notification_dch_connector.arn
}

#################### MISSING #########################
/*
dch-retry, dch-connector, publisher ==> lmd-parcels-hn-st1-euwe01-notification-publisher-connector-01
+ cloudwatch rule for dch-retry
+ event source mapping with sqs as trigger for dch-connector
*/
  
######################JULY RELEASE #################################
# NOT for June flex
/* resource "aws_lambda_function" "hn_lmb_notification_feedback" {
  s3_bucket     = aws_s3_bucket.hn-general-lambda-repo.id
  s3_key        = aws_s3_bucket_object.hn-notification-feedback_s3key.id
  function_name = join("-",[local.lmbName,"notification-feedback-01"])
  role          = var.lambdaExecutionRole
  handler       = "build/src/index.handler"
  runtime       = "nodejs12.x"
  timeout       = 300

     #security groups needs to check for prod.
  vpc_config  { 
    security_group_ids = ["sg-0429b6a02a15d681a"]
    subnet_ids = [
      "subnet-05a33b81b6ea84ff4",
      "subnet-0b43ff380a425d9aa"
    ]
  }

  environment {
    variables = {
      INTAKE_SECRET               = join("-",[local.asmName,"feedback-hn-intake-api-01"])
      DB_MASTER_SECRET            = var.dbMasterSecret["pr"]
      DB_NAME                     = var.dbName["pr"]
      DB_REPLICA_SECRET           = var.dbReplicaSet["pr"]
      FEEDBACK_MESSAGES_BUCKET    = aws_s3_bucket.hn_fdbck_msg_bucket.id
      NOTIFICATION_MESSAGE_BUCKET = aws_s3_bucket.dchpayloadmessages.id
      FEEDBACK_EVENT_BUCKET       = aws_s3_bucket.hn_fdbck_evnt_bucket.id
    }
  }
  tags = merge(local.common_tags,map("Name",local.lmbName))
}


resource "aws_lambda_permission" "hn_lmb_notification_feedback_perm" {
   depends_on = [aws_api_gateway_rest_api.hn_rest_api]
   statement_id  = "AllowExecutionFromApiGateway"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.hn_lmb_notification_feedback.function_name
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.hn_rest_api.execution_arn}/*//*"
} */
