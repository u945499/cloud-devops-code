/*
June ,202 rlease for HN Notifications

*/

#########################JUNE RELEASE ,2020 ,HN NOTIFICATION ,CLOUD WATCH ############################
resource "aws_cloudwatch_event_rule" "hn_lmb_dch_retry_rule" {
    name        = join("-",[local.cwrName,"schedule-lmb-notification-dch-retry"])
    schedule_expression =  "rate(10 minutes)"
}

resource "aws_cloudwatch_event_target" "hn_lmb_dch_retry_schedule" {
    target_id = "hn_lmb_dch_retry_schedule"
    rule      = aws_cloudwatch_event_rule.hn_lmb_dch_retry_rule.name
    arn       = aws_lambda_function.hn_lmb_dch_retry.arn
  }

  resource "aws_cloudwatch_log_group" "hn_service_svc_loggroup" {
  name = "/ecs/ctd-parcels-pr-euwe01-hn-release-01"

  tags = merge(local.common_tags,map("Name","ctd-parcels-pr-euwe01-hn-release-01"))
}


######################### MONITORING #########################

# alerting when tasks are stopped due to unexpected issue (not by user or autoscaling)

resource "aws_cloudwatch_event_rule" "hn_publisher_task_stopped" {
  name        = join("-",[local.cwrName,"publisher-task-stopped-01"])
  description = "Capture when task is stopped"

  event_pattern = <<PATTERN
{
      "source": [ "aws.ecs" ],
      "detail-type": [
        "ECS Task State Change"
      ],
      "detail": {
        "clusterArn": [
            "arn:aws:ecs:eu-west-1:119394011513:cluster/ecf-parcel-pr-euwe01-sfm-01"
          ],
        "group": [ "service:ecs-service-parcels-pr-euwe01-publisherservice-01" ],
        "lastStatus": [ "STOPPED" ],
        "stoppedReason" : ["Essential container in task exited"]
    }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "hn_publisher_task_stopped" {
  rule      = aws_cloudwatch_event_rule.hn_publisher_task_stopped.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.hn_monitoring.arn
}

resource "aws_cloudwatch_event_rule" "hn_broker_task_stopped" {
  name        = join("-",[local.cwrName,"broker-task-stopped-01"])
  description = "Capture when task is stopped"

  event_pattern = <<PATTERN
{
      "source": [ "aws.ecs" ],
      "detail-type": [
        "ECS Task State Change"
      ],
      "detail": {
        "clusterArn": [
            "arn:aws:ecs:eu-west-1:119394011513:cluster/ecf-parcel-pr-euwe01-sfm-01"
          ],
        "group": [ "service:ecs-service-parcels-pr-euwe01-brokerservice-01" ],
        "lastStatus": [ "STOPPED" ],
        "stoppedReason" : ["Essential container in task exited"]
    }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "hn_broker_task_stopped" {
  rule      = aws_cloudwatch_event_rule.hn_broker_task_stopped.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.hn_monitoring.arn
}