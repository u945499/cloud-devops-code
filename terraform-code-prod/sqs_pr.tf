/*
June release HN Notifications
*/

####################june ,2020 hn release for HN NOTIFCATIONS#########################

resource "aws_sqs_queue" "sqs_hn_notificationsqs" {
  name                      = join("-",[local.sqsName,"notification-01"])
  delay_seconds             = 10
  max_message_size          = 262144
  message_retention_seconds = 172800
  receive_wait_time_seconds = 10
  visibility_timeout_seconds        = 120
  redrive_policy = jsonencode({
    deadLetterTargetArn =  aws_sqs_queue.hn_sqs_iampolicy_notification_dlq.arn
    maxReceiveCount     = 4
  })
  lifecycle {
    create_before_destroy = true
  }
  kms_master_key_id= var.kmsMasterKeyID
 tags = merge(local.common_tags,map("Name",join("-",[local.sqsName,"notification-01"])))
  }

data "aws_iam_policy_document" "hn_sqs_iampolicy_notificationsqs" {
    policy_id = join("/", [aws_sqs_queue.sqs_hn_notificationsqs.arn, "SQSDefaultPolicy"])
    statement {
        
        actions = ["SQS:*"]
        effect = "Allow"
        principals {
            type        = "AWS"
            identifiers = ["*"]
        }
        resources=[aws_sqs_queue.sqs_hn_notificationsqs.arn]
        condition {
                   test     = "ArnEquals"
          variable = "aws:SourceArn"
          values = [aws_lambda_function.hn_lmb_notification_dch_publisher.arn]
        }
    }
}

resource "aws_sqs_queue_policy" "hn_sqs_policy_notificationsqs" {
  queue_url = aws_sqs_queue.sqs_hn_notificationsqs.id
  policy = data.aws_iam_policy_document.hn_sqs_iampolicy_notificationsqs.json
}

resource "aws_sqs_queue" "hn_sqs_iampolicy_notification_dlq" {
  name                      = join("-",[local.sqsName,"notification-dlq-01"])
  delay_seconds             = 30
  message_retention_seconds = 1209600
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 120
  max_message_size          = 262144
  kms_master_key_id                 = var.kmsMasterKeyID
  kms_data_key_reuse_period_seconds = 3600

 tags = merge(local.common_tags,map("Name",join("-",[local.sqsName,"notification-01"])))
}

############################july release#########################################